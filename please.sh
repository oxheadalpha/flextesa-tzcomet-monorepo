#! /bin/sh
set -e

say () { printf "fletimo: $@\n" >&2 ; }

babuso_test_port=8480

eval $(opam env)

build () {
    say "Building"
    dune build @check
    # bb self-test
    say "Building TZComet website"
    profile=dev root=tzcomet/ tzcomet/please.sh build all
    curl http://localhost:$babuso_test_port/please/die || echo "  \\-> don't care"
}

babconf () {
    bb config --port ${port:-$babuso_test_port} \
       --octez-node ${node}  \
       --theme ${theme:-default} "$@"
}

bbi () {
    export BABUSO_ROOT=$PWD/_build/babi-data-dir/
}
babi () {
    bbi
    bb "$@"
}
babiconf () {
    bbi
    export node=https://rpc.ghostnet.teztnets.xyz/
    babconf "$@"
}
babiwipe () {
    bbi
    rm -fr "$BABUSO_ROOT"
}

bbf () {
    # The one of the backend tests
    export BABUSO_ROOT=/tmp/runtest-test/babuso-20010/data-dir
}
babf () {
    bbf
    export node=http://localhost:20000
    bb "$@"
}



test () {
    dune runtest "$@"
}

longtestsann () {
    {
        printf "#==========================================================\n"
        printf "# FLETIMO-LONGTESTS:\n# $(date -R)\n#\n"
        printf "$*\n" | sed 's/^/# /'
        printf "#==========================================================\n"
    } >&2
}

mockuptests () {
    export PATH=${octez_binaries}:$PATH
    bb run-mockup-test --root "$PWD/_build/mockuptests/" "$@"
}

longtests () {
    export PATH=${octez_binaries}:$PATH
    longtestsann "Running Mockup-Tests"
    mockuptests
    longroot=$PWD/_build/backendtests/
    longtestsann "Running Backend-Test: Kathmandu (with Webdriver Tests)"
    tmp0=$(mktemp ${TMPDIR:-/tmp}/backend-tests-XXXX.log)
    say "Logs: $tmp0"
    bb run-backend-tests --protocol-kind Kathmandu --root "$longroot/kathmandu" \
       --run-webdriver-tests 'dune exec babuso-webtests/babexample.exe --' \
       > "$tmp0" 2>&1 || {
        say "FAILURE!!"
        tail -n 100 "$tmp0" | sed 's/^/  | /'
        exit 3
    }
    longtestsann "Running Backend-Test: Alpha"
    tmp1=$(mktemp ${TMPDIR:-/tmp}/backend-tests-XXXX.log)
    say "Logs: $tmp1"
    bb run-backend-tests --protocol-kind Alpha --root "$longroot/alpha" \
       > "$tmp1" 2>&1 || {
        say "FAILURE!!"
        tail -n 100 "$tmp1" | sed 's/^/  | /'
        exit 3
    }
    longtestsann "Long tests done \\o/"
}

opam_switch_name=${switch_name:-fletimo-413}
deps () {
    if [ -d _opam ] ; then
        say 'Opam switch already there'
    else
        opam switch create "$opam_switch_name" \
             --formula='"ocaml-base-compiler" {>= "4.13" & < "4.14"}'
        opam switch link "$opam_switch_name" .
    fi
    eval $(opam env)
    opam switch import switch.opam
}

lint () {
    dune exec autifest/autifest.exe apply || echo Letting happen
    dune build @fmt --auto || echo Letting happen
}

bb () { dune exec babuso-bin/babuso.exe -- "$@" ; }

reload () { eval $(./please.sh env) ; }

# __NOT_IN_ENV_FROM_HERE__
env () {
    opam env
    endline=`awk '/__NOT_IN_ENV_FROM_HERE__/ { print NR; exit 0; }' ./please.sh`
    head -n $endline ./please.sh | grep -v 'set -e' > /tmp/env.sh
    # eval $( ... ) swallows newlines, so we go through a temp-file:
    echo ". /tmp/env.sh"
}



{ if [ "$1" = "" ] ; then build ; else "$@" ; fi ; }
