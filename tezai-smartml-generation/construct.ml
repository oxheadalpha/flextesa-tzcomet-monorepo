open! Import
open SmartML
open Smart_ml

module L = struct
  include Literal

  let nat_of_z z = nat (Z.to_string z |> Big_int.big_int_of_string)
  let int_of_int z = int (Int.to_string z |> Big_int.big_int_of_string)
  let mutez_of_z z = mutez (Z.to_string z |> Big_int.big_int_of_string)
  let timestamp_of_z z = timestamp (Z.to_string z |> Big_int.big_int_of_string)
end

module E = struct
  let none = unline Expr.none
  let some = unline Expr.some
  let open_some = unline Library.open_some
  let sender = unline Expr.sender
  let amount = unline Expr.amount
  let constant = unline Expr.cst
  let tuple = unline Expr.tuple
  let first = unline Expr.first
  let second = unline Expr.second
  let bin_op op = unline (Expr.bin_op ~op)
  let add = unline (Expr.bin_op ~op:Basics.BAdd)
  let now = unline Expr.now

  let add_seconds ~timestamp ~nat =
    unline (Expr.add_seconds timestamp (unline Expr.to_int nat))

  let show = Expr.show
  let build_list ~elems = unline Expr.build_list ~elems
  let cons x l = unline Expr.cons x l
  let match_cons name = Expr.match_cons name
  let map_over_list l f = unline Expr.map_function l f
  let sum l = unline Expr.sum l
  let eif cond a b = unline Expr.eif cond a b
  let build_map ~entries ~big = unline Expr.build_map ~big ~entries
  let pack e = unline Expr.pack e

  let get ~map ~key default_value missing_messgage =
    unline Expr.item map key default_value missing_messgage

  let get_opt ~key ~map = unline Expr.getOpt key map

  let lambda name body ~clean_stack ~with_storage ~with_operations =
    unline Expr.lambda name body ~clean_stack ~with_storage ~with_operations

  let optionally_get_field from = function
    | None -> from
    | Some name ->
        let res = Expr.(attr ~line_no ~name from) in
        res

  let parameter ?field () =
    let base = unline Expr.params in
    optionally_get_field base field

  let storage ?field () =
    let base = unline Expr.storage in
    optionally_get_field base field

  let variable arg_name = unline Expr.variable arg_name
  let local n = unline Expr.local n
  let type_annotation ~t = unline (Expr.type_annotation ~t)

  let typed_comb_record : (string * Type.t * Expr.t) list -> Expr.t =
   fun l ->
    let t =
      Type.(
        record_default_layout SmartML.Config.Comb
          (List.map l ~f:(fun (f, t, _) -> (f, t))))
    in
    type_annotation ~t
      Expr.(record ~line_no (List.map l ~f:(fun (f, _, v) -> (f, v))))

  let ( === ) a b = bin_op BEq a b
  let ( <== ) a b = bin_op BLe a b
  let ( &&& ) a b = bin_op BAnd a b
  let ( ||| ) a b = bin_op BOr a b
  let not_eq a b = bin_op BNeq a b

  let and_l = function
    | [] -> constant (L.bool true)
    | one :: more -> List.fold more ~init:one ~f:( &&& )

  let or_l = function
    | [] -> constant (L.bool false)
    | one :: more -> List.fold more ~init:one ~f:( ||| )

  let split_pair pair = (first pair, second pair)

  let check_signature message signature pk =
    unline (Expr.check_signature message signature pk)

  let self_address = unline Expr.self_address
  let chain_id = unline Expr.chain_id
end

module I = struct
  let seq = Smart_ml.unline Command.seq
  let set_delegate = Smart_ml.unline Library.set_delegate
  let send_mutez ~destination = unline Library.send destination
  let verify ?failwith expr = Smart_ml.unline Command.verify expr failwith
  let sp_failwith = Smart_ml.unline Command.sp_failwith

  let update_variable ~field_exp ~val_exp =
    Smart_ml.unline (Command.set field_exp val_exp)

  let define_local var rhs is_mutable =
    Smart_ml.unline (Command.define_local var rhs is_mutable)

  let for_loop : string -> Expr.t -> Command.t -> Command.t =
   fun name e c -> Smart_ml.unline Command.for_loop name e c

  let while_loop e l = Smart_ml.unline Command.while_loop e l
  let if_then_else cond t e = Smart_ml.unline Command.ifte cond t e

  let mk_match_cons expr id ok_match ko_match =
    Smart_ml.unline Command.mk_match_cons expr id ok_match ko_match

  let comment s = Smart_ml.unline Command.comment s
  let trace x = Smart_ml.unline Command.trace x
end

module T = struct
  let unit = Type.unit
  let key_hash = Type.key_hash
  let signature = Type.signature
  let operation = Type.operation
  let lambda a b = Type.(lambda no_effects a b)
  let bytes = Type.bytes
  let key = Type.key
  let address = Type.address
  let mutez = Type.token
  let nat = Type.nat ()
  let option = Type.option
  let list = Type.list
  let comb_record l = Type.(record_default_layout SmartML.Config.Comb l)
  let equal = Type.equal
  let compare = Type.compare
  let pp = Type.pp
  let timestamp = Type.timestamp
  let int = Type.int ()
  let string = Type.string
  let pair = Type.pair
end

let entry_point ?(parameter = T.unit) name cmds =
  let open SmartML in
  let open Basics in
  build_entry_point ~name ~tparameter:parameter I.(seq cmds)
