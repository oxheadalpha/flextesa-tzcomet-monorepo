# Flextesa TZComet Monorepo

## Commands

Basics:

    ./please.sh deps   # Creates an opam switch `fletimo-413` linked locally
    ./please.sh build  # Checks that everythings build + fully builds TZComet
    ./please.sh test   # Runs the unit tests
    ./please.sh lint   # Runs autifest + ocamlformat everything

or use `env` to make the same functions directly available in the shell:

    eval $(./please.sh env)
    lint
    reload             # Reruns the eval command
    bb --help          # Call the Babuso binary
    longtest           # Runs tests that take longer than unit tests

To create a new opam-switch without destroying the current one if any:

    rm _opam # Removes the current link
    switch_name=my-other-switch-name ./please.sh deps

Then one can put back the old one, or any one:

    opam switch link <name> .

### Babuso-Specific

#### Toplevel

Start an OCaml Toplevel with most backend libraries and ppxes loaded in:

```sh
rlwrap dune exec babuso-bin/top.exe
```

#### Backend Tests

Run the backend-tests and make flextesa become interactive at the end:

```sh
./please.sh bb run-backend --pause-at-end=true
```

Then open <http://localhost:20010> and see `./please.sh bb run-backend --help`.

#### Mockup Tests

For the Law-Office tests:

```sh
./please.sh bb run-mockup-tests --help
```

#### Webdriver UI Tests

Webdriver tests are run by the backend-tests infrastructure, cf. function
`longtests` in `./please.sh`: option
`--run-webdriver-tests 'dune exec babuso-webtests/babexample.exe --'`.

`geckodriver` and `firefox` need to be available in the `$PATH`.
One can use their package manager (cf. `Dockerfile` in `tools/ci.sh`), or

    cd ocaml-webdriver/vendor ; ./get_geckodriver.sh

And then `export PATH=$PWD/ocaml-webdriver/vendor:$PATH`.



#### Running On Testnets

All the “please” functions can be put in the environment
with `eval $(./please.sh env)`, then cf.:

    bb --help

Example of use of the Ghostnet Babuso (`babi`, “i” for Ithacanet):

Wipe-out any previous configuration and storage:

    babiwipe

Create a configuration:

    port=4242 theme=random babiconf

Start the backend:

    babi start

Some things are possible from the CLI:

    babi query raw Get_configuration
    babi query uri-account Alice unencrypted:edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq


## Structure

`autifest/`:

- automation + manifest (cf. octez)
- Generates a bunch of `dune`, `dune-project` (hence `.opam`), `.ocamlformat`,
  `.gitlab-ci.yml`, `LICENSE` files. The monorepo's Gitlab-CI is partially
  generated too.
- Called by the `lint` command above, hence checked by the CI.

“Big” projects, managed mostly manually:

- [Flextesa](https://gitlab.com/tezos/flextesa)
    - Some sub libraries still need to be extracted into small-projects.
- [TZComet](https://github.com/oxheadalpha/TZComet)
    - A _lot_ of code is still quasi-duplicated with other libraries (the
      contract-metadata stuff, some Michelson stuff).

Small projects, managed as much as possible with `autifest/`, goal is 1
project/repo, easy to `opam-pin` & vendor:

- [oxheadalpha/tezai-michelson](https://gitlab.com/oxheadalpha/tezai-michelson/-/merge_requests)
- [oxheadalpha/tezai-contract-metadata](https://gitlab.com/oxheadalpha/tezai-contract-metadata/-/merge_requests)
- [oxheadalpha/tezai-contract-metadata-manipulation](https://gitlab.com/oxheadalpha/tezai-contract-metadata-manipulation/-/merge_requests)
- [oxheadalpha/lwd-bootstrap](https://gitlab.com/oxheadalpha/lwd-bootstrap/-/merge_requests)
- [oxheadalpha/lwd-bootstrap-generator](https://gitlab.com/oxheadalpha/lwd-bootstrap-generator)
- [oxheadalpha/tezai-base58-digest](https://gitlab.com/oxheadalpha/tezai-base58-digest)

On the “way out”:

- `tezos-contract-metadata/` is
  <https://github.com/oxheadalpha/tezos-contract-metadata> → goal is to
  strip/split this into other small libraries until it's empty.

Monorepo only:

- `tezai-smartml-generation/`: wrapper around SmartML, stays in the monorepo
  (Babuso-only)
- `babuso-*` implementation of Babuso

Vendored external dependencies:

- `let-def-lwd/` → [let-def/lwd](https://github.com/let-def/lwd) (no real need
  to vendor actually)
- `smartml-extract/` → from
  <https://gitlab.com/SmartPy/smartpy/-/tree/master/smartML> (not in opam, only
  used by Babuso)
- `ocaml-webdriver` cf.
  [!77](https://gitlab.com/oxheadalpha/flextesa-tzcomet-monorepo/-/merge_requests/77)
  (*Initial version with Webdriver tests, binaries removed*).


## Subtrees

Examples of subtree/monorepo operations:


```sh
git subtree pull --prefix flextesa \
    https://gitlab.com/tezos/flextesa master --squash
git subtree pull --prefix tzcomet \
    https://github.com/oxheadalpha/TZComet master --squash
git subtree add --prefix tezai-base58-digest \
    https://gitlab.com/oxheadalpha/tezai-base58-digest.git main --squash
git subtree add --prefix tezai-contract-metadata-manipulation \
    https://gitlab.com/oxheadalpha/tezai-contract-metadata-manipulation/ main --squash
```

### Making of `smartml-extract`

Inspired by experiments at
[smondet/gen-smartml](https://github.com/smondet/gen-smartml):

```sh
spytmp=$HOME/tmp/smartpy-public
git clone https://gitlab.com/SmartPy/smartpy.git "$spytmp" # Or
git -C "$spytmp" pull origin master
rm -fr smartml-extract
mkdir -p smartml-extract
cp -r $spytmp/smartML/core smartml-extract/smartml-core
cp -r $spytmp/smartML/utils_pure smartml-extract/smartml-utils-pure
cp -r $spytmp/smartML/michelson_base smartml-extract/smartml-michelson-base
cp -r $spytmp/smartML/tools smartml-extract/smartml-tools
cp -r $spytmp/smartML/michel smartml-extract/smartml-michel
cp -r $spytmp/smartML/utils smartml-extract/smartml-utils
#sed -i 's/(public_name/;; (public_name/g' smartml-extract/*/dune
sed -i 's/9@27-30@32-40@8/9-27-30-32-40-8/' smartml-extract/*/dune
sed -i 's/yojson)/num yojson)/' smartml-extract/smartml-utils-pure/dune
for d in $(find smartml-extract/ -type d ) ; do echo '(lang dune 2.9)' > "$d/dune-project" ; done
```


