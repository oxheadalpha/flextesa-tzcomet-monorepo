open! Import

module Octez_node_definition = struct
  type t = Wallet_data.Tezos_node.t [@@deriving sexp, compare, equal]
  type set = t list [@@deriving sexp, compare, equal]
end

module Ui_theme_name = struct
  type t = [ `Lumen | `Cerulean | `Greyscale | `Solar ]
  [@@deriving sexp, variants, compare, equal]

  let all = [ `Lumen; `Cerulean; `Greyscale; `Solar ]
end

module Accounts_table_field_name = struct
  type t =
    | Name of { with_type : [ `No | `Icon | `Text ] }
    | Address of { shorten : int option; links : [ `BCD ] list }
    | Balance of { convert : Wallet_data.Currency.t option }
    | Comments_summary
    | Quick_actions
  [@@deriving sexp, variants, compare, equal]

  let default_list =
    [
      name ~with_type:`Icon;
      address ~shorten:(Some 12) ~links:[ `BCD ];
      balance ~convert:None;
      balance ~convert:(Some `USD);
      comments_summary;
      quick_actions;
    ]
end

module Ui_options = struct
  type t = {
    debug : bool; [@default true]
    theme : Ui_theme_name.t; [@default `Cerulean]
    compact : bool; [@default false]
    operations_per_page : int; [@default 10]
    accounts_table_fields : Accounts_table_field_name.t list;
        [@default Accounts_table_field_name.default_list]
  }
  [@@deriving sexp, fields, compare, equal, make]

  let default = make ()
end

type t = {
  port : int;
  max_connections : int;
  nodes : Octez_node_definition.set;
  octez_client_executable : string;
  fake_tezos_client : bool;
  exchange_rates_allowed_age : float;
  exchange_rates_uri : string;
  ui_options : Ui_options.t;
}
[@@deriving sexp, fields, compare, equal]

let make ?(nodes = []) ?(octez_client_executable = "octez-client")
    ?(exchange_rates_uri = "https://api.tzkt.io/v1/quotes/last")
    ?(ui_options = Ui_options.default) ?(exchange_rates_allowed_age = 500.)
    ?(fake_tezos_client = false) ~port ?(max_connections = 32) () =
  {
    port;
    max_connections;
    nodes;
    octez_client_executable;
    fake_tezos_client;
    exchange_rates_allowed_age;
    exchange_rates_uri;
    ui_options;
  }
