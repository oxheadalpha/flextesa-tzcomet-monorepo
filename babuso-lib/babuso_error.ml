open! Import

module Error_type = struct
  type t = Transfer | Import_account | Unknown
  [@@deriving sexp, variants, compare, equal]

  let names : (string * t) list =
    [ ("Transfer", Transfer); ("Import_account", Import_account) ]

  let type_of_name : string -> t =
   fun s ->
    List.fold names ~init:Unknown ~f:(fun acc (str, typ) ->
        if phys_equal str s then typ else acc)
end

module Command_line_type = struct
  type command_line_error = {
    command : string;
    cmd_type : Error_type.t;
    exit_code : int;
    std_out : string list;
    std_err : string list;
  }
  [@@deriving sexp, equal]
end

module Protocol_type = struct
  let get_command_type : Sexp.t -> Error_type.t =
   fun sexp ->
    match sexp with
    | List (x :: _xs) -> (
        match x with
        | Sexp.Atom s -> Error_type.type_of_name s
        | _other -> Error_type.Unknown)
    | List [] -> Error_type.Unknown
    | Atom _ -> Error_type.Unknown
end

module Octez_error_type = struct
  let cause_not_found = "Primary error cause not found."

  let line_num_containing lines substring =
    List.foldi lines ~init:None ~f:(fun i acc x ->
        match acc with
        | Some n -> Some n
        | None -> if String.is_substring x ~substring then Some i else None)

  let get_line_range lines ~lo ~hi =
    let result =
      List.foldi lines ~init:[] ~f:(fun i acc x ->
          if i >= lo && i <= hi then x :: acc else acc)
    in
    List.rev result

  let parse_error_usage : string list -> string =
   fun lines ->
    let default = cause_not_found in
    let error_line_n = line_num_containing lines "Error:" in
    let usage_line_n = line_num_containing lines "Usage:" in
    let res =
      match Option.both error_line_n usage_line_n with
      | None -> default
      | Some _ ->
          let error_n =
            Option.value_exn
              ~message:
                "parse_octez_err - 'Error:' not found - this shouldn't happen"
              error_line_n
          in
          let usage_n =
            Option.value_exn
              ~message:
                "parse_octez_err - 'Usage:' not found - this shouldn't happen"
              usage_line_n
          in
          (* meaning, if not (usage_n > error_n and at least 3 lines in between) *)
          if usage_n - error_n < 3 then default
          else
            String.concat ~sep:" "
              (get_line_range lines ~lo:(error_n + 1) ~hi:(usage_n - 2))
    in
    res

  let line_n (lines : string list) n =
    dbgf "line_n called with lines: %a, n: %d" Fmt.(Dump.list string) lines n;
    let opt = List.nth lines n in
    Option.value opt ~default:cause_not_found

  let _string_plus_n_lines ~match_str ~num_skip_lines
      ?(default = cause_not_found) lines =
    let f (count_opt, acc) line =
      match count_opt with
      | Some n ->
          if phys_equal n num_skip_lines then (None, line)
          else (Some (n + 1), acc)
      | None ->
          if String.is_substring line ~substring:match_str then (Some 0, acc)
          else (None, acc)
    in
    let _, result = List.fold ~f ~init:(None, default) lines in
    result

  let _string_minus_n_lines ~match_str ~num_skip_lines
      ?(default = cause_not_found) lines =
    let f (count_opt, acc) line =
      match count_opt with
      | Some n -> if phys_equal n 0 then (None, line) else (Some (n - 1), acc)
      | None ->
          if String.is_substring line ~substring:match_str then
            (Some num_skip_lines, acc)
          else (None, acc)
    in
    let _, result = List.fold ~f ~init:(None, default) lines in
    result

  let transfer_cause std_err = line_n std_err 0
  let import_account_cause std_err = parse_error_usage std_err

  let type_to_f_parse_err : (Error_type.t * (string list -> string)) list =
    [ (Transfer, transfer_cause); (Import_account, import_account_cause) ]

  let f_unimplemented : string list -> string =
   fun _ -> "Primary error cause: unimplemented"

  let get_cl_error_parser : Error_type.t -> string list -> string =
   fun cmd_type ->
    List.fold type_to_f_parse_err ~init:f_unimplemented ~f:(fun acc (cct, f) ->
        if phys_equal cmd_type cct then f else acc)

  let parse_octez_err : Error_type.t -> string -> string list -> string =
   fun typ failure_identifier xs ->
    let preamble = failure_identifier ^ " failed due to: " in
    let f = get_cl_error_parser typ in
    let cause = f xs in
    let res = preamble ^ cause in
    dbgf
      "Babuso_error.Octez_error_type.primary_cause - found: |%s| from the \
       input |%a|"
      res
      Fmt.Dump.(list string)
      xs;
    res

  let f_transfer : Command_line_type.command_line_error -> bool =
   fun { command; _ } ->
    let command_list = String.split command ~on:' ' in
    match List.find command_list ~f:(fun s -> phys_equal s "transfer") with
    | Some _z -> true
    | None -> false

  let f_parse_cmd_to_type :
      ((Command_line_type.command_line_error -> bool) * Error_type.t) list =
    [ (f_transfer, Transfer) ]

  let get : Command_line_type.command_line_error -> Error_type.t =
   fun oce ->
    List.fold f_parse_cmd_to_type ~init:Error_type.Unknown
      ~f:(fun acc (key_f, value) -> if key_f oce then value else acc)
end

let string_list_to_atoms strs =
  Sexp.List (List.map strs ~f:(fun s -> Sexp.Atom s))

type babuso_error =
  | Command_line_error of Command_line_type.command_line_error
  | Configuration_error of { msg : string }
  | Contract_error of { contract_id : string; msg : string }
  | Http_response_error of {
      url : string;
      code : int;
      body : string option;
      msg : string;
    }
  | Http_send_error of { url : string; e_sexp : Sexp.t }
  | Io_error of { msg : string }
  | Octez_client_error of {
      cmd_line_err : Command_line_type.command_line_error;
      primary_cause : string;
    }
  | Rpc_send_error of { url : string; e_sexp : Sexp.t }
  | Rpc_response_error of {
      path : string;
      code : int;
      body : string option;
      msg : string;
    }
  | Protocol_error of {
      cmd_type : Error_type.t;
      code : int;
      content_results : string;
    }
  | General_error of string
  | Unknown_error of string
[@@deriving sexp, variants, equal]

type tagged_babuso_error = babuso_error * string option [@@deriving sexp, equal]
type t = tagged_babuso_error [@@deriving sexp, equal]

exception Babuso_exn of tagged_babuso_error

let add_tag : babuso_error * string option -> string -> tagged_babuso_error =
 fun (bab_error, tag) addl_tag ->
  let new_tag =
    match tag with
    | Some t -> Some (t ^ " | " ^ addl_tag)
    | None -> Some addl_tag
  in
  (bab_error, new_tag)

let mk_tagged_error : ?tag:string -> babuso_error -> tagged_babuso_error =
 fun ?tag err -> (err, tag)

let reraise_with_tag (bab_error, tag) addl_tag =
  let new_bab_err = add_tag (bab_error, tag) addl_tag in
  raise (Babuso_exn new_bab_err)

let msg_only prefix msg = Fmt.str "%S - %S" prefix msg
let error_pp bab_error = Sexp.to_string (sexp_of_babuso_error bab_error)

let tagged_error_pp : ?addl_tag:string -> babuso_error * string option -> string
    =
 fun ?addl_tag (bab_error, tag) ->
  let new_error =
    match addl_tag with
    | Some t -> add_tag (bab_error, tag) t
    | None -> (bab_error, tag)
  in
  Sexp.to_string (sexp_of_tagged_babuso_error new_error)

(* let babuso_error_primary_cause : babuso_error -> string = *)
(*  fun be -> *)
(*   match be with *)
(*   | Octez_client_error {cmd_line_err; _} -> *)
(*       Command_line_type.primary_cause cmd_line_err.cmd_type cmd_line_err.std_err *)
(*   | _other -> "Primary cause not yet implemented" *)

let () =
  let f = function Babuso_exn (err, _) -> Some (error_pp err) | _ -> None in
  Caml.Printexc.register_printer f
