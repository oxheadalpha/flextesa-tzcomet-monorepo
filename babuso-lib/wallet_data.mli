open! Import

module Bytes_rep : sig
  type t = As_hex of string  (** without the 0x *)
  [@@deriving sexp, equal, variants, compare]

  val of_hex : string -> t
  val empty : t
  val of_binary : string -> t
  val of_binary_bytes : bytes -> t
  val to_binary : t -> string
  val to_zero_x : t -> string
  val to_zero_x_summary : ?limit:int -> ?chunk:int -> t -> string
  val to_hex : t -> string
  val to_pp : t -> 'a Pp.t
  val to_json : t -> Json.t
  val append : t -> t -> t
  val size : t -> int
  val hex_dump : t -> string
end

module B58 : sig
  type t = string [@@deriving sexp, equal, compare]
end

module Address : sig
  type t = B58.t [@@deriving sexp, equal, compare]
end

module Public_key : sig
  type t = B58.t [@@deriving sexp, equal, compare]
end

module Public_key_hash : sig
  type t = B58.t [@@deriving sexp, equal, compare]
end

module Michelson_expression : sig
  type t = Untyped of Michelson.t [@@deriving sexp, equal, variants, compare]

  val of_michelson : Michelson.t -> t
  val to_michelson : t -> Michelson.t
end

module Timestamp : sig
  type t = Seconds of Z.t [@@deriving sexp, equal, compare, variants]
end

module Signature : sig
  type t = B58.t [@@deriving sexp, equal, compare]

  val to_bytes : t -> Bytes_rep.t

  (* module Full : sig
       type t = {public_key: Public_key.t; blob: Bytes_rep.t; signature: t}
       [@@deriving sexp, equal]
     end *)

  module Draft : sig
    open Drafting

    module Data_hash : sig
      type t = H of string [@@deriving sexp, equal, compare]

      val digest_sexp : Sexp.t -> t
    end

    type hash = Data_hash.t [@@deriving sexp, equal, compare]
    type sig_draft = t option [@@deriving sexp, equal, compare]
    type sig_req = hash * Public_key.t [@@deriving sexp, equal, compare]

    type t = (sig_req * sig_draft) Collection.t
    [@@deriving sexp, equal, compare]

    (* val of_blob : (Bytes_rep.t, 'a) Import.List.Assoc.t -> Bytes_rep.t -> 'a option *)

    val add_or_replace : t -> hash:hash -> pk:string -> string -> unit

    module As_list : sig
      type t = (sig_req * sig_draft) List.t [@@deriving sexp, equal, compare]

      val get : t -> hash:hash -> pk:string -> string option

      val to_signature_list :
        t -> hash:hash -> keys:string list -> string option list
    end
  end
end

module Mutez : sig
  type t = Z.t

  val to_float : t -> float
  val to_tez_string : t -> string
end

module Michelson_type : sig
  type t = Lambda_unit_operations [@@deriving sexp, equal, compare, variants]
end

module Lambda_unit_operations : sig
  type t = Michelson of Michelson.t [@@deriving sexp, equal, variants]

  val to_michelson : t -> Michelson.t

  type lambda_t = t

  module Draft : sig
    open Drafting

    type op =
      | Transfer of {
          destination : string Reference.t;
          amount : string Reference.t;
        }
      | Delegation of { delegate : string Reference.t }
      | Fa2_transfer of {
          token_id_address : string Reference.t;
          token_id_number : string Reference.t;
          amount : string Reference.t;
          source : string Reference.t;
          destination : string Reference.t;
        }
    [@@deriving sexp, equal, variants, compare]

    type t = Structured of op Collection.t
    [@@deriving sexp, equal, variants, compare]

    val create : unit -> t
    val add_empty_transfer : t -> unit
    val add_empty_delegation : t -> unit
    val add_empty_fa2_transfer : t -> unit
  end
end

module Variable : sig
  module Name : sig
    type t = string [@@deriving sexp, equal]
  end

  val unique_name : string list -> string -> string

  (* TODO: When all the thrashing is done, see if all these types are still needed: *)
  module Type : sig
    type t =
      | Mutez
      | Address
      | Nat
      | Key_list
      | Weighted_key_list
      | Pkh_option
      | Bytes
      | Signature_option_list
      | Lambda_unit_to_operation_list
      | Time_span
      | Timestamp_option
      | Unit
      | Arbitrary of Michelson_expression.t
    [@@deriving sexp, variants, compare, equal]

    val of_michelson :
      (int, string) Tezos_micheline.Micheline.node -> t * string list

    val to_michelson : t -> Tezai_michelson.Untyped.t
    val pp : Formatter.t -> t -> unit
  end

  module Value : sig
    type t =
      | Address of Address.t
      | Mutez of Z.t
      | Nat of Z.t
      | Key_list of Public_key.t list
      | Weighted_key_list of (Z.t * Public_key.t) list
      | Unit
      | Some_pkh of Public_key_hash.t
      | Signature_option_list of Signature.t option list
      | Bytes of string
      | None_pkh
      | Lambda_unit_to_operation_list of Michelson.t
      | Time_span of Z.t
      | String of string
      | Timestamp_option of Timestamp.t option
      | Arbitrary_typed of {
          t : Michelson_expression.t;
          v : Michelson_expression.t;
        }
    [@@deriving sexp, variants, equal]

    val to_type : t -> Type.t
    val to_michelson : t -> Tezai_michelson.Untyped.t

    val of_michelson :
      Type.t -> (int, string) Tezos_micheline.Micheline.node -> t

    module Q : sig
      val nat : t -> Z.t
      val key_list : t -> string list
    end
  end

  module Map : sig
    type t = (Name.t * Value.t) list [@@deriving sexp, equal]

    val empty : t
    val of_list : 'a -> 'a
    val get_exn : (string, Value.t) Import.List.Assoc.t -> string -> Value.t
  end

  module Record : sig
    type t = Comb of (Name.t * Value.t) list
    [@@deriving sexp, variants, equal]

    val empty : t

    val of_map :
      (string, Value.t) Import.List.Assoc.t -> structure:string list -> t

    val to_michelson : t -> Tezai_michelson.Untyped.t
    val get : t -> string -> Value.t

    val update_from_normalized_michelson :
      t -> (int, string) Tezos_micheline.Micheline.node -> t

    val of_normalized_michelson_type_and_expression :
      t:(int, string) Import.Michelson.M.node ->
      v:(int, string) Import.Michelson.M.node ->
      t
  end

  module Parameter_type : sig
    type t = {
      structure : [ `Comb ]; [@default `Comb]
      fields : (string * Type.t) list; [@main]
    }
    [@@deriving sexp, fields, equal, make]

    val comb_of_normalized_michelson :
      (int, string) Tezos_micheline.Micheline.node -> t

    val get_comb : t -> (string * Type.t) list
  end
end

module Constant_or_variable : sig
  type 'a t = Constant of 'a | Variable of Variable.Name.t
  [@@deriving sexp, variants, equal]

  val collect_variables : 'a -> 'b t -> (string * 'a) list
end

module Control : sig
  type t =
    | Anyone
    | Weighted_multisig of {
        counter : Variable.Name.t;
        keys : (Z.t * Public_key.t) list Constant_or_variable.t;
        threshold : Z.t Constant_or_variable.t;
      }
    | Sender_is of Address.t Constant_or_variable.t
    | When_inactive of {
        last_activity : Variable.Name.t;
        timespan : Z.t Constant_or_variable.t;
      }
    | And of t list
    | Or of t list
  [@@deriving sexp, variants, equal]

  val key_param_id : int -> string
  val collect_variables : t -> (string * Variable.Type.t) list
  val collect_parameters : t -> (string * Variable.Type.t) list
end

module Ability : sig
  type t =
    | Call_operations_lambda
    | Transfer_mutez of {
        amount : [ `No_limit | `Limit of Mutez.t Constant_or_variable.t ];
        destination :
          [ `Anyone | `Address of Address.t Constant_or_variable.t | `Sender ];
      }
    | Set_delegate
    | Reset_activity_timer of Variable.Name.t
    | Sequence of t list
    | Vote
    | Do_nothing
    | Boomerang
    | Update_variables of (Variable.Name.t * Variable.Type.t) list
    | Generic_multisig_main
  [@@deriving sexp, variants, equal]

  val collect_variables : t -> (string * Variable.Type.t) list
  val collect_parameters : t -> (string * Variable.Type.t) list
end

module Entrypoint : sig
  type t = { name : string; [@main] control : Control.t; ability : Ability.t }
  [@@deriving sexp, fields, make, equal]

  val collect_parameters : t -> (string * Variable.Type.t) list
  val collect_variables : t -> (string * Variable.Type.t) list
end

module Foreign_contract : sig
  type t = {
    entrypoints : (string * Michelson.t) list;
    storage_type : Michelson.t; [@default Michelson.C.t_unit]
    storage_content : Michelson.t; [@default Michelson.C.e_unit]
  }
  [@@deriving sexp, fields, make, equal, compare]
end

module Smart_contract : sig
  type t =
    | Custom of {
        entrypoints : Entrypoint.t list (* ; fully_recognized: bool *);
        can_receive_funds : bool;
        variables : Variable.Record.t;
      }
    | Foreign of Foreign_contract.t
    | Generic_multisig of { variables : Variable.Record.t }
  [@@deriving sexp, variants, equal]

  val make :
    ?can_receive_funds:bool ->
    ?variables:Variable.Record.t ->
    Entrypoint.t list ->
    t

  val collect_variables : t -> (string * Variable.Type.t) list
end

module Originated_contract : sig
  type t = { address : Address.t; [@main] contract : Smart_contract.t }
  [@@deriving sexp, fields, make, equal]
end

module Key_pair : sig
  type backend =
    | Ledger of { uri : string }
    | Unencrypted of { private_uri : string }
  [@@deriving sexp, variants, equal]

  type t = {
    public_key_hash : Public_key_hash.t;
    public_key : Public_key.t;
    backend : backend; [@main]
  }
  [@@deriving sexp, fields, make, equal]

  val octez_client_sk_uri : backend -> string
end

module Friend : sig
  type t = {
    public_key_hash : Public_key_hash.t;
    public_key : Public_key.t option;
  }
  [@@deriving sexp, fields, make, equal]
end

module Human_prose : sig
  type t = Raw_markdown of string [@@deriving sexp, variants, compare, equal]
end

module Account : sig
  module Draft : sig
    open Drafting

    module Internal_id : sig
      type t = V of string * int [@@deriving sexp, variants, equal, compare]

      val fresh : unit -> t
      val next : t -> t
      val pp : Formatter.t -> t -> unit
    end

    module Role_id = Internal_id
    module Timer_id = Internal_id
    module Spending_limit_id = Internal_id
    module Team_id = Internal_id

    module Capability : sig
      module Control : sig
        type t =
          | Anyone
          | Sender_is of Role_id.t Option.t Reference.t
          | Or of t Option.t Reference.t Collection.t
          | And of t Option.t Reference.t Collection.t
          | Timer_expires of Timer_id.t Option.t Reference.t
          | Team_acknowledges of Team_id.t Option.t Reference.t
        [@@deriving sexp, variants, equal, compare]
      end

      module Action : sig
        type t =
          | Transfer of {
              amount_limit : Spending_limit_id.t Option.t Reference.t;
              destination : Role_id.t Option.t Reference.t;
            }
          | Set_delegate
          | Set_variable of Internal_id.t Option.t Reference.t
          | Reset_timer of Timer_id.t Option.t Reference.t
          | Do_nothing
        [@@deriving sexp, variants, equal, compare]

        val empty_transfer : unit -> t
      end

      type t = {
        name : string Reference.t;
        control : Control.t Option.t Reference.t;
        actions : Action.t Reference.t Collection.t;
      }
      [@@deriving sexp, make, fields, equal, compare]

      val create_empty : unit -> t
    end

    module Meta_variable (Value_type : sig
      type t [@@deriving sexp, equal, compare]

      val empty : unit -> t
    end) : sig
      type t = {
        id : Internal_id.t;
        public_name : string Reference.t;
        initial_value : Value_type.t Reference.t;
      }
      [@@deriving sexp, fields, equal, compare]

      val create_empty : unit -> t
    end

    module Value_string : sig
      type t = string

      val empty : unit -> string
    end

    module Role : sig
      type t = {
        id : Internal_id.t;
        public_name : string Reference.t;
        initial_value : Value_string.t Reference.t;
      }
      [@@deriving sexp, fields, equal, compare]

      val create_empty : unit -> t
    end

    module Timer : sig
      type t = {
        id : Internal_id.t;
        public_name : string Reference.t;
        initial_value : Value_string.t Reference.t;
      }
      [@@deriving sexp, fields, equal, compare]

      val create_empty : unit -> t
    end

    module Spending_limit : sig
      type t = {
        id : Internal_id.t;
        public_name : string Reference.t;
        initial_value : Value_string.t Reference.t;
      }
      [@@deriving sexp, fields, equal, compare]

      val create_empty : unit -> t
    end

    module Team_draft : sig
      type t =
        | Percentages of {
            keys : (Role_id.t option Reference.t * int Reference.t) Collection.t;
          }
      [@@deriving sexp, variants, equal, compare]

      val empty : unit -> t
    end

    module Team : sig
      type t = {
        id : Internal_id.t;
        public_name : string Reference.t;
        initial_value : Team_draft.t Reference.t;
      }
      [@@deriving sexp, fields, equal, compare]

      val create_empty : unit -> t
    end

    type t =
      | From_address_or_uri of string Reference.t
      | Generic_multisig of {
          gas_wallet : string Reference.t;
          threshold : string Reference.t;
          public_keys : string list Reference.t;
        }
      | Custom_contract of {
          gas_wallet : string Reference.t;
          has_default_do_nothing : bool Reference.t;
          capabilities : Capability.t Collection.t;
          roles : Role.t Collection.t;
          timers : Timer.t Collection.t;
          spending_limits : Spending_limit.t Collection.t;
          teams : Team.t Collection.t;
        }
    [@@deriving sexp, variants, compare, equal]

    val create_from_address_or_uri : ?uri:string -> unit -> t
    val create_generic_multisig : unit -> t
    val create_custom_contract : unit -> t
  end

  module Status : sig
    type t =
      | Originated_contract of Originated_contract.t
      | Key_pair of Key_pair.t
      | Friend of Friend.t
      | Contract_to_originate of {
          operation : string;
          contract : Originated_contract.t option;
        }
      | Contract_failed_to_originate of {
          operation : string;
          contract : Originated_contract.t option;
        }
      | Draft of Draft.t
    [@@deriving sexp, variants, equal]
  end

  type t = {
    id : string;
    display_name : string;
    balance : Z.t option;
    state : Status.t; [@main]
    history : Status.t list; [@default []]
    comments : Human_prose.t option;
  }
  [@@deriving sexp, fields, make, equal]

  val get_address : t -> string option
  val get_public_key : t -> string option
  val get_secret_key : t -> Key_pair.backend option
  val can_be_gas_wallet : t -> bool
  val can_sign : t -> bool
  val is_draft : t -> bool

  val multisig_parameters :
    t -> < counter : Z.t ; keys : string list ; threshold : Z.t >

  val custom_contract_entrypoint :
    entrypoint:string ->
    t ->
    < entrypoint : Entrypoint.t ; variables : Variable.Record.t >

  val foreign_contract_entrypoint :
    entrypoint:string ->
    t ->
    < entrypoint_type : Michelson.t ; foreign_contract : Foreign_contract.t >
end

module Mempool_section : sig
  type t =
    | Applied
    | Refused
    | Outdated
    | Branch_refused
    | Branch_delayed
    | Unprocessed
  [@@deriving sexp, equal, variants]

  val is_applied : t -> bool
end

module Mempool_error : sig
  type t = Unknown of Json.t [@@deriving sexp, equal, variants, compare]

  val of_json : Json.t -> t
end

module Tezos_operation_error : sig
  module Kind : sig
    type t =
      | Empty_transaction
      | Non_existing_contract of string
      | Balance_too_low of { contract : string; balance : Z.t; amount : Z.t }
      | Storage_exhausted
      | Ill_typed_michelson of {
          expected_type : Michelson.t;
          expression : Michelson.t;
        }
      | Syntax_error_in_michelson of {
          expected_from_protocol : string;
          expression : Michelson.t;
        }
      | Contract_runtime_error of { address : string }
      | Contract_hit_failwith of { location : int; message : Michelson.t }
      | Unknown of Json.t
    [@@deriving sexp, variants, equal]

    val of_error_json : Json.t -> t
  end

  type t = { kind : Kind.t; raw : Json.t [@main] }
  [@@deriving sexp, fields, equal, make]

  val of_errors_json : Json.t -> t list
end

module Simulation_result : sig
  module Balance_update : sig
    type t =
      | Account of { address : string; change : Z.t }
      | Burn of { category : string; change : Z.t }
    [@@deriving sexp, variants, equal]
  end

  module Operation_result : sig
    type t = {
      status : [ `Applied | `Failed | `Backtracked ];
      errors : Tezos_operation_error.t list;
      balance_updates : Balance_update.t list;
      consumed_milligas : Z.t option;
      storage_size : Z.t option;
      allocated_destination_contract : bool option;
      paid_storage_size_diff : Z.t option;
      originated_contracts : string list;
      internal_operations : t list;
    }
    [@@deriving sexp, fields, make, equal]

    val total_milligas : t -> Z.t
  end

  type t = { operations : Operation_result.t list; [@main] raw : Json.t }
  [@@deriving sexp, equal, make, fields]

  val originated_contracts : t -> string list
  val total_milligas : t -> Z.t
  val total_paid_storage_diff : t -> Z.t
  val all_applied : t -> bool
  val all_errors : t -> Tezos_operation_error.t list
end

(** For now we keep the weird name because we link with Flextesa which already
    owns the {!Tezos_protocol} name. *)
module Tzrotocol : sig
  module Kind : sig
    type t = Ithaca | Jakarta | Alpha
    [@@deriving sexp, variants, compare, equal]

    val canonical_hash : t -> string
    val all_with_hashes : (t * string) list
    val name : t -> string
  end

  type t = { kind : Kind.t; [@main] hash : string option }
  [@@deriving sexp, make, equal, compare, fields]

  val of_hash : string -> t
end

module Tezos_operation : sig
  type t =
    | Transfer of {
        source : Account.t;
        amount : Z.t;
        destination : Address.t;
        entrypoint : string;
        parameters : Michelson.t option;
      }
    | Origination of {
        source : Account.t;
        balance : Z.t;
        code : Michelson.t;
        storage_initialization : Michelson.t;
      }
    | Reveal of { account : Account.t }
  [@@deriving sexp, variants, equal]

  val source : t -> Account.t
end

module Tezos_batch : sig
  type op = {
    specification : Tezos_operation.t; [@main]
    fee : Z.t; [@default Z.zero]
    gas_limit : Z.t; [@default Z.of_int 1_040_000]
    storage_limit : Z.t; [@default Z.of_int 60_000]
    counter : Z.t;
  }
  [@@deriving sexp, fields, make, equal]

  val fake_signature : string

  type t = {
    branch : string;
    protocol : Tzrotocol.t;
    signature : Signature.t; [@default fake_signature]
    chain_id : string;
    operations : op list; [@main]
  }
  [@@deriving sexp, fields, make, equal]

  val unbatch : t -> t list
  val to_json : [< `For_forge | `For_simulation ] -> t -> Json.t
end

module Simulation_failure : sig
  type t = {
    which : [ `Computing_fees | `Final ];
    simulation : Simulation_result.t;
    batch : Tezos_batch.t;
  }
  [@@deriving fields, make, sexp, equal]

  val to_pp : t -> 'a Pp.t

  exception E of t

  val raise :
    [ `Computing_fees | `Final ] ->
    simulation:Simulation_result.t ->
    batch:Tezos_batch.t ->
    'a

  val kinds_of_exn : exn -> Tezos_operation_error.Kind.t list
  val exn_has_kind : exn -> f:(Tezos_operation_error.Kind.t -> bool) -> bool
end

module Operation : sig
  type id = string [@@deriving sexp, equal]

  module Draft : sig
    open Drafting

    module Parameter_draft : sig
      type v =
        | Address of string Reference.t
        | Amount of string Reference.t
        | Of_type of Variable.Type.t * string Reference.t
      [@@deriving sexp, variants, compare, equal]

      type t = { name : string; v : v [@main] }
      [@@deriving sexp, fields, compare, equal, make]

      val of_parameter : id * Variable.Type.t -> t
    end

    (* module Kind : sig *)
    type t =
      | Simple_transfer of {
          source_account : string Reference.t;
          destination_address : string Reference.t;
          amount : string Reference.t;
        }
      | Call_generic_multisig_update_keys of {
          contract_address : Address.t;
          gas_wallet : string Reference.t;
          threshold : string Reference.t;
          public_keys : string list Reference.t;
          signatures : Signature.Draft.t;
        }
      | Call_generic_multisig_main of {
          contract_address : Address.t;
          gas_wallet : string Reference.t;
          lambda : Lambda_unit_operations.Draft.t Reference.t;
          signatures : Signature.Draft.t;
        }
      | Call_custom_contract_entrypoint of {
          contract_address : Address.t;
          entrypoint : string;
          gas_wallet : string Reference.t;
          parameters : Parameter_draft.t List.t;
        }
      | Call_foreign_contract_entrypoint of {
          contract_address : Address.t;
          entrypoint : string;
          gas_wallet : string Reference.t;
          parameters : Parameter_draft.t List.t;
        }
    [@@deriving sexp, variants, compare, equal]

    val create_call_generic_multisig_update_keys :
      ?threshold:Z.t -> ?public_keys:id list -> address:id -> unit -> t

    val create_call_generic_multisig_main : address:id -> unit -> t

    val create_simple_transfer :
      ?source:id -> ?destination:id -> ?amount:id -> unit -> t

    val create_call_custom_contract_entrypoint :
      address:id ->
      entrypoint:id ->
      parameters:(id * Variable.Type.t) list ->
      unit ->
      t

    val create_call_foreign_contract_entrypoint :
      address:id ->
      entrypoint:id ->
      parameters:(id * Variable.Type.t) list ->
      unit ->
      t

    val destination_address : t -> id option
  end

  module Specification : sig
    type t =
      | Origination of {
          account : string;
          specification : Smart_contract.t;
          initialization : Variable.Map.t;
          gas_wallet : string;
        }
      | Transfer of {
          source : string;
          destination : Address.t;
          amount : Mutez.t;
          entrypoint : string;
          parameters : (string * Variable.Value.t) list;
        }
      | Draft of Draft.t
    [@@deriving sexp, variants, equal]
  end

  module Error : sig
    type t =
      | Stuck_in_the_mempool of
          Mempool_section.t * Mempool_error.t list * [ `Expired | `Non_expired ]
      | Operation_lost
      | Simulation_failure of Simulation_failure.t
      | Unknown of Sexp.t
    [@@deriving sexp, variants, equal]
  end

  module Status : sig
    type tezos_operation = {
      operation_hash : string;
      base_block_hash : string;
      inclusion_block_hashes : (string * bool) list;
    }
    [@@deriving sexp, fields, make, equal]

    type t =
      | Paused
      | Ordered
      | Work_in_progress of {
          op : tezos_operation option;
          simulation_result : Simulation_result.t option;
          mempool_section : Mempool_section.t option;
          mempool_errors : Mempool_error.t list;
        }
      | Success of { op : tezos_operation }
      | Failed of Error.t
    [@@deriving sexp, variants, equal]

    val failed_sexpable : ('a -> Sexp.t) -> 'a -> t
    (** We shall slowly get rid of this function: *)
  end

  type t = {
    id : id;
    order : Specification.t;
    status : Status.t;
    status_history : (float * Status.t) list;
    last_update : float;
    comments : Human_prose.t option;
  }
  [@@deriving sexp, fields, make, equal]

  val same_id : t -> t -> bool
  val different_id : t -> t -> bool
  val compare_by_update : t -> t -> int
  val is_draft : t -> bool
  val with_status : t -> Status.t -> t

  val with_status_work_in_progress :
    ?simulation_result:Simulation_result.t ->
    ?tezos_operation:Status.tezos_operation ->
    ?mempool_errors:Mempool_error.t list ->
    ?mempool_section:Mempool_section.t ->
    t ->
    t

  val with_status_error : t -> Error.t -> t
  val with_status_error_sexpable : t -> ('a -> Sexp.t) -> 'a -> t
  val with_status_success : t -> tezos_operation:Status.tezos_operation -> t
  val new_draft : Draft.t -> t
  val is_old_and_uninteresting : t -> now:float -> bool
  val get_source_account_id : t -> id option
  val get_destination_address : t -> id option
  val get_amount : t -> Z.t option
end

module Signing_request : sig
  type metadata =
    [ `From_address of Address.t
    | `Comment of string * Human_prose.t
    | `Operation_draft of Operation.Draft.t ]
  [@@deriving sexp, variants, compare, equal]

  type t [@@deriving sexp, compare, equal]

  val make : ?metadata:metadata list -> public_key:string -> Bytes_rep.t -> t
  val blob : t -> Bytes_rep.t
  val public_key : t -> string
  val metadata : t -> metadata list
end

module Currency : sig
  type t = [ `USD | `EUR | `BTC | `ETH ]
  [@@deriving sexp, variants, compare, equal]

  val to_symbol : t -> string
  val to_unit : t -> float * string
end

module Exchange_rates : sig
  type t = {
    timestamp : float;
    btc : float;
    eur : float;
    usd : float;
    eth : float;
  }
  [@@deriving sexp, fields]

  val get_currency : t -> Currency.t -> float
end

module Tezos_node : sig
  type t = { id : string; base_url : string [@main] }
  [@@deriving sexp, compare, equal, make, fields]

  val make_new : string -> t

  module Status : sig
    type problem =
      [ `Timeouts_observed
      | `Wrong_chain_id of string * string
      | `No_chain_id_consensus
      | `Head_is_behind ]
    [@@deriving sexp, compare, equal, variants]

    type t = {
      health : [ `Fine | `Unknown | `Injured | `Dead ]; [@default `Unknown]
      problems : problem list;
      head : (int * string) option;
      chain_id : string option;
      version : string option;
    }
    [@@deriving make, sexp, compare, equal, fields]

    val add_problem : t -> problem -> t
  end
end

module Network : sig
  type t = Mainnet | Ghostnet | Kathmandunet | Sandbox of string
  [@@deriving variants, compare, equal, sexp]

  val of_chain_id : string -> t
  val all_public_chain_ids : string list
end

module Network_information : sig
  type t = {
    nodes : (Tezos_node.t * Tezos_node.Status.t) list;
    network : Network.t option;
  }
  [@@deriving fields, compare, equal, sexp, make]

  val fine_nodes : t -> (int * string * string) list
end

module Ledger_nano : sig
  module Device : sig
    type t = { animals : string; wallet_app_version : string option }
    [@@deriving sexp, equal, compare, fields, make]
  end

  module Curve_derivation : sig
    type t = Bip25519 | Ed25519 | Secp256k1 | P256
    [@@deriving sexp, equal, compare, variants]

    val all : t list
    val to_path : t -> string
  end
end

module Uri_analysis : sig
  module Fact : sig
    type t =
      | Balance of Z.t
      | Address of Address.t
      | Public_key of Public_key.t
    [@@deriving sexp, equal, compare, variants]
  end

  type t = {
    input : string; [@main]
    valid : bool; [@default true]
    facts : Fact.t list;
  }
  [@@deriving sexp, equal, compare, make, fields]
end
