let watermark = "\x05"

let pack mich =
  watermark
  ^ (Data_encoding.Binary.to_bytes_exn Untyped.expr_encoding
       (Untyped.to_canonical_micheline mich)
    |> Bytes.to_string)
