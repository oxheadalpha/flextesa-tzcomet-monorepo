type t = (int, string) Tezos_micheline.Micheline.node

val of_canonical_micheline : string Tezos_micheline.Micheline.canonical -> t
val to_canonical_micheline : t -> string Tezos_micheline.Micheline.canonical

val of_micheline_node :
  ?make_location:('a -> int) -> ('a, string) Tezos_micheline.Micheline.node -> t

val pp : Format.formatter -> t -> unit

(** Construct Micheline/Michelson expressions. *)
module C : sig
  val int : Z.t -> t
  val inti : int -> t
  val string : string -> t
  val bytes : bytes -> t
  val prim : ?annotations:string list -> string -> t list -> t
  val seq : t list -> t
  val t_or : t list -> t
  val t_unit : t
  val t_nat : t
  val t_string : t
  val t_pair : t list -> t
  val t_mutez : t
  val t_operation : t
  val t_option : t -> t
  val t_list : t -> t
  val t_lambda : t -> t -> t
  val t_key_hash : t
  val t_key : t
  val t_address : t
  val t_entrypoints : (string * t) list -> t
  val e_unit : t
  val e_none : t
  val e_some : t -> t
  val seq_map : 'a list -> f:('a -> t) -> t
  val e_right : t -> t
  val e_left : t -> t
  val e_pair : t -> t -> t
  val push : t -> t -> t
  val fail_with : string -> t list
  val assert_some : t
  val script : parameter:t -> storage:t -> t list -> t
  val concrete : string -> t
end

(** Match on Micheline/Michelson expressions. *)
module M : sig
  type ('l, 'p) node = ('l, 'p) Tezos_micheline.Micheline.node =
    | Int of 'l * Z.t
    | String of 'l * string
    | Bytes of 'l * bytes
    | Prim of
        'l * 'p * ('l, 'p) Tezos_micheline.Micheline.node list * string list
    | Seq of 'l * ('l, 'p) Tezos_micheline.Micheline.node list
end

val encoding : t Data_encoding.t
(** Encoding compatible with the “real” thing; it does not not normalize the the
    reppresentation for the binary serialization to be the same as ["PACK"] or
    ["tezos-client hash data ..."]. *)

val of_json : Ezjsonm.value -> t
val to_json : t -> Ezjsonm.value

val primitives : (string * string) list
(** The Michelson primitives from the protocol, converted to strings. *)

val expr_encoding : string Tezos_micheline.Micheline.canonical Data_encoding.t
