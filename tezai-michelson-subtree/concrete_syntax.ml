module List = ListLabels
open Tezos_micheline

let parse_micheline ~check_indentation ~check_primitives m =
  let rec primitive_check =
    let open Micheline in
    function
    | Prim (_, s, args, _) ->
        (match
           List.find_opt Untyped.primitives ~f:(fun (p, _) -> String.equal p s)
         with
        | Some _ -> ()
        | None -> Fmt.failwith "Unknown primitive: %S" s);
        List.iter args ~f:primitive_check
    | _ -> ()
  in
  match Micheline_parser.tokenize m with
  | tokens, [] -> (
      match
        Micheline_parser.parse_expression ~check:check_indentation tokens
      with
      | node, [] -> (
          try
            if check_primitives then primitive_check node;
            Ok
              (Untyped.of_micheline_node
                 ~make_location:(fun loc -> loc.Micheline_parser.start.byte)
                 node)
          with e -> Error [ Tezos_error_monad.Error_monad.Exn e ])
      | _, errs -> Error errs)
  | _, errs -> Error errs

let parse_exn ~check_indentation ~check_primitives m =
  match parse_micheline ~check_indentation ~check_primitives m with
  | Ok o -> o
  | Error e ->
      Fmt.failwith "parse_micheline: %a"
        Tezos_error_monad.Error_monad.pp_print_error e

let to_string c =
  Fmt.str "%a" Micheline_printer.print_expr
    (Micheline_printer.printable Base.Fn.id (Untyped.to_canonical_micheline c))

(* let micheline_node_to_string node =
   micheline_canonical_to_string (Micheline.strip_locations node) *)
