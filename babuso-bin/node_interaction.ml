open! Import
open Babuso_lib
open Wallet_data

type t = {
  branch_age : int; [@default 2]  (** This is octez-client's default *)
}
[@@deriving sexp, fields, make, equal]

let _get ctxt : t = ctxt#node_interaction

let prepare_operation_simulation ctxt node operation =
  let { branch_age } = _get ctxt in
  let head_minus_age =
    Octez_node.get_block ctxt node (`Head_minus branch_age)
  in
  let next_protocol = Octez_node.next_protocol ctxt node |> Tzrotocol.of_hash in
  let branch_level =
    Json.Q.(head_minus_age |> field ~k:"header" |> int_field ~k:"level")
  in
  let branch_hash = Json.Q.(head_minus_age |> string_field ~k:"hash") in
  let branch_chain_id = Json.Q.(head_minus_age |> string_field ~k:"chain_id") in
  let source_account = Tezos_operation.source operation in
  let source_address =
    match Account.get_address source_account with
    | Some s -> s
    | None ->
        Failure.raise
          Docpp.(
            text "Source Account does not have an address:"
            +++ sexpable Account.sexp_of_t source_account)
  in
  let pk = Octez_node.get_revealed_key ctxt node ~address:source_address in
  let source_counter =
    Octez_node.get_counter ctxt node ~address:source_address |> Z.succ
  in
  let the_batch =
    let open Tezos_batch in
    let ops =
      match pk with
      | None ->
          [
            make_op
              (Tezos_operation.reveal ~account:source_account)
              ~counter:source_counter;
            make_op ~counter:(Z.succ source_counter) operation;
          ]
      | Some _ -> [ make_op ~counter:source_counter operation ]
    in
    make ~protocol:next_protocol ~branch:branch_hash ops
      ~chain_id:branch_chain_id
  in
  dbgp
    Docpp.(
      text "prepare_operation"
      +++ itemize
            [
              textf "level %d" branch_level;
              textf "branch hash %s" branch_hash;
              textf "chain: %s" branch_chain_id;
              textf "revealed key: %s" (Option.value ~default:"NONE" pk);
              textf "whole-batch:"
              +++ Json.to_pp (Tezos_batch.to_json `For_simulation the_batch);
            ]);
  the_batch

let simulate_batch ctxt node the_batch =
  let simulation_result =
    Octez_node.simulate_operation ctxt node
      (Tezos_batch.to_json `For_simulation the_batch)
  in
  (* From here on, we throw exceptions: *)
  let results_of_simulation =
    let parse_result_object ?internal_operations result =
      let open Json.Q in
      let status =
        match string_field ~k:"status" result with
        | "failed" -> `Failed
        | "applied" -> `Applied
        | "backtracked" -> `Backtracked
        | _ ->
            Failure.raise
              Docpp.(text "Unknown operation status" +++ Json.to_pp result)
      in
      let errors =
        try Tezos_operation_error.of_errors_json (field ~k:"errors" result)
        with _ -> []
      in
      let balance_updates =
        (try field ~k:"balance_updates" result |> list with _ -> [])
        |> List.map ~f:(fun update_obj ->
               match string_field update_obj ~k:"kind" with
               | "contract" ->
                   Simulation_result.Balance_update.account
                     ~address:(string_field update_obj ~k:"contract")
                     ~change:(z_field ~k:"change" update_obj)
               | "burned" ->
                   Simulation_result.Balance_update.burn
                     ~category:(string_field update_obj ~k:"category")
                     ~change:(z_field ~k:"change" update_obj)
               | other ->
                   Failure.raise
                     Docpp.(
                       textf "Unknown kind of balance update: %s" other
                       +++ Json.to_pp result))
      in
      let consumed_milligas =
        try Some (z_field result ~k:"consumed_milligas") with _ -> None
      in
      let originated_contracts =
        try string_list_field result ~k:"originated_contracts" with _ -> []
      in
      let storage_size =
        try Some (z_field result ~k:"storage_size") with _ -> None
      in
      let allocated_destination_contract =
        try Some (bool_field result ~k:"allocated_destination_contract")
        with _ -> None
      in
      let paid_storage_size_diff =
        try Some (z_field result ~k:"paid_storage_size_diff") with _ -> None
      in
      Simulation_result.Operation_result.make ~balance_updates ~errors
        ?internal_operations ?allocated_destination_contract ~status
        ?paid_storage_size_diff ?storage_size ?consumed_milligas
        ~originated_contracts ()
    in
    Simulation_result.make ~raw:simulation_result
      Json.Q.(
        field ~k:"contents" simulation_result
        |> list
        |> List.map ~f:(fun content_obj ->
               let metadata = field ~k:"metadata" content_obj in
               let result = field metadata ~k:"operation_result" in
               let internal_operations =
                 try
                   list (field metadata ~k:"internal_operation_results")
                   |> List.map ~f:(fun op ->
                          field op ~k:"result" |> parse_result_object)
                 with _ -> []
               in
               parse_result_object ~internal_operations result))
  in
  dbgp
    Docpp.(
      text "result of simulation:"
      +++ sexpable Simulation_result.sexp_of_t results_of_simulation);
  let unbatched = Tezos_batch.unbatch the_batch in
  let forge_results =
    List.map unbatched ~f:(fun batch ->
        Octez_node.forge_operations ctxt node
          (Tezos_batch.to_json `For_forge batch))
  in
  dbgp
    Docpp.(
      text "Unbatched forge results:"
      +++ itemize (List.map ~f:Bytes_rep.to_pp forge_results));
  (results_of_simulation, forge_results)

(* Taken from Octez: docs/alpha/plugins.rst:213 *)
let default_minimal_fees = Z.of_int 100
let default_minimal_nanotez_per_gas_unit = Z.of_int 100
let default_minimal_nanotez_per_byte = Z.of_int 1000

let set_fees _ctxt ?(override_op = Fn.id) the_batch results_of_simulation blobs
    =
  (* See src/proto_alpha/lib_client/injection.ml:755 *)
  let fixed_batch =
    let first_one = ref true in
    let open Tezos_batch in
    {
      the_batch with
      operations =
        List.map3_exn the_batch.operations
          (Simulation_result.operations results_of_simulation) blobs
          ~f:(fun op opresult blob ->
            let blob_size = Bytes_rep.size blob in
            let milligas =
              Simulation_result.Operation_result.total_milligas opresult
            in
            let storage_size =
              Simulation_result.Operation_result.storage_size opresult
              |> Option.value ~default:Z.zero
            in
            let allocation_size = Z.of_int 257 in
            let allocation_of_destination_size =
              Simulation_result.Operation_result.allocated_destination_contract
                opresult
              |> Option.value_map
                   ~f:(function true -> allocation_size | false -> Z.zero)
                   ~default:Z.zero
            in
            let originated_contracts_size =
              Simulation_result.Operation_result.originated_contracts opresult
              |> List.fold ~init:Z.zero ~f:(fun size _ ->
                     Z.add size allocation_size)
            in
            let thousand = Z.of_int 1_000 in
            let gas = Z.(div milligas thousand) in
            let nanotez =
              let minimal = Z.mul thousand default_minimal_fees in
              let for_gas = Z.mul default_minimal_nanotez_per_gas_unit gas in
              let for_serialization =
                let actual_blob_size =
                  let signature_size = 64 in
                  let shell_header_size = 32 (* A block hash (?) *) in
                  if not !first_one then blob_size
                  else (
                    first_one := false;
                    blob_size + signature_size + shell_header_size)
                in
                Z.mul default_minimal_nanotez_per_byte
                  (Z.of_int actual_blob_size)
              in
              let total = Z.add minimal (Z.add for_gas for_serialization) in
              dbgp
                Docpp.(
                  Fmt.kstr verbatim
                    "Computed fees: min:%a + gas:%a + size:%a = %a" Z.pp_print
                    minimal Z.pp_print for_gas Z.pp_print for_serialization
                    Z.pp_print total);
              total
            in
            override_op
              {
                op with
                gas_limit = Z.(add (of_int 2) gas);
                storage_limit =
                  Z.(
                    add
                      (add allocation_of_destination_size
                         originated_contracts_size)
                      (add storage_size one));
                fee = Z.(add one (div nanotez thousand));
              });
    }
  in
  dbgp
    Docpp.(text "fixed batch:" +++ sexpable Tezos_batch.sexp_of_t fixed_batch);
  fixed_batch

let initiate_operation ctxt ?override_op node operation =
  let batch = prepare_operation_simulation ctxt node operation in
  let simulation, forges = simulate_batch ctxt node batch in
  let () =
    if not (Simulation_result.all_applied simulation) then
      Simulation_failure.raise `Computing_fees ~simulation ~batch
  in
  let batch = set_fees ctxt ?override_op batch simulation forges in
  let forge_result =
    (* We forge again with the right fees! *)
    Octez_node.forge_operations ctxt node (Tezos_batch.to_json `For_forge batch)
  in
  dbgp Docpp.(text "Forge result:" +++ Bytes_rep.to_pp forge_result);
  let source_account = Tezos_operation.source operation in
  let signature =
    Signer.sign_bytes ~watermark:Signer.Watermark.operation ctxt source_account
      forge_result
  in
  dbgp Docpp.(text "Signature result:" +++ verbatim signature);
  let finalized_batch = { batch with Tezos_batch.signature } in
  let simulation, _ = simulate_batch ctxt node finalized_batch in
  let () =
    if not (Simulation_result.all_applied simulation) then
      Simulation_failure.raise `Final ~simulation ~batch:finalized_batch
  in
  let decoded = Signature.to_bytes signature in
  dbgp Docpp.(text "Signature as hex:" +++ Bytes_rep.to_pp decoded);
  let operation_hash =
    Octez_node.inject_operation ctxt node
      (Bytes_rep.append forge_result decoded)
  in
  dbgp Docpp.(text "Injection, operation ID:" +++ verbatim operation_hash);
  let originated_contracts =
    Simulation_result.originated_contracts simulation
    |> List.mapi ~f:(fun idx simaddr ->
           let real_addr =
             Tezai_base58_digest.Identifier.Kt1_address.of_base58_operation_hash
               ~index:(Int32.of_int_exn idx) operation_hash
           in
           dbgp
             Docpp.(
               textf "Contract %s from simulation becomes actually %S." simaddr
                 real_addr);
           real_addr)
  in
  object
    method operation_hash = operation_hash
    method batch = finalized_batch
    method simulation = simulation
    method originated_contracts = originated_contracts
  end

let get_level block =
  Json.Q.(block |> field ~k:"header" |> int_field ~k:"level")

let find_operation_in_block ctxt node ~operation_hash ~branch_level ~head_level
    ~ttl =
  let rec rabbit_hole level =
    if level <= branch_level then (
      dbgp Docpp.(text "Operation not in a block");
      `Not_in_a_block)
    else
      let block = Octez_node.get_block ctxt node (`Level level) in
      let operations =
        Json.Q.(list (field block ~k:"operations") |> List.concat_map ~f:list)
      in
      match
        List.find operations ~f:(fun op ->
            String.equal operation_hash (Json.Q.string_field ~k:"hash" op))
      with
      | Some _ ->
          let block_hash = Json.Q.string_field block ~k:"hash" in
          dbgp Docpp.(text "Operation found in" +++ verbatim block_hash);
          `In_block (block_hash, head_level - level > 0)
      | None -> rabbit_hole (level - 1)
  in
  rabbit_hole (min head_level (branch_level + ttl))

let find_initiated_operation ctxt node ~operation_hash ~operation_branch =
  let ttl =
    let constants = Octez_node.get_constants ctxt node in
    constants#max_operations_time_to_live
  in
  (* let branch_hash = op#batch |> Tezos_batch.branch in *)
  let branch_block = Octez_node.get_block ctxt node (`Hash operation_branch) in
  let branch_level = get_level branch_block in
  let head_block = Octez_node.get_block ctxt node `Head in
  let head_level = get_level head_block in
  let expiredness =
    if head_level > branch_level + ttl then `Expired else `Non_expired
  in
  match
    find_operation_in_block ctxt node ~operation_hash ~ttl ~branch_level
      ~head_level
  with
  | `Not_in_a_block -> (
      let mempool = Octez_node.get_mempool ctxt node in
      match
        List.find_map mempool#contents ~f:(fun (loc, oplist) ->
            match
              List.find oplist ~f:(fun opo ->
                  String.equal opo#hash operation_hash)
            with
            | Some op -> Some (loc, op#errors)
            | None -> None)
      with
      | Some (loc, errors) -> `In_mempool (loc, errors, expiredness)
      | None -> `Not_found expiredness)
  | `In_block _ as i -> i
