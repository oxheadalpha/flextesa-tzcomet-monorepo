open! Import

(** {1 Tests of The Backend Using Flextesa}

    This module provides the command `run-backend-tests` (in {!With_flextesa}).

    See {i some} of the usage documentation at the beginning of
    ["./please.sh bb run-backen --help"].

    Test scenarios are added with

    {[
      let () =
        let open Make_scenario in
        add "choose-a-good-name" @@ fun scenario ->
        (*  ...  *)
        (* Attention here: accessing state from scenario steps should be made lazy! *)
        ()
    ]}

    See the ones at the end of the file.

    {!Make_scenario.add} makes up the {!Scenario.t} and add it to a global list.
    {!With_flextesa.run}:

    - starts a sandbox (using flextesa {b as a library!}),
    - starts the Babuso backend (within Flextesa's project management),
    - and then runs all the scenarios (modulo filters like ["--only-matching"]),
    - the end is like usual Flextesa scenarios, they obey the interactivity
      command-line options, if it becomes interactive, there are a couple of
      custom babuso-specific commands.

    Note that {!With_flextesa} should be the only module using {!Lwt} (inherited
    from Flextesa). *)

let test_the_test =
  try String.equal (Caml.Sys.getenv "testthetest") "true" with _ -> false

(** Tests which are not “protocol” interactions with Babuso, they are
    lower-level system tests which require for instance a sandbox. *)
module Low_level_tests = struct
  open Babuso_lib
  open Wallet_data

  let basic_threads_module ctxt =
    let trace = ref [] in
    let start = Unix.gettimeofday () in
    let t v =
      trace :=
        let relative = Unix.gettimeofday () -. start in
        let quantized = relative *. 3. |> Float.to_int in
        dbgf "trace %s %.2f → %d" v relative quantized;
        (quantized, v) :: !trace
    in
    let tf fmt = Fmt.kstr t fmt in
    t "start";
    let delay n = Unix.sleepf (Float.of_int n *. 0.4) in
    let delay_lwt n = Lwt_unix.sleep (Float.of_int n *. 0.4) in
    let _t0 =
      Threads.spawn ctxt (fun () ->
          delay 1;
          t "t0";
          delay 4;
          t "t0-2")
    in
    tf "t0 is started";
    delay 2;
    tf "main-0";
    let _t1 =
      Threads.spawn ctxt (fun () ->
          delay 4;
          t "t1")
    in
    tf "t1 is started";
    delay 1;
    (let exception Should_not_happen in
    try
      let () =
        Threads.lwt_run ctxt
          Lwt.(
            fun () ->
              let base () =
                delay_lwt 1 >>= fun () ->
                t "lwt0";
                delay_lwt 2 >>= fun () ->
                t "lwt1";
                Lwt.fail Should_not_happen
              in
              Lwt.pick [ base (); delay_lwt 2 ])
      in
      delay 2;
      t "lwt-ok";
      ()
    with
    | Should_not_happen -> t "THIS SHOULD NOT HAPPEN"
    | e -> tf "lwt-run-catch: %a" Exn.pp e);
    delay 2;
    t "end";
    let show_trace tr =
      List.map tr ~f:(fun (t, v) -> Fmt.str "(%d, %S)" t v)
      |> String.concat ~sep:";\n  "
    in
    let full_trace = List.rev !trace in
    let expected =
      [
        (0, "start");
        (0, "t0 is started");
        (1, "t0");
        (2, "main-0");
        (2, "t1 is started");
        (4, "lwt0");
        (6, "t0-2");
        (7, "t1");
        (8, "lwt-ok");
        (10, "end");
      ]
    in
    if Poly.equal full_trace expected then ()
    else (
      Fmt.epr
        "basic_threads_module test failed:\n\n\
         let full_trace = [\n\
        \  %s\n\
         ]\n\n\
         let expected = [\n\
        \  %s\n\
         ]\n\n\
         %!"
        (show_trace full_trace) (show_trace expected);
      Fmt.failwith "basic_threads_module test failed")

  let test_node_interaction_00 ctxt =
    let raises msg f ~exn =
      try
        f () |> ignore;
        Failure.raise
          Docpp.(
            textf "test_node_interaction_00: %s was supposed to raise." msg)
      with
      | e when exn e -> ()
      | other ->
          Failure.raise
            Docpp.(
              textf
                "test_node_interaction_00: %s was supposed to raise something \
                 else than:"
                msg
              +++ Exception.to_pp other)
    in
    let first_account =
      (* First account we find that is a keypair with a balance. *)
      Option.value_exn
        ~message:
          "test_node_interaction_00 test requires at least one account to be \
           configured"
        (Backend_state.Accounts.all ctxt
        |> List.find ~f:(fun acc ->
               Account.can_sign acc
               &&
               match Account.balance acc with
               | Some b when Z.gt b Z.one -> true
               | _ -> false))
    in
    let the_chosen_node =
      (* We cannot use the regular function because the tests are not
         running the monitoring. *)
      let all =
        Backend_state.configuration ctxt |> Wallet_configuration.nodes
      in
      List.find all ~f:(fun n ->
          String.is_prefix
            (Wallet_data.Tezos_node.base_url n)
            ~prefix:"http://localhost:")
      |> Option.value_exn ~message:"Could not find the sandbox node"
    in
    let basic_transfer ?(add_message = Docpp.nop) ?override_op
        ?(entrypoint = "default") ?parameters ?(source = first_account) ~amount
        ~destination () =
      dbgp
        Docpp.(
          Fmt.kstr verbatim "Test-basic-transfer to %s." destination
          ++ cut
          ++ itemize
               [
                 text "Amount:" +++ sexpable Z.sexp_of_t amount;
                 text "Parameters:"
                 +++ sexpable (Option.sexp_of_t Michelson.sexp_of_t) parameters;
               ]
          +++ add_message);
      Node_interaction.initiate_operation ?override_op ctxt the_chosen_node
        (Tezos_operation.transfer ~source ~amount
           ~destination (* "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" *)
           ~entrypoint ~parameters)
    in
    let origination ?override_op ?(source = first_account) ?(balance = Z.zero)
        ~code ~init () =
      Node_interaction.initiate_operation ?override_op ctxt the_chosen_node
        (Tezos_operation.origination ~source ~balance ~code
           ~storage_initialization:init)
    in
    let destination = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" in
    raises "empty-transaction"
      ~exn:
        (Simulation_failure.exn_has_kind
           ~f:Tezos_operation_error.Kind.(equal empty_transaction))
      (basic_transfer
         ~add_message:Docpp.(text "Empty transaction")
         ~amount:Z.zero ~destination);
    let op =
      basic_transfer
        ~add_message:Docpp.(text "A whole tez")
        ~amount:(Z.of_int 1_000_000) ~destination ()
    in
    let rec wait_for_op ~expect op =
      let loc =
        let operation_hash = op#operation_hash in
        let operation_branch = Tezos_batch.branch op#batch in
        Node_interaction.find_initiated_operation ctxt the_chosen_node
          ~operation_hash ~operation_branch
      in
      match loc with
      | `In_block (hash, final) ->
          let msg =
            Docpp.(
              textf "Operation %s is in %s (%s)" op#operation_hash hash
                (if final then "final" else "not final"))
          in
          expect op ~msg loc;
          ()
      | `Not_found `Expired ->
          let msg = Docpp.(textf "Operation %s LOST" op#operation_hash) in
          expect op ~msg loc;
          ()
      | `Not_found `Non_expired ->
          let msg = Docpp.(textf "Operation %s kinda LOST" op#operation_hash) in
          expect op ~msg loc;
          ()
      | `In_mempool (section, _, _) ->
          let msg =
            Docpp.(
              textf "Operation %s in the mempool:" op#operation_hash
              +++ sexpable Mempool_section.sexp_of_t section)
          in
          Unix.sleepf 1.;
          if Mempool_section.is_applied section then wait_for_op op ~expect
          else expect op ~msg loc
    in
    let op_is_in_block _op ~msg = function
      | `In_block _ -> ()
      | _ -> Failure.raise Docpp.(text "Operation is not in a block," +++ msg)
    in
    let op_is_refused _op ~msg = function
      | `In_mempool (Mempool_section.Refused, _, _) -> ()
      | _ -> Failure.raise Docpp.(text "Operation is not “Refused,”" +++ msg)
    in
    wait_for_op op ~expect:op_is_in_block;
    let op_low_fee =
      basic_transfer ~amount:(Z.of_int 1_000_000) ~destination ()
        ~add_message:Docpp.(text "Low fee")
        ~override_op:Tezos_batch.(fun op -> { op with fee = Z.of_int 10 })
    in
    wait_for_op op_low_fee ~expect:op_is_refused;
    let hic_et_nunc =
      (* one on mainnet *) "KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton"
    in
    raises "wrong-destination"
      ~exn:
        (Simulation_failure.exn_has_kind
           ~f:
             Tezos_operation_error.Kind.(
               equal (non_existing_contract hic_et_nunc)))
      (basic_transfer
         ~add_message:Docpp.(text "Wrong destination")
         ~amount:Z.one ~destination:hic_et_nunc);
    let address = Account.get_address first_account |> Option.value_exn in
    let balance = Octez_node.get_balance ctxt the_chosen_node ~address in
    let amount = Z.(add balance one) in
    raises "wrong-amount"
      ~exn:
        (Simulation_failure.exn_has_kind
           ~f:
             Tezos_operation_error.Kind.(
               equal (balance_too_low ~balance ~contract:address ~amount)))
      (basic_transfer
         ~add_message:Docpp.(text "Balance too low")
         ~amount ~destination);
    let module Mich = Tezai_michelson.Untyped.C in
    let oporig =
      (* https://smartpy.io/ide?code=eJyFksFKAzEQhu95iiFesloX2mOhIIhPIHgNw@5sDWSTkEyLfXsnu2tbq2iOyZdv_vzkDl45ZoI39AeCR3j5wDF5giFmcN4fCmdkdyRIh5xioQIx@FOrlBtTzAxlxMzpBFigJKU6j6XMysloSmqfYxBJx81WgayeBrDWBcfWmkJ@WMGxostxXXW3rYQp1dTP4XYLpybuScwk4pNN0QU@qz0NvGgTZhzLrbdHxva7dwbbSf@3Pbv9@6z_V3u_g43UNIBmkkqRqWgIkcEFeX_Akazdnodh31sh2NQDiaQvHermPH4iriZ3a0Gv2l5vmkuqjgJmF4UQf71pv7bMT6p9Xxt99RX0L6KHnQyc@1kSd3FMTp7mYrCMeU9sdLkEX91ka9Qn7KrC0A-- *)
      let code =
        Mich.(
          script
            ~parameter:(t_entrypoints [ ("left", t_nat); ("right", t_unit) ])
            ~storage:(prim "nat" [])
            [
              prim "UNPAIR" [];
              prim "IF_LEFT"
                [
                  seq [ prim "SWAP" []; prim "DROP" [] ];
                  seq
                    [
                      prim "DROP" [];
                      prim "PUSH" [ t_nat; inti 2 ];
                      prim "SWAP" [];
                      prim "MUL" [];
                    ];
                ];
              prim "NIL" [ prim "operation" [] ];
              prim "PAIR" [];
            ])
      in
      let init = Mich.(int Z.one) in
      origination ~balance:Z.one ~code ~init ()
    in
    let tassert msg f =
      if f () then ()
      else Failure.raise Docpp.(textf "Test-assertion %S failed." msg)
    in
    let no_exn msg f =
      try f ()
      with e ->
        Failure.raise
          Docpp.(textf "Test step %S raised:" msg +++ Exception.to_pp e)
    in
    let contract_address =
      no_exn "there is a contract adddress" (fun () ->
          match oporig#originated_contracts with
          | [ one ] -> one
          | _ -> assert false)
    in
    tassert "some storage was paid for" (fun () ->
        let orig_simulation = oporig#simulation in
        Z.gt (Simulation_result.total_paid_storage_diff orig_simulation) Z.one);
    wait_for_op oporig ~expect:op_is_in_block;
    let opcall =
      basic_transfer ~amount:Z.zero ~destination:contract_address
        ~add_message:Docpp.(text "Entrypoint `left`, should succeed")
        ~entrypoint:"left"
        ~parameters:(Mich.int (Z.of_int 42))
        ()
    in
    wait_for_op opcall ~expect:op_is_in_block;
    ()

  let all () =
    [
      ("basic-threads", basic_threads_module);
      ("test-00-nodes-interaction", test_node_interaction_00);
    ]

  let run_all ~backend_data_dir =
    let ctxt =
      let ctxt =
        object
          method file_io = Io.File.default ()
        end
      in
      Backend_state.load ctxt backend_data_dir
    in
    List.iter (all ()) ~f:(fun (n, f) ->
        try f ctxt
        with e -> Fmt.failwith "Low-level test: %S failed: %a" n Exn.pp e);
    ()

  let command () =
    let open Cmdliner in
    let open Term in
    let term =
      const (fun ctxt choices ->
          match choices with
          | [] ->
              List.iter (all ()) ~f:(fun (_, f) -> f ctxt);
              ()
          | more ->
              List.iter more ~f:(fun prefix ->
                  match
                    List.find (all ()) ~f:(fun (n, _) ->
                        String.is_prefix n ~prefix)
                  with
                  | Some (n, f) ->
                      Fmt.epr "Running %S (for choice %S)\n%!" n prefix;
                      f ctxt
                  | None -> Fmt.failwith "No test matching %S\n" prefix))
      $ Backend_state.loading_cmdliner_term ()
      $ Arg.(value (pos_all string [] (info [] ~doc:"Pick tests to run.")))
    in
    ( term,
      Cmd.info "run-low-level-system-tests"
        ~doc:"Run just the “low-level/system” backend-tests." )
end

module Up_message = Babuso_lib.Protocol.Message.Up
module Down_message = Babuso_lib.Protocol.Message.Down
open Babuso_lib
open Wallet_data

module Run_backend = struct
  type t = {
    base_command : string; [@default "babuso"]
    data_dir : string;
    demo_mode : bool;
    configuration : Wallet_configuration.t;
  }
  [@@deriving make, sexp, compare, equal]

  let init_and_make_command self =
    let config_path = Path.(self.data_dir // "configuration") in
    let ctxt =
      object
        method shell = Io.Shell.default ()
        method file_io = Io.File.default ()
      end
    in
    Io.File.make_path ctxt (Path.dirname config_path);
    Io.File.write_lines ctxt config_path
      [ Wallet_configuration.sexp_of_t self.configuration |> Sexp.to_string ];
    Fmt.str
      "while babuso_debug_color=none %s start --state '%s' %s ; do echo \
       restart ; done"
      self.base_command self.data_dir
      (if self.demo_mode then "--listen-addr 0.0.0.0" else "")
end

module Scenario = struct
  module Test_failure = struct
    type t = { reason : string; data : (string * Sexp.t) list [@main] }
    [@@deriving make, sexp]
  end

  module Context = struct
    type t = < fail : Test_failure.t -> unit >

    let fail (ctxt : t) ?(data = []) reason =
      ctxt#fail (Test_failure.make ~reason data)

    let fail_wrong_message (ctxt : t) down =
      fail ctxt "Wrong down message"
        ~data:[ ("message", Down_message.sexp_of_t down) ]

    let check ctxt msg ?data cond =
      if not cond then fail ctxt ?data (Fmt.str "Check failed: %s" msg)
  end

  module Direct = struct
    module Context = struct
      type t =
        < make_failure : Test_failure.t -> exn
        ; client :
            ?wait:string ->
            string list ->
            < err : string list
            ; out : string list
            ; status : Unix.process_status >
        ; call_babuso : Up_message.t -> Down_message.t
        ; bake : unit >
    end
  end

  module Step = struct
    type t =
      | Fail of (unit -> string * (string * Sexp.t) list)
      | Call_babuso of {
          query : Up_message.t;
          check : Context.t -> Down_message.t -> unit;
        }
      | Lazy of (unit -> t list)
      | Call_babuso_until of {
          attempts : int;
          sleep : float;
          bake_attempts : bool;
          query : Up_message.t;
          until : Context.t -> Down_message.t -> unit;
        }
      | Direct of (Direct.Context.t -> unit)
      | Bake
    [@@deriving variants]
  end

  type t = { name : string; [@main] steps : Step.t list } [@@deriving make]
end

module All_scenarios = struct
  let _all : Scenario.t list ref = ref []
  let add name steps = _all := Scenario.make name ~steps :: !_all
  let get () = List.rev !_all
end

module With_flextesa = struct
  open Flextesa
  open Internal_pervasives

  let failf fmt = Fmt.kstr (fun s -> fail (`Scenario_error s)) fmt
  let ktez i = Int64.(of_int i * 1_000L * 1_000_000L)

  let gas_accounts =
    List.init 20 ~f:(fun i ->
        (Fmt.kstr Tezos_protocol.Account.of_name "babuso-user-%d" i, ktez 2))

  exception Pass_failure of Scenario.Test_failure.t [@@deriving sexp]

  exception
    Pass_error of
      [ `Process_error of Process_result.Error.error
      | `System_error of [ `Fatal ] * System_error.static ]

  let run_all_low_level_tests state ~babuso_data_dir =
    try
      System_error.catch
        (fun () ->
          Lwt_preemptive.detach
            (fun () ->
              Low_level_tests.run_all ~backend_data_dir:babuso_data_dir;
              ())
            ())
        ()
    with e ->
      Console.sayf state
        Fmt.(fun ppf () -> pf ppf "Low-level tests failure: %a" Exn.pp e)
      >>= fun () -> System_error.fail_fatalf "low-level tests failed :("

  let run state ~do_low_level_tests ?babuso_base_command ~base_port ~node_exec
      ~client_exec ~demo_mode ~baker_exec ~accuser_exec ~restrict_scenarios
      ~protocol_kind ~webdriver_tests () =
    let baker_account = Tezos_protocol.Account.of_name "babuso-baker" in
    let protocol =
      let open Tezos_protocol in
      let d = default () in
      let hash = Protocol_kind.canonical_hash protocol_kind in
      {
        d with
        kind = protocol_kind;
        hash;
        bootstrap_accounts = (baker_account, ktez 20_000) :: gas_accounts;
      }
    in
    Helpers.clear_root state >>= fun () ->
    Helpers.System_dependencies.precheck state `Or_fail
      ~protocol_kind:protocol.Tezos_protocol.kind
      ~executables:
        ([ node_exec; client_exec ]
        @ if state#test_baking then [ baker_exec; accuser_exec ] else [])
    >>= fun () ->
    Test_scenario.network_with_protocol ~protocol ~size:1 ~base_port state
      ~node_exec ~client_exec
    >>= fun (nodes, _protocol) ->
    let client = Tezos_client.of_node ~exec:client_exec (List.hd_exn nodes) in
    let baker_client =
      Tezos_client.Keyed.make client ~key_name:"babuso-baker"
        ~secret_key:(Tezos_protocol.Account.private_key baker_account)
    in
    Tezos_client.Keyed.initialize state baker_client >>= fun _ ->
    Tezos_client.Keyed.bake state baker_client "first-bake" >>= fun () ->
    Tezos_client.rpc state
      ~client:(Tezos_client.of_node (List.hd_exn nodes) ~exec:client_exec)
      `Get ~path:"/chains/main/chain_id"
    >>= fun chain_id_json ->
    let level () =
      Test_scenario.Queries.all_levels state ~nodes >>= function
      | [ (_, `Level one) ] -> return one
      | other -> failf "all_levels returned %d values" (List.length other)
    in
    level () >>= fun initial_level ->
    let network_id =
      match chain_id_json with `String s -> s | _ -> assert false
    in
    let babuso_port = base_port + 10 in
    let babuso =
      let data_dir =
        Paths.root state // Fmt.str "babuso-%d" babuso_port // "data-dir"
      in
      let nodes =
        [
          Wallet_data.Tezos_node.make_new
            (Fmt.str "http://localhost:%d" base_port);
          Wallet_data.Tezos_node.make_new
            (Fmt.str "http://deadnode.example.com:4242");
        ]
      in
      let configuration =
        let octez_client_executable =
          Tezos_executable.get client_exec ~protocol_kind:protocol.kind
        in
        Wallet_configuration.make ~octez_client_executable ~port:babuso_port
          ~max_connections:32 ~fake_tezos_client:true ~nodes ()
      in
      Run_backend.make ?base_command:babuso_base_command ~data_dir ~demo_mode
        ~configuration ()
    in
    let babuso_command = Run_backend.init_and_make_command babuso in
    let babuso_backend =
      Running_processes.Process.make_in_session "babuso-backend"
        (`Process_group_script babuso_command) [ "sh" ]
    in
    Running_processes.start state babuso_backend >>= fun babuso_backend_state ->
    let babuso_backend_state = ref babuso_backend_state in
    let stderr_path =
      Running_processes.output_path state
        !babuso_backend_state.Running_processes.State.process `Stderr
    in
    Interactive_test.Pauser.add_commands state
      Interactive_test.Commands.(
        all_defaults state ~nodes
        @ [
            bake_command state ~clients:[ baker_client ];
            secret_keys state ~protocol;
            forge_and_inject_piece_of_json state ~clients:[ baker_client ];
            unit_loop_no_args
              ~description:(Fmt.str "Get info on the Babuso running here.")
              [ "b"; "babuso" ] (fun () ->
                Console.sayf state
                  Fmt.(
                    fun ppf () ->
                      pf ppf "@[<v>Babuso:";
                      pf ppf "@,Base command: `%s`" babuso.base_command;
                      pf ppf "@,Data-dir: %s" babuso.data_dir;
                      pf ppf "@,Port: %d" babuso.configuration.port;
                      pf ppf "@,Stderr: %s" stderr_path;
                      pf ppf "@,PID: %d"
                        !babuso_backend_state.Running_processes.State.lwt#pid;
                      pf ppf "@,GUI: http://localhost:%d"
                        babuso.configuration.port;
                      pf ppf "@]";
                      ()));
            unit_loop_no_args
              ~description:(Fmt.str "Visit Babuso's STDERR with $EDITOR")
              [ "vbo"; "view-babuso-output" ] (fun () ->
                System.editor state >>= fun editor ->
                Fmt.kstr (System.command state) "%s %s" editor stderr_path
                >>= fun _ -> return ());
            unit_loop_no_args ~description:(Fmt.str "Restart Babuso.")
              [ "rb"; "restart-babuso" ] (fun () ->
                Running_processes.kill state !babuso_backend_state >>= fun () ->
                Running_processes.start state !babuso_backend_state.process
                >>= fun new_state ->
                babuso_backend_state := new_state;
                Console.sayf state
                  Fmt.(
                    fun ppf () ->
                      pf ppf "@[<v>Babuso:";
                      pf ppf "@,Base command: `%s`" babuso.base_command;
                      pf ppf "@,Data-dir: %s" babuso.data_dir;
                      pf ppf "@,Port: %d" babuso.configuration.port;
                      pf ppf "@,Stderr: %s" stderr_path;
                      pf ppf "@,PID: %d"
                        !babuso_backend_state.Running_processes.State.lwt#pid;
                      pf ppf "@,GUI: http://localhost:%d"
                        babuso.configuration.port;
                      pf ppf "@]";
                      ()));
            unit_loop_no_args
              ~description:"(Re)-run the low-level backend tests."
              [ "lt"; "low-level-tests" ] (fun () ->
                run_all_low_level_tests state ~babuso_data_dir:babuso.data_dir);
          ]
        @ arbitrary_commands_for_each_and_all_clients state ~clients:[ client ]);
    let call message =
      let body = Up_message.sexp_of_t message |> Sexp.to_string_mach in
      let url =
        Fmt.str "http://localhost:%d/ui-protocol"
          babuso.configuration.Wallet_configuration.port
      in
      Running_processes.run_successful_cmdf state
        "curl --silent --show-error -H 'Content-type: text/plain' --data %s %s"
        (Path.quote body) (Path.quote url)
      >>= fun result ->
      match
        Down_message.t_of_sexp
          (Sexplib.Sexp.of_string (String.concat ~sep:"\n" result#out))
      with
      | v -> return v
      | exception _ ->
          System_error.fail_fatalf "Calling %a failed with %a" Sexp.pp_hum
            (Up_message.sexp_of_t message)
            Fmt.(list ~sep:cut string)
            result#out
    in
    Helpers.wait_for state ~attempts:10
      ~seconds:(fun () -> return 0.5)
      (fun _ ->
        Asynchronous_result.bind_on_result (call Up_message.get_configuration)
          ~f:(function
          | Ok _ -> return (`Done ())
          | Error _ -> return (`Not_done "waiting for babuso")))
    >>= fun () ->
    (* Down_message.blob *)
    System.read_file state stderr_path >>= fun stderr_content ->
    Console.sayf state
      Fmt.(
        fun ppf () ->
          pf ppf "Level: %d, Network: %s,@ Babuso PID: %d,@ stderr: %s@."
            initial_level network_id
            !babuso_backend_state.Running_processes.State.lwt#pid stderr_path;
          pf ppf "Stderr:@.";
          pf ppf "%s@."
            (try
               String.subo stderr_content
                 ~pos:(String.length stderr_content - 180)
             with _ -> stderr_content))
    >>= fun () ->
    let failures = ref [] in
    let direct_client =
      Tezos_client.of_node (List.hd_exn nodes) ~exec:client_exec
    in
    let run_direct ~add_failure f =
      (*
let () =
        Ppx_sexp_conv_lib.Conv.Exn_converter.add
          [%extension_constructor Pass_failure] (function
          | Pass_failure m ->
              Sexp.List
                [Sexp.Atom "Pass_failure"; Scenario.Test_failure.sexp_of_t m]
          | _ -> assert false ) in
 *)
      (* let scenario_fail f = raise (Pass_failure f) in *)
      let run_in_main f =
        Lwt_preemptive.run_in_main
          Lwt.(
            fun () ->
              f () >>= fun { Attached_result.result; attachments = _ } ->
              match result with
              | Ok o -> Lwt.return o
              | Error e -> raise (Pass_error e)
            (* scenario_fail
               (Scenario.Test_failure.make ~reason:"run_in_main TODO" []) *))
      in
      let ctxt : Scenario.Direct.Context.t =
        object
          method client ?wait args =
            run_in_main (fun () ->
                Tezos_client.successful_client_cmd state ~client:direct_client
                  ?wait args)

          method call_babuso up = run_in_main (fun () -> call up)

          method bake =
            run_in_main (fun () ->
                Tezos_client.Keyed.bake state baker_client "direct-bake")

          method make_failure x = Pass_failure x
        end
      in
      Lwt_preemptive.detach
        (fun () ->
          try Attached_result.ok (f ctxt) with
          | Pass_failure tf ->
              add_failure tf;
              Attached_result.error `Test_failed
          | Pass_error ((`Process_error _ | `System_error _) as e) ->
              Attached_result.error e)
        ()
    in
    let run_scenario name steps =
      let open Scenario in
      Asynchronous_result.bind_on_result ~f:(function
        | Ok () -> return ()
        | Error `Test_failed -> return ()
        | Error ((`Process_error _ | `System_error _) as err) -> fail err)
      @@ List_sequential.iteri steps ~f:(fun ith step ->
             let call_babuso query check maybe_add_failure =
               call query >>= fun down ->
               let exception Pass_failure of Test_failure.t in
               let context =
                 object
                   method fail tf = raise (Pass_failure tf)
                 end
               in
               try
                 check context down;
                 return ()
               with
               | Pass_failure tf ->
                   maybe_add_failure tf;
                   fail `Test_failed
               | other_exn ->
                   maybe_add_failure
                     (Test_failure.make ~reason:"Unhandled exception"
                        [ ("exception", Exn.sexp_of_t other_exn) ]);
                   fail `Test_failed
             in
             let add_failure test_failure =
               failures := (name, ith, test_failure) :: !failures
             in
             let rec run_step = function
               | Step.Fail f ->
                   let reason, data = f () in
                   add_failure (Test_failure.make ~reason data);
                   fail `Test_failed
               | Step.Lazy f -> List_sequential.iter ~f:run_step (f ())
               | Direct f -> run_direct ~add_failure f
               | Step.Call_babuso { query; check } ->
                   call_babuso query check add_failure
               | Step.Call_babuso_until
                   { query; until; attempts; bake_attempts; sleep } ->
                   let last_failure = ref None in
                   let rec attempt_call nth =
                     if nth <= attempts then
                       Asynchronous_result.bind_on_result
                         (call_babuso query until (fun tf ->
                              last_failure := Some tf))
                         ~f:(function
                           | Ok () -> return ()
                           | Error `Test_failed ->
                               System.sleep sleep >>= fun () ->
                               (if bake_attempts then
                                Tezos_client.Keyed.bake state baker_client
                                  (Fmt.str "%s@%d@attempt-%d" name ith nth)
                               else return ())
                               >>= fun () -> attempt_call (nth + 1)
                           | Error ((`Process_error _ | `System_error _) as err)
                             ->
                               fail err)
                     else (
                       add_failure
                         (Option.value !last_failure
                            ~default:
                              (Test_failure.make
                                 ~reason:"Meehh?? Is this a bug???" []));
                       fail `Test_failed)
                   in
                   attempt_call 1
               | Bake ->
                   Tezos_client.Keyed.bake state baker_client
                     (Fmt.str "%s@%d" name ith)
             in
             run_step step)
    in
    let all_scenarios =
      let all = All_scenarios.get () in
      match restrict_scenarios with
      | [] -> all
      | more ->
          List.filter all ~f:(fun scen ->
              List.exists more ~f:(fun pat -> Re.execp pat scen.name))
    in
    List_sequential.iteri all_scenarios ~f:(fun ith scenario ->
        Console.sayf state
          Fmt.(fun ppf () -> pf ppf "Scenario %d: %s" ith scenario.name)
        >>= fun () -> run_scenario scenario.name scenario.steps)
    >>= fun () ->
    begin
      begin
        if state#test_baking then
          let baker_daemon =
            let key = baker_client.Tezos_client.Keyed.key_name in
            let client = baker_client.Tezos_client.Keyed.client in
            Tezos_daemon.baker_of_node (List.hd_exn nodes) ~key ~protocol_kind
              ~exec:baker_exec ~client
          in
          Running_processes.start state
            (Tezos_daemon.process state baker_daemon)
          >>= fun { process = _; lwt = _ } -> return ()
        else return ()
      end
      >>= fun () ->
      (if do_low_level_tests then
       run_all_low_level_tests state ~babuso_data_dir:babuso.data_dir
      else return ())
      >>= fun () ->
      begin
        match webdriver_tests with
        | `No_webdriver_tests -> return ()
        | `Run_webdriver_tests_with base_command ->
            let geckodriver_port = 4455 in
            let geckodriver_process =
              let command = Fmt.str "geckodriver --port %d" geckodriver_port in
              Running_processes.Process.make_in_session "geckodriver"
                (`Process_group_script command) [ "sh" ]
            in
            Console.sayf state
              Fmt.(fun ppf () -> pf ppf "Starting the geckodriver process.")
            >>= fun () ->
            Running_processes.start state geckodriver_process >>= fun _state ->
            Console.sayf state
              Fmt.(fun ppf () -> pf ppf "Running the webdriver test.")
            >>= fun () ->
            Running_processes.run_successful_cmdf state
              "%s --babuso-port %d --geckodriver-host http://127.0.0.1:%d"
              base_command babuso_port geckodriver_port
            >>= fun res ->
            Console.sayf state
              Fmt.(
                fun ppf () ->
                  pf ppf "Webdriver test:@.";
                  List.iter res#out ~f:(fun line -> pf ppf "  | %s@." line);
                  ())
      end
      >>= fun () ->
      match !failures with
      | [] -> Console.sayf state Fmt.(const string "All Good & Done \\o/")
      | more ->
          Console.sayf state
            Docpp.(
              fun ppf () ->
                to_fmt ppf
                  (box
                     (text "There were test failures:"
                     +++ itemize
                           (List.map more ~f:(fun (name, ith, failure) ->
                                textf "%s[%d]: %s" name ith failure.reason
                                +++ itemize
                                      (List.map failure.data ~f:(fun (k, v) ->
                                           textf "%s:" k +++ sexp v)))))))
          >>= fun () -> fail (`Scenario_error "tests failed :(")
    end

  let command () =
    let open Cmdliner in
    let open Term in
    let pp_error = Test_command_line.Common_errors.pp in
    let base_state =
      Test_command_line.Command_making_state.make
        ~application_name:"Babuso-test" ~command_name:"runtest" ()
    in
    let docs = Manpage_builder.section_test_scenario base_state in
    let term =
      const
        (fun
          (`Restrict_scenarios restrict_scenarios)
          (`Do_low_level_tests do_low_level_tests)
          webdriver_tests
          protocol_kind
          babuso_base_command
          demo_mode
          base_port
          node_exec
          client_exec
          (* admin_exec *)
            baker_exec
          accuser_exec
          state
        ->
          let threads_ctxt =
            let threads = Threads.init () in
            object
              method threads = threads
            end
          in
          (* Backend_state.load ctxt babuso_data_dir in *)
          let actual_test =
            run state ~demo_mode ~babuso_base_command ~base_port ~node_exec
              ~client_exec ~baker_exec ~accuser_exec ~restrict_scenarios
              ~protocol_kind ~do_low_level_tests ~webdriver_tests
          in
          Test_command_line.Run_command.or_hard_fail
            ~lwt_run:(fun l -> Threads.lwt_run threads_ctxt (fun () -> l))
            state ~pp_error
            (Interactive_test.Pauser.run_test ~pp_error state actual_test))
      $ Arg.(
          const (fun l ->
              `Restrict_scenarios
                (List.map l ~f:(fun s ->
                     Re.Posix.(compile (re ~opts:[ `NoSub ] s)))))
          $ value
              (opt_all string []
                 (info [ "only-matching" ]
                    ~doc:
                      "Restrict scenarios with POSIX regular expressions \
                       (multiple options are allowed: OR semantics).")))
      $ Arg.(
          const (fun flag -> `Do_low_level_tests (not flag))
          $ value
              (flag
                 (info [ "no-low-level-tests" ]
                    ~doc:"Do not run the low-level tests.")))
      $ Arg.(
          const (function
            | None -> `No_webdriver_tests
            | Some bin -> `Run_webdriver_tests_with bin)
          $ value
              (opt (some string) None
                 (info [ "run-webdriver-tests" ] ~docv:"EXEC"
                    ~doc:"Run the Webdriver tests.")))
      $ Tezos_protocol.Protocol_kind.cmdliner_term ~default:`Kathmandu
          ~docs:"OPTIONS" ()
      $ Arg.(
          value
            (opt string "dune exec babuso-bin/babuso.exe -- "
               (info [ "babuso-command" ] ~doc:"The command to run babuso")))
      $ Arg.(value (flag (info [ "demo-mode" ] ~doc:"")))
      $ Arg.(
          value & opt int 20_000
          & info [ "base-port"; "P" ] ~docs
              ~doc:"Base port number to build upon.")
      $ Tezos_executable.cli_term base_state `Node "tezos"
      $ Tezos_executable.cli_term base_state `Client "tezos"
      $ Tezos_executable.cli_term base_state `Baker "tezos"
      $ Tezos_executable.cli_term base_state `Accuser "tezos"
      $ Test_command_line.Full_default_state.cmdliner_term base_state ()
    in
    let info =
      let doc = "Run Babuso tests with Flextesa." in
      let man : Manpage.block list =
        let pf fmt = Fmt.kstr (fun p -> [ `P p ]) fmt in
        let pre l = [ `Pre (String.concat ~sep:"\n" l) ] in
        Manpage_builder.make base_state
          ~intro_blob:
            "This meta-test builds a small sandbox network, starts Babuso, and \
             then runs  various scenarios using the “UI-Protocol” a.k.a. \
             `Babuso_lib.Protocol`."
        @@ pf "Example:"
        @ pre
            [
              "PATH=/path/to/octez-binaries:\\$PATH dune exec \
               babuso-bin/babuso.exe \\\\\n\
              \    run-backend-tests --only-matching 'basic:add-key-pair'";
            ]
        @ pf
            "There is an environment variable: `testthetest` which can be set \
             to `true` to force the tests to have failures (for instance to \
             look at how the failures are displayed)."
        @ pf "Available tests (`testthetest` being %b):" test_the_test
        @ pre
            (List.map (All_scenarios.get ()) ~f:(fun scen ->
                 Fmt.str "* %s: %d steps" scen.name (List.length scen.steps)))
        @ pf
            "Flextesa's interactivity can be used to setup a sandbox with \
             babuso running (the command `b` also helps):"
        @ pre
            [
              "PATH=...  ./please.sh bb run-backend \\\\\n\
              \    --pause-at-end=true --only doeijdedelijde";
            ]
        @ pf
            "→ Starts running tests that match doeijdedelijde (there are \
             none), and when done, enters Flextesa's interactive mode:"
        @ pre
            [
              "* Command `b` gives the link to GUI (among other things).";
              "* Command `rb` attempts to restart Babuso.";
              Fmt.str
                "* Command `vbo` opens Babuso's stderr file in \\$EDITOR (%s)."
                (match Caml.Sys.getenv "EDITOR" with
                | ed -> Fmt.str "= %S" ed
                | exception _ -> Fmt.str "Not set: will try `nano` or `vi`");
            ]
        @ pf
            "Also not that within Flextesa Babuso is running in a `while` \
             loop, once the test is in interactive mode and everything is \
             running, one can restart Babuso with:"
        @ pre [ "curl http://localhost:20010/please/die" ]
        @ pf "Where 20010 is the default port (option `--base-port` + 10)."
      in
      Cmd.info "run-backend-tests" ~man ~doc
    in
    (term, info)
end

(** The main API to add tests togther with {!Scenario.Context}. *)
module Make_scenario = struct
  type scenario_context = < add_step : Scenario.Step.t -> unit >

  let with_scenario_context f =
    let steps = ref [] in
    let context =
      object
        method add_step step = steps := step :: !steps
      end
    in
    f context;
    List.rev !steps

  let add name f =
    let steps = with_scenario_context f in
    All_scenarios.add name steps

  let fail (self : scenario_context) f = self#add_step (Scenario.Step.fail f)

  let fail_direct (self : scenario_context) ?(data = []) message =
    fail self (fun () -> (message, data))

  let failf (self : scenario_context) ?data fmt =
    Fmt.kstr (fail_direct self ?data) fmt

  let bake (self : scenario_context) = self#add_step Scenario.Step.bake

  let call_babuso (self : scenario_context) query check =
    self#add_step (Scenario.Step.call_babuso ~query ~check)

  let call_babuso_until (self : scenario_context) ?(bake_attempts = false)
      ?(attempts = 10) ?(sleep = 0.5) query until =
    self#add_step
      (Scenario.Step.call_babuso_until ~bake_attempts ~attempts ~sleep ~query
         ~until)

  let lazily (self : scenario_context) f =
    self#add_step Scenario.Step.(lazy_ (fun () -> with_scenario_context f))

  let call_babuso_lazy (self : scenario_context) upf ~check =
    lazily self (fun ctxt -> call_babuso ctxt (upf ()) check)

  let call_babuso_down_is_done (self : scenario_context) upf =
    call_babuso_lazy self upf ~check:(fun ctxt -> function
      | Down_message.Done -> ()
      | other -> Scenario.Context.fail_wrong_message ctxt other)

  let wait_for_account_condition_by_name scenario ~check_message ~account_name
      ~until =
    call_babuso_until scenario ?bake_attempts:(Some true) ?attempts:None
      ?sleep:(Some 2.) Up_message.all_accounts (fun ctxt -> function
      | Down_message.Account_list al ->
          dbgf "Got %d accounts:@ %a" (List.length al) Sexp.pp_hum
            (List.sexp_of_t Babuso_lib.Wallet_data.Account.sexp_of_t al);
          Scenario.Context.check ctxt check_message
            (List.exists al ~f:(fun acc ->
                 String.equal acc.display_name account_name && until acc))
      | other -> Scenario.Context.fail_wrong_message ctxt other)
end

let () =
  let open Make_scenario in
  if test_the_test then (
    add "self-test-scenario:test-failure" @@ fun scenario ->
    let conf = ref None in
    call_babuso scenario Up_message.get_configuration (fun ctxt -> function
      | Down_message.Configuration config ->
          dbgf "Got config!";
          conf := Some config
      | other -> Scenario.Context.fail_wrong_message ctxt other);
    fail scenario (fun () ->
        ( "Failing the test on purpose.",
          [
            ("example0", Up_message.sexp_of_t Up_message.get_configuration);
            ( "configuration",
              Option.sexp_of_t Wallet_configuration.sexp_of_t !conf );
          ] ));
    ())

module Comments = struct
  let md ?(loc : (string * int * int * int) option) l =
    let actual_comment =
      l
      @ Option.value_map loc ~default:[] ~f:(fun (file, l, _, _) ->
            [ ""; Fmt.str "Cf. Source at `%s:%d`" file l ])
    in
    Human_prose.raw_markdown (String.concat ~sep:"\n" actual_comment)

  let mdo ?loc l = Some (md ?loc l)
end

let () =
  let open Make_scenario in
  add "basic:ledger-from-fake-client" @@ fun scenario ->
  let conf = ref None in
  call_babuso scenario Up_message.get_configuration (fun ctxt -> function
    | Down_message.Configuration config ->
        dbgf "Got config!";
        conf := Some config
    | other -> Scenario.Context.fail_wrong_message ctxt other);
  call_babuso scenario Up_message.list_connected_ledgers (fun ctxt -> function
    | Down_message.Ledgers ledgers ->
        (* There is at least one ledger: the fake-octez-client one *)
        Scenario.Context.check ctxt "at least a ledger"
          ~data:
            [ ("ledgers", List.sexp_of_t Ledger_nano.Device.sexp_of_t ledgers) ]
          (List.length ledgers >= 1);
        ()
    | other -> Scenario.Context.fail_wrong_message ctxt other);
  let account_name = "ledger-0" in
  let ledger_uri = "ledger://crouching-tiger-hidden-dragon/ed25519/0h/1h" in
  call_babuso scenario
    Up_message.(
      let comments = Comments.mdo [ "Ledger provided by the _fake client_." ] in
      import_account ~name:account_name ~kind:(`Ledger_uri ledger_uri) ~comments)
    (fun ctxt -> function
      | Down_message.Done -> ()
      | other -> Scenario.Context.fail_wrong_message ctxt other);
  call_babuso_until scenario ~sleep:2. Up_message.all_accounts (fun ctxt ->
    function
    | Down_message.Account_list al ->
        dbgf "Got %d ledgers" (List.length al);
        Scenario.Context.check ctxt "ledger-added-balance-zero"
          (List.exists al ~f:(fun acc ->
               String.equal acc.display_name account_name
               && Poly.equal acc.balance
                    (Some (if test_the_test then Z.one else Z.zero))))
    | other -> Scenario.Context.fail_wrong_message ctxt other);
  ()

let account_of_seed seed =
  let open Flextesa.Tezos_protocol.Account in
  let flx = of_name seed in
  object
    method name = seed
    method pkh = pubkey_hash flx
    method pk = pubkey flx
    method sk = private_key flx
  end

let alice = account_of_seed "alice"

let funded_account () =
  let flx =
    try List.random_element_exn With_flextesa.gas_accounts |> fst
    with _ -> Fmt.failwith "No gas accounts???"
  in
  let open Flextesa.Tezos_protocol.Account in
  account_of_seed (name flx)

let ensure_funded_account_imported scenario =
  let open Make_scenario in
  let account = funded_account () in
  call_babuso_down_is_done scenario
    Up_message.(
      fun () ->
        import_account ~name:account#name ~kind:(`Unencrypted_uri account#sk)
          ~comments:None);
  let current_balance = ref Z.zero in
  let account_id = ref "" in
  wait_for_account_condition_by_name scenario ~account_name:account#name
    ~check_message:"account-added-balance-non-zero"
    ~until:
      Babuso_lib.Wallet_data.Account.(
        fun acc ->
          match acc.balance with
          | Some zbal when Z.gt zbal Z.one ->
              current_balance := zbal;
              account_id := acc.id;
              dbgf "Current_Balance: %a, account-id: %s" Z.pp_print
                !current_balance !account_id;
              true
          | _ -> false);
  object
    method name = account#name
    method keypair = account
    method id = !account_id
    method current_balance = !current_balance
    method update_balance z = current_balance := z
  end

let wait_for_operation ?(check_data = fun () -> []) scenario ~check_message
    ~until =
  let open Make_scenario in
  call_babuso_until scenario ?bake_attempts:(Some true) ?attempts:None
    ?sleep:(Some 2.) Up_message.all_operations (fun ctxt -> function
    | Down_message.Operation_list al ->
        dbgf "Got %d operations:@ %a" (List.length al) Sexp.pp_hum
          (List.sexp_of_t Babuso_lib.Wallet_data.Operation.sexp_of_t al);
        let found = List.exists al ~f:until in
        Scenario.Context.check ~data:(check_data ()) ctxt check_message found
    | other -> Scenario.Context.fail_wrong_message ctxt other)

let operation_success_status =
  let open Operation.Status in
  function Success _ -> true | _ -> false

let operation_failure_status =
  let open Operation.Status in
  function Failed _ -> true | _ -> false

let order_operation ?(comment = fun () -> [])
    ?(check_status = operation_success_status) scenario ~specify =
  let open Make_scenario in
  let spec = ref None in
  call_babuso_down_is_done scenario
    Up_message.(
      fun () ->
        let specification = specify () in
        spec := Some specification;
        let comments =
          Comments.mdo
          @@ [ "Operation created by `backend_tests.ml`."; "" ]
          @ List.intersperse (comment ()) ~sep:""
        in
        order_operation ~specification ~comments);
  bake scenario;
  let op = ref None in
  wait_for_operation scenario ~check_message:"operation-in-there"
    ~check_data:(fun () ->
      [
        ("specification", Operation.Specification.sexp_of_t (specify ()));
        ("op", Option.sexp_of_t Operation.sexp_of_t !op);
      ])
    ~until:(function
      | o when Option.equal Operation.Specification.equal (Some o.order) !spec
        ->
          op := Some o;
          check_status o.status
      | _ -> false);
  let op_obj =
    object (self)
      method operation =
        Option.value_exn !op ~message:"Operation does not have  an ID yet"

      method id = self#operation.Operation.id

      method specification =
        Option.value_exn !spec
          ~message:"Operation does not have a specification yet"
    end
  in
  (* lazily scenario (fun subscenario ->
      let open Operation.Status in
      match op_obj#operation.status with
      | Success _ -> ()
      | _ ->
          failf subscenario "Wrong Operation Status: %a" Sexp.pp_hum
            (op_obj#operation |> Operation.sexp_of_t) ) *)
  op_obj

let () =
  let open Make_scenario in
  add "basic:add-key-pair-with-balance" @@ fun scenario ->
  let account = ensure_funded_account_imported scenario in
  let _op =
    order_operation scenario ~specify:(fun () ->
        Operation.Specification.transfer ~source:account#id
          ~destination:alice#pkh ~amount:(Z.of_int 1_000_000)
          ~entrypoint:"default" ~parameters:[])
  in
  wait_for_account_condition_by_name scenario ~account_name:account#name
    ~check_message:"account-added-balance-descreased"
    ~until:
      Account.(
        fun acc ->
          match acc.balance with
          | Some zbal when Z.lt zbal account#current_balance ->
              account#update_balance zbal;
              true
          | _ -> false);
  ()

let bob = account_of_seed "bob"

let find_account_by_name ?(wait_for_kt1 = true) scenario ~account_name =
  let open Make_scenario in
  let kt1_address = ref None in
  let account_id = ref None in
  wait_for_account_condition_by_name scenario ~account_name
    ~check_message:(Fmt.str "looking for %s" account_name)
    ~until:
      Account.(
        function
        | acc when wait_for_kt1 -> (
            let open Status in
            match acc.state with
            | Originated_contract Originated_contract.{ address; contract = _ }
              ->
                kt1_address := Some address;
                account_id := Some acc.id;
                dbgp Docpp.(textf "Found contract: %s %s" acc.id address);
                true
            | _ -> false)
        | acc ->
            account_id := Some acc.id;
            dbgp Docpp.(textf "Found account (no kt1): %s" acc.id);
            true);
  object
    method id = Option.value_exn !account_id ~message:"find_account_by_name:id"

    method kt1_address =
      Option.value_exn !kt1_address ~message:"find_account_by_name:kt1"
  end

let () =
  let open Make_scenario in
  add "basic:successful-multisig-contract" @@ fun scenario ->
  let account = ensure_funded_account_imported scenario in
  let msig_name = "generic-msig-01" in
  call_babuso_down_is_done scenario
    Up_message.(
      fun () ->
        let keys = [ alice#pk; bob#pk; account#keypair#pk ] in
        let threshold = 1 in
        let comments =
          let file, l, _, _ = Caml.__POS__ in
          Comments.mdo
            [
              "## 1-of-3-then-2 Multisig";
              "";
              "Originated with 3 keys, then updated to 2.";
              Fmt.str "Cf. test `%s:%d`" file l;
            ]
        in
        create_generic_multisig_account ~name:msig_name ~keys ~threshold
          ~comments ~gas_wallet:account#id);
  bake scenario;
  let msig = find_account_by_name scenario ~account_name:msig_name in
  let amount = Z.of_int 20_220_000 in
  let _op =
    order_operation scenario ~specify:(fun () ->
        Operation.Specification.transfer ~source:account#id
          ~destination:msig#kt1_address ~amount ~entrypoint:"default"
          ~parameters:[])
  in
  wait_for_account_condition_by_name scenario ~account_name:msig_name
    ~check_message:"msig-account-funded"
    ~until:Account.(fun acc -> Option.equal Z.equal acc.balance (Some amount));
  let ep_name = "main/update_keys" in
  let parameters =
    [
      ("threshold", Babuso_lib.Wallet_data.Variable.Value.nat Z.one);
      ( "keys",
        Babuso_lib.Wallet_data.Variable.Value.key_list
          [ account#keypair#pk; alice#pk ] );
    ]
  in
  let blob_to_sign = ref None in
  call_babuso_lazy scenario
    ~check:
      Down_message.(
        fun ctxt -> function
          | Blob s -> blob_to_sign := Some s
          | other -> Scenario.Context.fail_wrong_message ctxt other)
    Up_message.(
      fun () ->
        make_blob_for_signature ~target_contract_id:msig#id ~entrypoint:ep_name
          ~parameters);
  let account_signature = ref None in
  call_babuso_lazy scenario
    ~check:
      Down_message.(
        fun ctxt -> function
          | Signature s -> account_signature := Some s
          | other -> Scenario.Context.fail_wrong_message ctxt other)
    Up_message.(
      fun () ->
        let blob =
          Option.value_exn !blob_to_sign ~message:"Blob-none-pointer-exception"
        in
        sign_blob ~account:account#id ~blob);
  let _op_failed =
    order_operation
      ~comment:(fun () ->
        [ "The Multisig refuses non-zero transaction amounts." ])
      ~check_status:operation_failure_status scenario
      ~specify:(fun () ->
        Operation.Specification.transfer ~source:account#id
          ~destination:msig#kt1_address ~amount:Z.one ~entrypoint:ep_name
          ~parameters:
            (parameters
            @ [
                ( "signatures",
                  Variable.Value.signature_option_list
                    [
                      None;
                      None;
                      Some
                        (Option.value_exn !account_signature
                           ~message:"Missing signature from Alice");
                    ] );
              ]))
  in
  let _op =
    order_operation scenario ~specify:(fun () ->
        Operation.Specification.transfer ~source:account#id
          ~destination:msig#kt1_address ~amount:Z.zero ~entrypoint:ep_name
          ~parameters:
            (parameters
            @ [
                ( "signatures",
                  Variable.Value.signature_option_list
                    [
                      None;
                      None;
                      Some
                        (Option.value_exn !account_signature
                           ~message:"Missing signature from Alice");
                    ] );
              ]))
  in
  let _op_failed =
    order_operation ~check_status:operation_failure_status scenario
      ~comment:(fun () ->
        [ "Cannot reuse the signature, multisig-counter has changed." ])
      ~specify:(fun () ->
        Operation.Specification.transfer ~source:account#id
          ~destination:msig#kt1_address ~amount:Z.zero ~entrypoint:ep_name
          ~parameters:
            (parameters
            @ [
                ( "signatures",
                  Variable.Value.signature_option_list
                    [
                      None;
                      None;
                      Some
                        (Option.value_exn !account_signature
                           ~message:"Missing signature from Alice");
                    ] );
              ]))
  in
  let _op_failed =
    order_operation ~check_status:operation_failure_status scenario
      ~comment:(fun () ->
        [ "The Multisig wants at least one signature, here we provide 0." ])
      ~specify:(fun () ->
        Operation.Specification.transfer ~source:account#id
          ~destination:msig#kt1_address ~amount:Z.zero ~entrypoint:ep_name
          ~parameters:
            (parameters
            @ [
                ( "signatures",
                  Variable.Value.signature_option_list [ None; None; None ] );
              ]))
  in
  ()

let () =
  let open Make_scenario in
  add "basic:failing-multisig-contract" @@ fun scenario ->
  let account = ensure_funded_account_imported scenario in
  let msig_name = "wrong-msig-01" in
  call_babuso_down_is_done scenario
    Up_message.(
      fun () ->
        let keys = [ alice#pk; "tz123WrongWrong"; account#keypair#pk ] in
        let threshold = 1 in
        let comments =
          let file, l, _, _ = Caml.__POS__ in
          Comments.mdo
            [
              "## Failing Generic Multisig";
              "";
              Fmt.str
                "It uses a wrong format for a public-key, cf. test `%s:%d`" file
                l;
            ]
        in
        create_generic_multisig_account ~name:msig_name ~keys ~threshold
          ~comments ~gas_wallet:account#id);
  bake scenario;
  let msig =
    find_account_by_name scenario ~wait_for_kt1:false ~account_name:msig_name
  in
  wait_for_operation scenario ~check_message:"Finding failed origination"
    ~until:
      Operation.(
        fun op ->
          match op.status with
          | Failed _ -> (
              match op.order with
              | Specification.Origination { account; _ } ->
                  String.equal msig#id account
              | Specification.Transfer _ | Specification.Draft _ -> false)
          | Status.Paused | Status.Ordered | Status.Work_in_progress _
          | Status.Success _ ->
              false);
  ()

let () =
  let open Make_scenario in
  add "basic:successful-custom-contract-01" @@ fun scenario ->
  let account = ensure_funded_account_imported scenario in
  let account_name = "custom-01" in
  let open Ability in
  let open Control in
  let open Entrypoint in
  let open Constant_or_variable in
  let default_transfer = make "default" ~control:anyone ~ability:do_nothing in
  let funder_set_delegate =
    make "funder_set_delegate"
      ~control:(sender_is (constant account#keypair#pkh))
      ~ability:set_delegate
  in
  let admin_transfer =
    make "admin_transfer"
      ~control:(sender_is (variable "administrator"))
      ~ability:
        (transfer_mutez
           ~amount:(`Limit (constant (Z.of_int 42_042_042)))
           ~destination:`Anyone)
  in
  let entrypoints = [ default_transfer; funder_set_delegate; admin_transfer ] in
  let initialization =
    Variable.Map.of_list
      [ ("administrator", Variable.Value.address account#keypair#pkh) ]
  in
  call_babuso_down_is_done scenario
    Up_message.(
      fun () ->
        let comments =
          Fmt.kstr
            (Fn.compose Option.some Human_prose.raw_markdown)
            "### A _Custom_ Contract\n\n\
             A contract created by test `basic:successful-custom-contract-01`, \
             with %d _entrypoints_:\n\n\
             %s."
            (List.length entrypoints)
            (List.map entrypoints ~f:(fun e ->
                 Fmt.str "- `%s`" (Entrypoint.name e))
            |> String.concat ~sep:",\n")
        in
        create_smart_contract ~name:account_name ~gas_wallet:account#id
          ~comments ~entrypoints ~initialization);
  bake scenario;
  let contract_account = find_account_by_name scenario ~account_name in
  let _op =
    order_operation scenario
      ~comment:(fun () ->
        [
          Fmt.str
            "Transfer 3 `millitez` to the contract which then forwards 2 \
             `millitez` to Alice (`%s`)."
            alice#pkh;
        ])
      ~specify:(fun () ->
        Operation.Specification.transfer ~source:account#id
          ~destination:contract_account#kt1_address ~amount:(Z.of_int 3_000)
          ~entrypoint:(Entrypoint.name admin_transfer)
          ~parameters:
            (Entrypoint.collect_parameters admin_transfer
            |> List.map ~f:(function
                 | "destination", Variable.Type.Address ->
                     ("destination", Variable.Value.address alice#pkh)
                 | "amount", Variable.Type.Mutez ->
                     ("amount", Variable.Value.mutez (Z.of_int 2_000))
                 | other, _ -> Fmt.failwith "Unknown parameter: %S" other)))
  in
  ()

let () =
  let open Make_scenario in
  add "basic:drafts-and-simulations-01" @@ fun scenario ->
  let account = ensure_funded_account_imported scenario in
  let unrevealed = "unrevealed-01" in
  let acc =
    Fmt.kstr Flextesa.Tezos_protocol.Account.of_name "babuso-%s" unrevealed
  in
  call_babuso_down_is_done scenario
    Up_message.(
      fun () ->
        let comments =
          let file, l, _, _ = Caml.__POS__ in
          Fmt.kstr
            (Fn.compose Option.some Human_prose.raw_markdown)
            "## Unrevealed account (`%s:%d`).\n\n\
             Used to show reveal operations." (Path.basename file) l
        in
        save_account_draft ~id:unrevealed ~display_name:unrevealed ~comments
          ~glorify:true
          ~content:
            (Account.Draft.create_from_address_or_uri
               ~uri:(Flextesa.Tezos_protocol.Account.private_key acc)
               ()));
  bake scenario;
  let _u_account =
    find_account_by_name ~wait_for_kt1:false scenario ~account_name:unrevealed
  in
  let _op =
    order_operation scenario
      ~comment:(fun () ->
        [
          Fmt.str
            "Transfer 3 `millitez` to the contract which then forwards 2 \
             `millitez` to Alice (`%s`)."
            alice#pkh;
        ])
      ~specify:(fun () ->
        Operation.Specification.transfer ~source:account#id
          ~destination:(Flextesa.Tezos_protocol.Account.pubkey_hash acc)
          ~amount:(Z.of_int 3_000_000) ~entrypoint:"default" ~parameters:[])
  in
  let draft_0 =
    Operation.Draft.create_simple_transfer ~source:unrevealed
      ~destination:account#keypair#pkh ~amount:"10 mutez" ()
  in
  call_babuso_lazy scenario
    Up_message.(fun () -> run_operation_draft_simulation ~draft:draft_0)
    ~check:
      (fun _ -> function
        | Down_message.Simulation_result (res, batch) ->
            Failure.assert_true
              Poly.(Simulation_result.all_errors res = [])
              ~f:
                Docpp.(
                  fun () ->
                    simple_loc Caml.__POS__
                    +++ text "All Errors not empty:"
                    ++ itemize
                         (List.map
                            (Simulation_result.all_errors res)
                            ~f:(sexpable Tezos_operation_error.sexp_of_t)));
            Failure.assert_true
              ~f:
                Docpp.(
                  fun () ->
                    simple_loc Caml.__POS__ +++ text "Not a batch of 2:"
                    ++ itemize
                         (List.map
                            (Tezos_batch.operations batch)
                            ~f:(sexpable Tezos_batch.sexp_of_op)))
              (Tezos_batch.operations batch |> List.length = 2);
            ()
        | other ->
            Fmt.failwith "Wrong down message: %a" Sexp.pp_hum
              (Down_message.sexp_of_t other));
  ()

module Make_direct_scenario = struct
  let add name f = All_scenarios.add name [ Scenario.Step.direct f ]

  open Scenario

  type context = Scenario.Direct.Context.t

  let client_command (ctxt : context) ?wait args =
    let (_ : < .. >) = ctxt#client ?wait args in
    ()

  let client_command_stdout (ctxt : context) ?wait args =
    let (res : < .. >) = ctxt#client ?wait args in
    res#out

  let fail ?(data = []) (ctxt : context) reason =
    ctxt#make_failure (Test_failure.make ~reason data) |> raise

  let fail_wrong_message (ctxt : context) down =
    fail ctxt "Wrong down message"
      ~data:[ ("message", Down_message.sexp_of_t down) ]

  let down_is_done ctxt = function
    | Down_message.Done -> ()
    | other -> fail_wrong_message ctxt other

  let bake (ctxt : context) = ctxt#bake
  let call_babuso (ctxt : context) up = ctxt#call_babuso up

  let call_babuso_down_is_done ctxt up =
    call_babuso ctxt up |> down_is_done ctxt

  let wait_for ?(bake_attempts = true) ?(attempts = 20) ?(sleep = 1.2) ctxt
      ~name ~f =
    let rec go nth =
      match f () with
      | v -> v
      | exception _ when nth < attempts ->
          Unix.sleepf sleep;
          if bake_attempts then bake ctxt;
          go (nth + 1)
      | exception e ->
          Fmt.kstr
            (fail
               ~data:
                 [
                   ("attempts", Int.sexp_of_t nth);
                   ("sleep", Float.sexp_of_t sleep);
                   ("exception", Exn.sexp_of_t e);
                 ]
               ctxt)
            "wait_for: %s" name
    in
    go 0

  let find_map_account ?(name = "UNKNOWN") ctxt ~f =
    match call_babuso ctxt Up_message.all_accounts with
    | Down_message.Account_list al -> (
        dbgf "Got %d accounts:@ %a" (List.length al) Sexp.pp_hum
          (List.sexp_of_t Babuso_lib.Wallet_data.Account.sexp_of_t al);
        match List.find_map al ~f with
        | Some x -> x
        | None -> fail ctxt (Fmt.str "Account %S not found" name))
    | other -> fail_wrong_message ctxt other

  let wait_for_account ?bake_attempts ?(name = "UNKNOWN") ?attempts ?sleep ctxt
      ~f =
    wait_for ?bake_attempts ?attempts ?sleep ctxt
      ~name:(Fmt.str "wait-for-account-%s" name) ~f:(fun () ->
        find_map_account ctxt ~name ~f)

  let wait_for_account_condition ?bake_attempts ?name ?attempts ?sleep ctxt ~f =
    wait_for_account ?name ?bake_attempts ?attempts ?sleep ctxt ~f:(function
      | acc when f acc -> Some ()
      | _ -> None)

  let ensure_funded_account_imported ?(also_in_octez_client = true) ctxt =
    let account = funded_account () in
    call_babuso_down_is_done ctxt
      Up_message.(
        import_account ~name:account#name ~kind:(`Unencrypted_uri account#sk)
          ~comments:None);
    let id, balance =
      wait_for_account ctxt ~attempts:5 ~name:(Fmt.str "%s-import" account#name)
        ~bake_attempts:false ~f:(fun acc ->
          if String.equal acc.display_name account#name then
            Option.map acc.balance ~f:(fun b -> (acc.id, b))
          else None)
    in
    if also_in_octez_client then
      client_command ctxt
        [ "import"; "secret"; "key"; account#name; account#sk; "--force" ];
    object
      method name = account#name
      method keypair = account
      method id = id
      method initial_balance = balance
    end

  let originate_contract ~contract_name ?(balance = "0") ~from ?(init = "Unit")
      ctxt ~code =
    client_command ctxt
      [
        "originate";
        "contract";
        contract_name;
        "transferring";
        balance;
        "from";
        from#name;
        "running";
        code;
        "--init";
        init;
        "--burn-cap";
        "10.1";
        "--force";
      ];
    bake ctxt;
    let kt1 =
      client_command_stdout ctxt [ "show"; "known"; "contract"; contract_name ]
      |> String.concat ~sep:""
    in
    object
      method name = contract_name
      method kt1 = kt1
    end
end

let () =
  let open Make_direct_scenario in
  add "basic:uri-draft-accounts-01" @@ fun ctxt ->
  let from = ensure_funded_account_imported ctxt in
  let contract =
    originate_contract ctxt ~contract_name:"c0" ~balance:"10.42" ~from
      ~code:"parameter unit; storage unit; code {UNPAIR; FAILWITH}"
  in
  let contract_account = "contract-account-01" in
  (* fail ctxt "failing on purpose" ; *)
  call_babuso_down_is_done ctxt
    Up_message.(
      let comments =
        let file, l, _, _ = Caml.__POS__ in
        Comments.mdo
          [
            "**Imported KT1 address …**";
            "Originated by the test with `octez-client`";
            Fmt.str "Cf. `%s:%d`" (Path.basename file) l;
          ]
      in
      save_account_draft ~id:contract_account ~display_name:contract_account
        ~comments ~glorify:true
        ~content:(Account.Draft.create_from_address_or_uri ~uri:contract#kt1 ()));
  bake ctxt;
  wait_for_account_condition ~attempts:8
    ~name:(Fmt.str "%s-with-balance" contract_account)
    ~bake_attempts:false ctxt
    ~f:
      Account.(
        fun acc ->
          String.equal acc.id contract_account
          && match acc.balance with None -> false | Some v -> Z.gt v Z.one);
  ()

(** The {e result} of compiling
    {{:https://gist.github.com/smondet/284346fb772a13d1d55b7d664c45758a} this}. *)
let mini_fa2_michelson =
  {tz|
parameter (or (unit %default) (list %transfer (pair (address %from_) (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))));
storage   (pair (list %history (list (pair (address %from_) (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))) (big_map %metadata string bytes));
code
  {
    UNPAIR;     # @parameter : @storage
    IF_LEFT
      {
        DROP;       # @storage
        # == default ==
        # self.data.history = sp.list([]) # @storage
        NIL (list (pair address (list (pair address (pair nat nat))))); # list (list (pair address (list (pair address (pair nat nat))))) : @storage
        UPDATE 1;   # @storage
      }
      {
        SWAP;       # @storage : @parameter%transfer
        # == transfer ==
        # self.data.history.push(params) # @storage : @parameter%transfer
        DUP;        # @storage : @storage : @parameter%transfer
        CAR;        # list (list (pair (address %from_) (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) : @storage : @parameter%transfer
        DIG 2;      # @parameter%transfer : list (list (pair (address %from_) (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) : @storage
        CONS;       # list (list (pair (address %from_) (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) : @storage
        UPDATE 1;   # @storage
      }; # @storage
    NIL operation; # list operation : @storage
    PAIR;       # pair (list operation) @storage
  };
|tz}
  |> String.split ~on:'\n'
  |> List.map ~f:(fun line ->
         let drop c = String.mem "  \t" c in
         match String.strip ~drop line |> String.split ~on:'#' with
         | [] -> assert false
         | one :: _ -> one)
  |> String.concat ~sep:" "

let mini_fa2_storage () =
  {|(Pair {} {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d})|}

let () =
  let open Make_direct_scenario in
  add "basic:fa2-multisig-transfers" @@ fun ctxt ->
  let from = ensure_funded_account_imported ctxt in
  let fa2_contract =
    originate_contract ctxt ~contract_name:"fa2-0" ~from
      ~init:(mini_fa2_storage ()) ~code:mini_fa2_michelson
  in
  let fa2_account = "fa2-01" in
  call_babuso_down_is_done ctxt
    Up_message.(
      let comments =
        Comments.mdo ~loc:Caml.__POS__
          [
            "*Imported Fake FA2 contract*";
            "Originated by the test with `octez-client`.";
          ]
      in
      save_account_draft ~id:fa2_account ~display_name:fa2_account ~comments
        ~glorify:true
        ~content:
          (Account.Draft.create_from_address_or_uri ~uri:fa2_contract#kt1 ()));
  bake ctxt;
  let msig_account = from#name ^ "+msig" in
  call_babuso_down_is_done ctxt
    Up_message.(
      let comments =
        Comments.mdo ~loc:Caml.__POS__
          [ Fmt.str "*%s's 1/1-multisig*" from#name ]
      in
      save_account_draft ~id:msig_account ~display_name:msig_account ~comments
        ~glorify:true
        ~content:
          (Account.Draft.generic_multisig
             ~gas_wallet:(Drafting.Reference.create from#id)
             ~threshold:(Drafting.Reference.create "1")
             ~public_keys:(Drafting.Reference.create [ from#keypair#pk ])));
  bake ctxt;
  let msig_kt1 =
    wait_for_account ~attempts:12
      ~name:(Fmt.str "%s-with-address" msig_account)
      ctxt
      ~f:
        Account.(
          fun acc ->
            if String.equal acc.id msig_account then Account.get_address acc
            else None)
  in
  let signatures = Drafting.Collection.of_list [] in
  let key_to_find_later = 424242 in
  let draft =
    let fa2_transfer =
      Lambda_unit_operations.Draft.fa2_transfer
        ~token_id_address:(Drafting.Reference.create fa2_contract#kt1)
        ~token_id_number:
          (Drafting.Reference.create (Int.to_string key_to_find_later))
        ~amount:(Drafting.Reference.create "1010")
        ~source:(Drafting.Reference.create msig_kt1)
        ~destination:(Drafting.Reference.create from#keypair#pkh)
    in
    Operation.Draft.call_generic_multisig_main ~contract_address:msig_kt1
      ~gas_wallet:(Drafting.Reference.create from#id)
      ~lambda:
        (Lambda_unit_operations.Draft.structured
           (Drafting.Collection.of_list [ fa2_transfer ])
        |> Drafting.Reference.create)
      ~signatures
  in
  let hash, entrypoint, parameters =
    match
      Drafting.Render.sample_env (fun () ->
          Validate.Operation.draft ~rates:None
            ~with_signatures:Validate.Operation.With_signatures.not_yet
            (* ~before_signatures:true *) draft)
    with
    | Error draft_error ->
        fail ctxt "Could not pre-validate operation"
          ~data:[ ("errpr", Drafting.Render.sexp_of_error draft_error) ]
    | Ok
        (Operation.Specification.Transfer
          { source = _; destination; amount = _; entrypoint; parameters }) ->
        let hash =
          Validate.Operation.With_signatures.make_hash ~destination ~parameters
            ~counter:Z.zero
        in
        (hash, entrypoint, parameters)
    | Ok v ->
        fail ctxt "Validation did not make a transfer"
          ~data:[ ("result", Operation.Specification.sexp_of_t v) ]
  in
  let blob =
    match
      call_babuso ctxt
        Up_message.(
          make_blob_for_signature ~target_contract_id:msig_account ~entrypoint
            ~parameters)
    with
    | Down_message.Blob blob -> blob
    | other -> fail_wrong_message ctxt other
  in
  let signature =
    match call_babuso ctxt Up_message.(sign_blob ~account:from#id ~blob) with
    | Down_message.Signature s -> s
    | other -> fail_wrong_message ctxt other
  in
  Signature.Draft.add_or_replace signatures ~hash ~pk:from#keypair#pk signature;
  let op_id = msig_account ^ "-op1" in
  let op =
    let comments =
      Comments.md ~loc:Caml.__POS__ [ Fmt.str "*Call %s*" msig_account ]
    in
    Operation.make ~id:op_id ~comments
      ~order:(Operation.Specification.draft draft)
      ~status:Operation.Status.paused ~last_update:1. ()
  in
  call_babuso_down_is_done ctxt Up_message.(save_operation_draft op);
  call_babuso_down_is_done ctxt Up_message.(glorify_operation_draft op);
  wait_for_account_condition ctxt ~name:"fa2-has-digested-msig-operation"
    ~f:(fun acc ->
      String.equal (Account.id acc) fa2_account
      && begin
           try
             let foreigner =
               Account.foreign_contract_entrypoint ~entrypoint:"default" acc
             in
             let storage =
               Foreign_contract.storage_content foreigner#foreign_contract
             in
             String.is_substring
               ~substring:(Int.to_string key_to_find_later)
               (Michelson.to_concrete storage)
           with _ -> false
         end);
  ()

let commands () = [ Low_level_tests.command (); With_flextesa.command () ]
