open! Import
open Babuso_lib
open Babuso_error
open Wallet_data

let z = Pp.tag

let protect ctxt _name f =
  (* dbgf "Update_loop/%s starts" name ; *)
  match f ctxt with
  | () -> ()
  | exception Babuso_exn (e, z) ->
      let new_err = add_tag (e, z) "Babuso.Update_loop.protect" in
      Backend_state.record_loop_error ctxt (Babuso_exn new_err)
  | exception e ->
      let tagged =
        mk_tagged_error ~tag:"Babuso.Update_loop.protect"
          (Babuso_error.unknown_error (Exn.to_string e))
      in
      Backend_state.record_loop_error ctxt (Babuso_exn tagged)

let exchange_rates ctxt =
  let latest_quotes = Backend_state.Exchange_rates.timestamp ctxt in
  let allowed_age = Backend_state.exchange_rates_allowed_age ctxt in
  if Float.(Unix.gettimeofday () > latest_quotes + allowed_age) then (
    let tzkt_quotes = Backend_state.exchange_rates_uri ctxt in
    let quotes =
      let j = Http_client.Json_web_api.call ctxt tzkt_quotes in
      dbgf "quotes: %a" Json.pp j;
      let float k = Json.Q.float_field j ~k in
      Exchange_rates.
        {
          timestamp = Unix.gettimeofday ();
          btc = float "btc";
          usd = float "usd";
          eth = float "eth";
          eur = float "eur";
        }
    in
    Backend_state.Exchange_rates.set ctxt quotes;
    ());
  ()

let with_alerting ctxt ~reason gas_account f =
  let open Babuso_error in
  match gas_account with
  | { Account.state = Key_pair { backend = Ledger _; _ }; display_name; _ } ->
    begin
      let alert_id =
        Events.add_alert ctxt
          (Events.Ev.Alert.ledger_wants_human_intervention
             ~ledger_id:(Account.id gas_account) ~ledger_name:display_name
             ~reason)
      in
      match f () with
      | v ->
          Events.remove_alert ctxt alert_id;
          v
      | exception Babuso_exn (e, tag) ->
          Events.remove_alert ctxt alert_id;
          reraise_with_tag (e, tag) "Babuso.Update_loop.with_alerting"
      | exception e ->
          Events.remove_alert ctxt alert_id;
          raise
            (Babuso_exn
               ( Unknown_error (Exn.to_string e),
                 Some "Babuso.Update_loop.with_alerting" ))
    end
  | _ -> f ()

let make_tezos_operation_from_order ctxt order =
  match order with
  | Operation.Specification.Origination
      { account = _; specification; initialization; gas_wallet } ->
      let code, init_record =
        match specification with
        | Smart_contract.Generic_multisig _ ->
            ( Tezai_michelson.Untyped.of_json
                (Json.of_string Data.generic_multisig_macro_expanded_json),
              Variable.Record.of_map
                initialization (* Variable.Map.to_michelson initialization *)
                ~structure:[ "counter"; "threshold"; "keys" ] )
        | Smart_contract.Custom { entrypoints; _ } ->
            let `Code mich, `Storage_fields_order structure =
              Law_office.make_contract entrypoints
            in
            ( (* Hopefully temporary hack: *)
              Fmt.kstr Tezai_michelson.Untyped.C.concrete "{%s}" mich,
              Variable.Record.of_map initialization ~structure )
        | Smart_contract.Foreign _ ->
            Failure.raise_textf
              "Origination of foreign contracts is not supported."
      in
      let gas_account = Backend_state.Accounts.get_by_id ctxt gas_wallet in
      ( Tezos_operation.origination ~source:gas_account ~balance:Z.zero ~code
          ~storage_initialization:(Variable.Record.to_michelson init_record),
        Some init_record )
  | Operation.Specification.Transfer
      { source; amount; destination; entrypoint; parameters } ->
      let source_account = Backend_state.Accounts.get_by_id ctxt source in
      let entrypoint, parameters =
        match entrypoint with
        | "main/update_keys" ->
            let counter, _, _ =
              Indexer.generic_multisig_counter_threshold_keys ctxt
                ~address:destination
            in
            let variables =
              Variable.Record.comb [ ("counter", Variable.Value.nat counter) ]
            in
            ( "main",
              Operation_forge.Contract_call.Generic_multisig.(
                arg
                  ~payload:(update_keys_payload ctxt ~parameters)
                  ~variables ~parameters) )
        | "main/lambda" ->
            let counter, _, _ =
              Indexer.generic_multisig_counter_threshold_keys ctxt
                ~address:destination
            in
            let variables =
              Variable.Record.comb [ ("counter", Variable.Value.nat counter) ]
            in
            ( "main",
              Operation_forge.Contract_call.Generic_multisig.(
                arg
                  ~payload:(lambda_payload ctxt ~parameters)
                  ~variables ~parameters) )
        | other -> (other, Variable.Record.(comb parameters |> to_michelson))
      in
      ( Tezos_operation.transfer ~source:source_account ~amount
          ~destination (* "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" *)
          ~entrypoint ~parameters:(Some parameters),
        None )
  | Operation.Specification.Draft _ ->
      Fmt.failwith "make_tezos_operation_from_order: should not be a draft"

let accounts ctxt =
  let all_accounts = Backend_state.Accounts.all ctxt in
  List.iter all_accounts ~f:(fun acc ->
      (* let open Account in *)
      protect ctxt (Fmt.str "account/%s(%s)" acc.id acc.display_name)
        (fun ctxt ->
          match acc.state with
          | Friend fr ->
              Backend_state.Accounts.modify_by_id ctxt acc.id (fun acc ->
                  let balance =
                    Some (Indexer.balance ctxt ~address:fr.public_key_hash)
                  in
                  let pk =
                    match fr.public_key with
                    | Some s -> Some s
                    | None ->
                        Indexer.manager_key ctxt ~address:fr.public_key_hash
                  in
                  let acc =
                    {
                      acc with
                      balance;
                      state = Friend { fr with public_key = pk };
                    }
                  in
                  acc)
          | Key_pair kp ->
              Backend_state.Accounts.modify_by_id ctxt acc.id (fun acc ->
                  let balance =
                    Some (Indexer.balance ctxt ~address:kp.public_key_hash)
                  in
                  let acc = { acc with balance } in
                  acc)
          | Originated_contract { address; contract } ->
              Backend_state.Accounts.modify_by_id ctxt acc.id (fun acc ->
                  let balance = Some (Indexer.balance ctxt ~address) in
                  let updated_contract =
                    match contract with
                    | Smart_contract.Custom ({ variables; _ } as c) ->
                        Smart_contract.Custom
                          {
                            c with
                            variables =
                              Indexer.update_custom_contract_variables ctxt
                                ~address ~variables;
                          }
                    | Smart_contract.Generic_multisig _ ->
                        Smart_contract.generic_multisig
                          ~variables:
                            (Indexer.generic_multisig_variables ctxt ~address)
                    | Smart_contract.Foreign _ ->
                        Smart_contract.Foreign
                          (Indexer.forein_contract ctxt ~address)
                  in
                  let state =
                    Account.Status.originated_contract
                      { address; contract = updated_contract }
                  in
                  let acc = { acc with balance; state } in
                  acc)
          | Draft _ | Contract_failed_to_originate _ -> ()
          | Contract_to_originate { operation; contract } -> (
              match Backend_state.Operations.get_by_id ctxt operation with
              | { status = Operation.Status.Success _; _ } ->
                  let pseudoriginated =
                    Failure.of_none contract
                      ~f:
                        Docpp.(
                          fun () ->
                            text
                              "BUG: there is an operation without contract \
                               address"
                            +++ sexpable Account.sexp_of_t acc)
                  in
                  Backend_state.Accounts.modify_by_id ctxt acc.id (fun acc ->
                      {
                        acc with
                        state =
                          Account.Status.originated_contract pseudoriginated;
                      });
                  ()
              | { status = Operation.Status.Failed _; _ } ->
                  Backend_state.Accounts.modify_by_id ctxt acc.id (fun acc ->
                      {
                        acc with
                        state =
                          Account.Status.contract_failed_to_originate ~operation
                            ~contract;
                      });
                  ()
              | {
               status = Operation.Status.Paused | Ordered | Work_in_progress _;
               _;
              } ->
                  ())));
  ()

let start_operation ctxt order =
  let operation, init_record = make_tezos_operation_from_order ctxt order in
  let the_chosen_node = Backend_state.RPC.a_node ctxt in
  let op = Node_interaction.initiate_operation ctxt the_chosen_node operation in
  (op, the_chosen_node, init_record)

let operations ctxt =
  let all_operations = Backend_state.Operations.all ctxt in
  let treat_operation_exception op = function
    | Simulation_failure.E f ->
        Backend_state.Operations.modify ctxt
          (Operation.with_status_error op
             (Operation.Error.simulation_failure f))
    | Babuso_exn e ->
        Backend_state.Operations.modify ctxt
          (Operation.with_status_error_sexpable op Babuso_error.sexp_of_t e)
    | e ->
        Backend_state.Operations.modify ctxt
          (Operation.with_status_error_sexpable op Exn.sexp_of_t e)
  in
  List.iter all_operations ~f:(fun op ->
      (* let open Account in *)
      protect ctxt (Fmt.str "operation/%s" op.id) (fun _ctxt ->
          match op.status with
          | Ordered -> begin
              Backend_state.Operations.modify ctxt
                (Operation.with_status_work_in_progress op);
              match start_operation ctxt (Operation.order op) with
              | exception e -> treat_operation_exception op e
              | mop, _, _init_record -> begin
                  match Operation.order op with
                  | Operation.Specification.Origination
                      { account; specification; _ } ->
                      let address =
                        match mop#originated_contracts with
                        | [ one ] -> one
                        | _ -> assert false
                      in
                      let tezos_operation =
                        Operation.Status.make_tezos_operation
                          ~operation_hash:mop#operation_hash
                          ~base_block_hash:(Tezos_batch.branch mop#batch)
                          ()
                      in
                      let simulation_result = mop#simulation in
                      Backend_state.Operations.modify ctxt
                        (Operation.with_status_work_in_progress op
                           ~simulation_result ~tezos_operation);
                      Backend_state.Accounts.modify_by_id ctxt account
                        (fun acc ->
                          let state =
                            Account.Status.contract_to_originate
                              ~operation:op.id
                              ~contract:
                                (Some
                                   (Originated_contract.make
                                      ~contract:specification address))
                          in
                          { acc with state });
                      ()
                  | Operation.Specification.Transfer _ ->
                      let tezos_operation =
                        Operation.Status.make_tezos_operation
                          ~operation_hash:mop#operation_hash
                          ~base_block_hash:(Tezos_batch.branch mop#batch)
                          ()
                      in
                      let simulation_result = mop#simulation in
                      Backend_state.Operations.modify ctxt
                        (Operation.with_status_work_in_progress op
                           ~simulation_result ~tezos_operation);
                      ()
                  | Operation.Specification.Draft _ ->
                      Fmt.failwith "BUG: draft was ordered"
                end
            end
          | Work_in_progress { op = None; _ } ->
              Fmt.failwith "wip operation with no tezos-operation"
          | Work_in_progress
              { op = Some { operation_hash; base_block_hash; _ }; _ } -> (
              let the_chosen_node = Backend_state.RPC.a_node ctxt in
              let loc =
                Node_interaction.find_initiated_operation ctxt the_chosen_node
                  ~operation_hash ~operation_branch:base_block_hash
              in
              match loc with
              | `In_block (hash, true) ->
                  Backend_state.Operations.modify ctxt
                    (Operation.with_status_success op
                       ~tezos_operation:
                         {
                           Operation.Status.inclusion_block_hashes =
                             [ (hash, true) ];
                           operation_hash;
                           base_block_hash;
                         });
                  ()
              | `In_block (hash, false) ->
                  Backend_state.Operations.modify ctxt
                    (Operation.with_status_work_in_progress
                       ~tezos_operation:
                         {
                           Operation.Status.inclusion_block_hashes =
                             [ (hash, false) ];
                           operation_hash;
                           base_block_hash;
                         }
                       op);
                  ()
              | `In_mempool (mempool_section, mempool_errors, `Non_expired) ->
                  Backend_state.Operations.modify ctxt
                    (Operation.with_status_work_in_progress op ~mempool_errors
                       ~mempool_section);
                  ()
              | `Not_found `Non_expired -> ()
              | `Not_found `Expired ->
                  Backend_state.Operations.modify ctxt
                    (Operation.with_status_error op
                       Operation.Error.operation_lost)
              | `In_mempool (where, mempool_errors, status) ->
                  Backend_state.Operations.modify ctxt
                    (Operation.with_status_error op
                       (Operation.Error.stuck_in_the_mempool where
                          mempool_errors status));
                  ())
          | Paused | Success _ | Failed _ -> ()));
  ()

let start ctxt =
  let rec loop nth =
    let run () =
      if nth % 10 = 0 then protect ctxt "exchange_rates" exchange_rates;
      operations ctxt;
      if nth % 3 = 0 then accounts ctxt;
      ()
    in
    let continue () =
      dbgf "update_loop[%d]: sleeping now" nth;
      Unix.sleepf 2.;
      loop (nth + 1)
    in
    match run () with
    | () -> continue ()
    | exception Babuso_exn (e, z) ->
        let new_tagged = add_tag (e, z) "Babuso.Update_loop.start" in
        Backend_state.record_loop_error ctxt (Babuso_exn new_tagged);
        continue ()
    | exception e ->
        let tagged_e =
          Babuso_exn
            ( Babuso_error.unknown_error (Exn.to_string e),
              Some "Babuso.Update_loop.start" )
        in
        Backend_state.record_loop_error ctxt tagged_e;
        continue ()
  in
  let (_ : Threads.Preemptive.t) = Threads.spawn ctxt (fun () -> loop 0) in
  ()
