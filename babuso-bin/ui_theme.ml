open Import
include Babuso_lib.Wallet_configuration.Ui_theme_name

let css self : string =
  let extra_css =
    {css|
.user_comment {
  padding: 4px;
  font-size: 90%;    
}  
|css}
  in
  let extra_css_light = extra_css ^ {css|
code { color: #333; }
|css} in
  let extra_css_dark = extra_css ^ {css|
code { color: #888; }
|css} in
  Variants.map self
    ~lumen:(fun _ -> Data.lumen_css ^ extra_css_light)
    ~cerulean:(fun _ -> Data.cerulean_css ^ extra_css_light)
    ~solar:(fun _ -> Data.solar_css ^ extra_css_dark)
    ~greyscale:(fun _ -> Data.greyscale_css ^ extra_css_light)

let cmdliner_term () =
  let open Cmdliner in
  let names =
    [
      ("lumen", `Lumen);
      ("cerulean", `Cerulean);
      ("greyscale", `Greyscale);
      ("solarized", `Solar);
    ]
  in
  let enums =
    names
    @ [
        ("default", `Cerulean);
        ("dark", `Solar);
        ("random", List.random_element_exn names |> snd);
      ]
  in
  Arg.(
    value
      (opt (enum enums) `Cerulean
         (info [ "theme" ] ~doc:"Theme name of “random”")))
