open! Import
open Babuso_lib
open! Wallet_data
open Wallet_configuration

let cmdliner_term () =
  let open Cmdliner in
  let open Term in
  const
    (fun
      port
      theme
      max_connections
      octez_client_executable
      nodes
      fake_tezos_client
    ->
      let nodes = List.map nodes ~f:Tezos_node.make_new in
      let configuration =
        let ui_options = Ui_options.make ~theme () in
        make ~port ~max_connections ~octez_client_executable ~ui_options
          ~fake_tezos_client ~nodes ()
      in
      configuration)
  $ Arg.(value (opt int 8480 (info [ "port"; "P" ] ~doc:"Port to listen on.")))
  $ Ui_theme.cmdliner_term ()
  $ Arg.(
      value
        (opt int 32
           (info [ "max-connections" ] ~doc:"Maximum number of connections.")))
  $ Arg.(
      value
        (opt string "octez-client"
           (info
              [ "octez-client-executable" ]
              ~doc:"Path to octez-client binary.")))
  $ Arg.(
      value
        (opt_all string []
           (info [ "octez-node" ] ~doc:"Add an Octez node base-URL.")))
  $ Arg.(
      value
        (flag
           (info [ "fake-octez-client" ]
              ~doc:"Try to mockup octez-client responses.")))
