open! Import
open! Babuso_lib.Babuso_error

type t = { default_timeout : float [@default 5.] }
[@@deriving fields, sexp, compare, equal, make]

exception
  Timeout of {
    v : float;
    headers : (string * string) list;
    meth : [ `Get | `Post of string ];
    url : string;
  }

let _get ctxt = ctxt#http_client

module Low_level = struct
  let call_lwt ctxt ?(timeout = `Default) ?(headers = []) ?(meth = `Get) url =
    let self = _get ctxt in
    let open Lwt in
    let with_timeout v lwt =
      pick
        [
          lwt;
          begin
            Lwt_unix.sleep v >>= fun () ->
            raise (Timeout { url; meth; headers; v })
          end;
        ]
    in
    let with_timeout_or_not lwt =
      match timeout with
      | `No -> lwt
      | `Set v -> with_timeout v lwt
      | `Default -> with_timeout (default_timeout self) lwt
    in
    let meth, body =
      match meth with
      | `Get -> (`GET, None)
      | `Post s -> (`POST, Cohttp_lwt.Body.of_string s |> Option.some)
    in
    with_timeout_or_not
      (Cohttp_lwt_unix.Client.call meth (Uri.of_string url) ?body
         ~headers:(Cohttp.Header.of_list headers))
    >>= fun (resp, body) ->
    match Cohttp.Response.status resp with
    | `OK -> Cohttp_lwt.Body.to_string body
    | _ ->
        Lwt.catch
          (fun () -> Cohttp_lwt.Body.to_string_list body)
          (fun exn -> return [ Exn.to_string exn ])
        >>= fun bod ->
        Failure.raise
          Docpp.(
            text "HTTP-call failed:"
            +++ itemize
                  [
                    textf "URL: %S" url;
                    text "Response:" +++ sexpable Cohttp.Response.sexp_of_t resp;
                    text "Body:" +++ verbatim_lines bod;
                  ])
end

let call ctxt ?(timeout = `Default) ?(headers = []) ?(meth = `Get) url =
  Threads.lwt_run ctxt (fun () ->
      Low_level.call_lwt ctxt ~timeout ~headers ~meth url)

module Json_web_api = struct
  let call ctxt ?(headers = []) ?(meth = `Get) url =
    let headers = ("Content-type", "application/json") :: headers in
    call ctxt ~headers url
      ~meth:
        (match meth with
        | `Get -> `Get
        | `Post json -> `Post (Ezjsonm.value_to_string json))
    |> Ezjsonm.value_from_string
end
