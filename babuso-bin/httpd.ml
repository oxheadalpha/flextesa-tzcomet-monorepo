open! Import
include Tiny_httpd
open Babuso_lib.Babuso_error

let respond_error ?(code = 404) ?req pp =
  dbgf "respond_error - code: %d" code;
  let body =
    Fmt.str "%a" Pp.to_fmt
      Docpp.(
        vbox
          (box pp
          ++ Option.value_map req ~default:nop ~f:(fun req ->
                 cut ++ box ~indent:2 (text "Request:" +++ of_fmt Request.pp req))
          ))
  in
  let res =
    Response.make_string
      ~headers:[ ("Content-Type", "text/plain") ]
      (Error (404, body))
  in
  res

let respond_404 ?req pp = respond_error ~code:404 ?req pp

let handle_exceptions ?req f =
  let default_code = 500 in
  try f () with
  | Babuso_exn (Octez_client_error { cmd_line_err; primary_cause }, _tag) ->
      let proto_err =
        Protocol_error
          {
            cmd_type = cmd_line_err.cmd_type;
            code = default_code;
            content_results = primary_cause;
          }
      in
      let pp = sexp_of_babuso_error proto_err in
      respond_error ~code:default_code Docpp.(sexp pp)
  (* TODO: This may not be needed *)
  | Babuso_exn (Command_line_error cle, _tag) ->
      let proto_err =
        Protocol_error
          {
            cmd_type = cle.cmd_type;
            code = default_code;
            content_results = String.concat ~sep:"\n" cle.std_err;
          }
      in
      let pp = sexp_of_babuso_error proto_err in
      respond_error ~code:default_code Docpp.(sexp pp)
  (* These last two cases will hopefully never happen *)
  | Failure.F { doc; code } ->
      let code = match code with 404 -> 404 | _ -> default_code in
      respond_error ~code doc ?req
  | e ->
      respond_error ~code:default_code
        Docpp.(
          text "There was an error:"
          +++
          match e with
          | Failure s -> verbatim s
          | other -> Fmt.kstr verbatim "%a" Exn.pp other)

let header_argument query =
  match List.Assoc.find query "header" ~equal:String.equal with
  | Some "none" -> `None
  | None -> `Default
  | Some other ->
      let msg = Fmt.str "Wrong 'header' argument: %S" other in
      raise (Babuso_exn (General_error msg, Some "Babuso.Hppd.header_argument"))
