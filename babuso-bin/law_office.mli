open! Import
open! Babuso_lib.Wallet_data
open! SmartML
open Tezai_smartml_generation
open! Construct

module Storage : sig
  type t = {
    field : string;
    typ : SmartML.Type.t;
    vtype : Variable.Type.t;
    value_expr : Expr.t;
  }
  [@@deriving fields]

  val make : [> `Dont_use_make_any_more ]
end

module Parameters : sig
  type t = { field : string; vtype : Variable.Type.t }
  [@@deriving fields, make, equal]

  val to_expr : t -> Expr.t
end

val add_param_request_checking_type :
  Storage.t list ->
  Parameters.t list ref ->
  t_equal:(Variable.Type.t -> Variable.Type.t -> bool) ->
  Parameters.t ->
  unit

val add_parameter_request :
  Storage.t list -> Parameters.t list ref -> Parameters.t -> unit

val add_unique_parameter_request :
  Storage.t list -> Parameters.t list ref -> Parameters.t -> string

val add_storage_request_checking_type :
  t_equal:(SmartML.Type.t -> SmartML.Type.t -> bool) ->
  vt_equal:(Variable.Type.t -> Variable.Type.t -> bool) ->
  Storage.t list ref ->
  Parameters.t list ->
  Storage.t ->
  unit

val add_storage_request :
  Storage.t list ref -> Parameters.t list -> Storage.t -> unit

val const_or_var_storage :
  Storage.t list ref ->
  Parameters.t list ref ->
  'a Constant_or_variable.t ->
  make_expression:('a -> SmartML.Basics.Untyped.expr) ->
  vt:Variable.Type.t ->
  SmartML.Basics.Untyped.expr

val process_action :
  Storage.t list ref ->
  Parameters.t list ref ->
  Control.t ->
  Ability.t ->
  Command.t list

val check_parameters : Parameters.t list -> Entrypoint.t -> unit

val process_check :
  Storage.t list ref -> Parameters.t list ref -> Control.t -> Command.t list

val make_contract :
  Entrypoint.t list ->
  [> `Code of string ] * [> `Storage_fields_order of string list ]
