module Raw : sig
  val series_name : string
  val command_to_string_list : string -> string list
end

module Shell : sig
  type t

  val default : unit -> t
  val command_to_string_list : < shell : t ; .. > -> string -> string list
end

module File : sig
  type file_io = File_io

  val default : unit -> file_io
  val with_fake_capability : < file_io : file_io ; .. > -> 'a -> 'a
  val file_exists : < file_io : file_io ; .. > -> string -> bool
  val read_lines : < file_io : file_io ; .. > -> string -> string list
  val write_lines : < file_io : file_io ; .. > -> string -> string list -> unit
  val make_path : < file_io : file_io ; .. > -> string -> unit
end

module Clock : sig
  type t = Clock

  val default : unit -> t
  val with_fake_capability : < clock : t ; .. > -> 'a -> 'a
  val sleep : < clock : t ; .. > -> float -> unit
end
