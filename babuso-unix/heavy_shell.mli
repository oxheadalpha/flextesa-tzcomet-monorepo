module Heavy_series : sig
  type t = { temp_dir_path : string; mutable index : int }

  val make_dir_f :
    ('a -> string, Format.formatter, unit, string) format4 -> 'a -> unit

  val init : string -> t
  val fresh_tmp : t -> string -> string
  val read_lines : string -> string list

  val command :
    t ->
    string ->
    < err_path : string ; out_path : string ; output_lines : string list >
end
