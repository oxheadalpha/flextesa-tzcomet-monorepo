open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

let or_empty ctxt f =
  Reactive.bind (Web_app_state.Configuration.debug ctxt) ~f:(function
    | false -> Mono_html.empty ()
    | true -> f ())

let bloc ctxt c =
  or_empty ctxt (fun () ->
      let open Mono_html in
      Bootstrap.alert ~kind:`Warning (div (bt "👷 Debug") % c))

let text ctxt content =
  or_empty ctxt
    Mono_html.(
      fun () ->
        Bootstrap.color `Warning
          (span
             ~a:
               [
                 style
                   "border: 1px solid #99000044; padding: 2px; font-size: 90%";
               ]
             (Fmt.kstr t "🏗%s" content)))

let sexpable to_sexp v =
  let open Mono_html in
  pre (Fmt.kstr ct "%a" (Sexp.pp_hum_indent 4) (to_sexp v))

let expandable ctxt summ content =
  bloc ctxt Mono_html.(details ~summary:summ content)

let expandable_sexpable ctxt summ to_sexp v =
  bloc ctxt Mono_html.(details ~summary:summ (sexpable to_sexp v))

let boxify_style ?(level = 0) ctxt s =
  let open Mono_html in
  H5.a_style
    (Reactive.map (Web_app_state.Configuration.debug ctxt) ~f:(function
      | false -> s
      | true ->
          let bg =
            match Int.(level % 7) with
            | 0 -> "#ddd"
            | 1 -> "#dde"
            | 2 -> "#ded"
            | 3 -> "#eed"
            | 4 -> "#dee"
            | 5 -> "#ede"
            | _ -> "#edd"
          in
          Fmt.str
            "border: 2px solid #999; margin: 2px; background-color: %s; \
             padding: 2px; %s"
            bg s))
