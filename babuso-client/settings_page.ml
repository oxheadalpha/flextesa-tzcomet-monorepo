open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

let nodes_section ctxt nodes =
  let open Mono_html in
  let kind_of_health = function
    | `Fine -> `Success
    | `Dead -> `Danger
    | `Unknown -> `Info
    | `Injured -> `Warning
  in
  let show_health = function
    | `Fine -> t "Fine 💪"
    | `Dead -> t "Dead 💀"
    | `Injured -> t "Injured 🚑"
    | `Unknown -> t "Unknown 🤷"
  in
  let show_head = function
    | None -> t "Unknown"
    | Some (lvl, hash) ->
        Fmt.kstr bt "%d" lvl % t "/"
        % Custom_widget.block_hash ctxt hash ~shorten:None
  in
  let show_version = function
    | None -> t "Unknown 🤷"
    | Some json ->
        let prettified =
          try
            let mobj = Json.of_string json in
            let open Json.Q in
            let vo = field mobj ~k:"version" in
            let maj = int_field vo ~k:"major" in
            let min = int_field vo ~k:"minor" in
            let addi = string_field vo ~k:"additional_info" in
            let co = field mobj ~k:"commit_info" in
            let hash =
              let h = string_field co ~k:"commit_hash" in
              try String.subo h ~len:8 with _ -> h
            in
            let date =
              string_field co ~k:"commit_date"
              |> String.split ~on:' ' |> List.hd_exn
            in
            let version =
              match addi with
              | "dev" -> Fmt.kstr it "Development (%d.%d)" maj min
              | "release" ->
                  let doc =
                    Fmt.str "http://tezos.gitlab.io/releases/version-%d.html"
                      maj
                  in
                  link ~target:doc (Fmt.kstr bt "%d.%d" maj min)
              | _ -> Fmt.kstr ct "%d.%d (%s" maj min addi
            in
            let committish =
              let target =
                Fmt.str "https://gitlab.com/tezos/tezos/-/commit/%s" hash
              in
              link ~target (ct hash) %% parens (it date)
            in
            version %% t "/" %% committish
          with _ -> bt "Failed to parse"
        in
        let json =
          Custom_widget.Global_modal.(
            raw_json json |> as_button ~label:(t "Show Full JSON"))
        in
        prettified % json
  in
  let node_tools node =
    Custom_widget.inline_button (t "Delete") ~action:(fun () ->
        Web_app_state.Configuration.modification_action ctxt (fun conf ->
            let removed_once = ref false in
            Wallet_configuration.
              {
                conf with
                nodes =
                  List.filter_map conf.nodes ~f:(fun n ->
                      let e = Tezos_node.equal n node in
                      if e then
                        if !removed_once then Some n
                        else (
                          removed_once := true;
                          None)
                      else Some n);
              }))
    %% div
         (link
            ~target:
              (Fmt.str "%s/chains/main/mempool/pending_operations"
                 (Tezos_node.base_url node))
            (t "☏ Mempool RPC"))
  in
  let show_problems =
    let one = function
      | `Timeouts_observed -> it "Suprious timeouts were observed …"
      | `Wrong_chain_id (c, instead_of) ->
          it "Chain-ID is" %% ct c %% it "instead of" %% ct instead_of % t "."
      | `No_chain_id_consensus ->
          it "Network monitoring cannot even agree on a consensual chain-id!"
      | `Head_is_behind -> it "Head-block-level is too far behind."
    in
    function
    | [] -> empty ()
    | [ o ] -> div (one o)
    | more -> itemize (List.map ~f:one more)
  in
  let header_row =
    [ t "Base URI"; t "Status"; t "Head Block"; t "Version"; t "⚒" ]
  in
  let number_of_columns = List.length header_row in
  let node_row (n, status) =
    let health = Tezos_node.Status.health status in
    let problems = Tezos_node.Status.problems status in
    tr
      ~a:
        [
          classes
            [ Bootstrap.Label_kind.fill_div_class (kind_of_health health) ];
        ]
      (td (Tezos_node.base_url n |> ct)
      % td (show_health health % show_problems problems)
      % td (show_head (Tezos_node.Status.head status))
      % td (show_version (Tezos_node.Status.version status))
      % td (node_tools n))
    % Custom_widget.Debug.(
        or_empty ctxt @@ fun () ->
        tr
          (td
             ~a:[ H5.a_colspan (Reactive.pure number_of_columns) ]
             (expandable_sexpable ctxt
                (t "Low-level-status for" %% (Tezos_node.base_url n |> ct))
                Tezos_node.Status.sexp_of_t status)))
  in
  let wip doing node =
    tr
      (td
         ~a:[ H5.a_colspan (Reactive.pure number_of_columns) ]
         (Bootstrap.spinner (t "WIP")
         %% it doing
         %% ct (Tezos_node.base_url node)))
  in
  let nodes_statuses =
    Web_app_state.Network_info.reactive ctxt
    |> Reactive.bind ~f:(function
         | None ->
             itemize (List.map nodes ~f:(fun n -> ct (Tezos_node.base_url n)))
             %% bt "Status is not known …"
         | Some info ->
             let all = Network_information.nodes info in
             let configured_but_not_yet_informed =
               List.filter nodes ~f:(fun n ->
                   List.exists all ~f:(fun (nn, _) -> Tezos_node.equal n nn)
                   |> not)
             in
             let not_configured_any_more n =
               List.exists nodes ~f:(fun nn -> Tezos_node.equal n nn) |> not
             in
             Custom_widget.make_table ~header_row ctxt
               (list
                  (List.map all ~f:(fun (n, s) ->
                       if not_configured_any_more n then wip "Removing" n
                       else node_row (n, s)))
               % list
                   (List.map configured_but_not_yet_informed ~f:(fun node ->
                        wip "Getting information on" node))))
  in
  let add_node =
    let state = Reactive.var `Idle in
    Reactive.bind_var state ~f:(function
      | `Idle ->
          Custom_widget.inline_button (t "Add a Node") ~action:(fun () ->
              Reactive.set state `Form)
      | `Form ->
          let bidi = Reactive.var "" in
          let save_action () =
            Web_app_state.Configuration.modification_action ctxt (fun conf ->
                Wallet_configuration.
                  {
                    conf with
                    nodes =
                      Tezos_node.make_new (Reactive.peek bidi |> String.strip)
                      :: conf.nodes;
                  });
            Reactive.set state `Idle
          in
          t "Enter the base-URI:"
          %% Bootstrap.Input.bidirectional
               ~placeholder:
                 (Reactive.pure "http://octez-node.example.com/prefix/")
               ~cols:3 ~inline_block:true
               (Reactive.Bidirectional.of_var bidi)
               ~enter_action:save_action
          %% Custom_widget.inline_button (t "Add") ~action:save_action
          %% Custom_widget.inline_button (t "Cancel") ~action:(fun () ->
                 Reactive.set state `Idle))
  in
  nodes_statuses % div add_node

let render ctxt =
  let open Mono_html in
  let saving_state = Wip.idle_var () in
  h3 (t "Settings")
  % Reactive.bind
      Reactive.(Web_app_state.Configuration.get ctxt ** get saving_state)
      ~f:(function
        | None, _ | _, Wip.WIP ->
            bt "Configuration is not available, yet …"
            %% Bootstrap.spinner (t "WIP")
        | _, Wip.Failure s -> Bootstrap.label `Danger (bt s)
        | Some config, (Wip.Idle | Wip.Success ()) ->
            let edited =
              Custom_widget.Editor.make_var config ~to_string:(fun config ->
                  Sexp.to_string_hum (Wallet_configuration.sexp_of_t config))
            in
            Custom_widget.Editor.show_result ctxt edited ~to_html:(fun conf ->
                let open Wallet_configuration in
                let {
                  port : int;
                  max_connections : int;
                  nodes : Octez_node_definition.set;
                  octez_client_executable : string;
                  fake_tezos_client : bool;
                  exchange_rates_allowed_age : float;
                  exchange_rates_uri : string;
                  ui_options;
                } =
                  conf
                in
                let parameter name v show =
                  div (Fmt.kstr bt "%s:" name %% show v)
                in
                let nat _ _ = Fmt.kstr ct "%d" in
                let sexp f x = Fmt.kstr ct "%a" Sexp.pp_hum (f x) in
                h4 (t "Backend Configuration")
                % h5 (t "Restart Needed")
                % parameter "HTTP Port" port (nat 1024 (2 ** 16))
                % parameter "HTTP Max Connections" max_connections
                    (nat 1 10_000)
                % parameter "Octez/Tezos Client Executable"
                    octez_client_executable (sexp String.sexp_of_t)
                % parameter "Faking Tezos Client (Debug)" fake_tezos_client
                    (sexp Bool.sexp_of_t)
                % parameter "Exchange-rates' Allowed Age"
                    exchange_rates_allowed_age (sexp Float.sexp_of_t)
                % parameter "Exchange-rates' URI" exchange_rates_uri
                    (sexp String.sexp_of_t)
                % h5 (t "Tezos Nodes")
                % nodes_section ctxt nodes
                % h4 (t "GUI Options")
                %
                let Ui_options.
                      {
                        debug = _;
                        compact = _;
                        operations_per_page = _;
                        theme : Ui_theme_name.t;
                        accounts_table_fields;
                      } =
                  ui_options
                in
                let set_bool name field =
                  let open Fieldslib in
                  let v = Field.get field ui_options in
                  div
                    (Bootstrap.Input.check_box ~init_checked:v
                       (Some
                          (bt name
                          % Custom_widget.Debug.(
                              Fmt.kstr (text ctxt) "currently %b" v)))
                       ~action:(fun new_value ->
                         let ui_opts = Field.fset field ui_options new_value in
                         Web_app_state.Configuration.unset ctxt;
                         let conf = { conf with ui_options = ui_opts } in
                         Server_communication.call_with_var_unit saving_state
                           (Protocol.Message.Up.update_configuration conf)))
                in
                let set_int name field range =
                  let open Fieldslib in
                  let (v : int) = Field.get field ui_options in
                  let var = Reactive.var v in
                  div
                    (Fmt.kstr bt "%s →" name
                    %% Reactive.bind_var var ~f:(Fmt.kstr ct "%d")
                    %% div
                         (Bootstrap.Input.range ~init_value:v ~min:(fst range)
                            ~inline_block:true ~max:(snd range) ~cols:3 None
                            ~action:(fun new_value ->
                              Reactive.set var new_value)
                         % Reactive.bind_var var ~f:(fun new_value ->
                               Custom_widget.inline_button (t "Save")
                                 ~disabled:(new_value = v) ~action:(fun () ->
                                   let ui_opts =
                                     Field.fset field ui_options new_value
                                   in
                                   Web_app_state.Configuration.unset ctxt;
                                   let conf =
                                     { conf with ui_options = ui_opts }
                                   in
                                   Server_communication.call_with_var_unit
                                     saving_state
                                     (Protocol.Message.Up.update_configuration
                                        conf)))))
                in
                set_bool "Debug mode" Ui_options.Fields.debug
                % set_bool "Compact mode" Ui_options.Fields.compact
                % set_int "Number of operations per “page”"
                    Ui_options.Fields.operations_per_page (3, 50)
                % parameter "GUI Theme" theme (sexp Ui_theme_name.sexp_of_t)
                % parameter "Accounts-table Fields:" accounts_table_fields
                    (sexp @@ List.sexp_of_t Accounts_table_field_name.sexp_of_t))
            % Custom_widget.Editor.collapsable ctxt edited
                ~show_button_content:(t "Show Low-level Editor")
                ~save_button_content:(t "Save Configuration")
                ~unchanged:(Wallet_configuration.equal config)
                ~of_string:(fun s ->
                  Sexplib.Sexp.of_string s |> Wallet_configuration.t_of_sexp)
                ~save_action:(fun conf ->
                  dbgf "save config %a" Sexp.pp_hum
                    (Wallet_configuration.sexp_of_t conf);
                  Web_app_state.Configuration.unset ctxt;
                  Server_communication.call_with_var_unit saving_state
                    (Protocol.Message.Up.update_configuration conf)))
