open Import
open Babuso_lib
open Wallet_data
module State = Web_app_state
open Lwd_bootstrap

let import_ledger_ui () =
  let open Mono_html in
  let state = Reactive.var Wip.Idle in
  let try_again () =
    Bootstrap.button (t "Try again") ~action:(fun () ->
        Server_communication.list_connected_ledgers state)
  in
  Reactive.bind_var state ~f:(function
    | Wip.Idle ->
        Bootstrap.button (t "Import a Ledger key-pair") ~action:(fun () ->
            Server_communication.list_connected_ledgers state)
    | Wip.WIP ->
        Bootstrap.alert ~kind:`Warning
          (bt "Scanning" %% Bootstrap.spinner (t "Please wait"))
    | Wip.Success [] ->
        Bootstrap.alert ~kind:`Warning
          (bt
             "The client found no ledgers connected, make sure the your ledger \
              is connected, with the Tezos Wallet application started.")
        %% try_again ()
    | Wip.Success sl ->
        let form_state = Reactive.var Wip.Idle in
        Reactive.bind_var form_state ~f:(function
          | Wip.Idle ->
              Bootstrap.alert ~kind:`Success
                (itemize ~numbered:true
                   (List.map sl
                      ~f:
                        Ledger_nano.Device.(
                          fun { animals; wallet_app_version } ->
                            let head =
                              ct animals
                              %%
                              match wallet_app_version with
                              | None -> bt "(Unknown version)"
                              | Some v -> t "(Tezos Wallet" %% ct v % t ")"
                            in
                            let module Curve_derivation = struct
                              let to_string = function
                                | `Bip25519 -> "bip25519"
                                | `Secp256k1 -> "secp256k1"
                            end in
                            let form =
                              let curve = Reactive.var `Bip25519 in
                              let curve_dropdown =
                                let show c =
                                  Fmt.kstr ct "/%s"
                                    (Curve_derivation.to_string c)
                                in
                                let title = Reactive.bind_var curve ~f:show in
                                Bootstrap.Dropdown_menu.(
                                  button title
                                    (header
                                       (it "Choose Curve/Derivation Method")
                                    :: List.map [ `Bip25519; `Secp256k1 ]
                                         ~f:(fun v ->
                                           item (show v) ~action:(fun () ->
                                               Reactive.set curve v))))
                              in
                              let path = Reactive.var [| "0h"; "0h" |] in
                              let account_name =
                                Reactive.var
                                  (String.lsplit2_exn animals ~on:'-' |> fst)
                              in
                              let path_form =
                                Reactive.bind_var path ~f:(fun current_path ->
                                    Bootstrap.Form.(
                                      let items =
                                        Array.to_list
                                        @@ Array.mapi current_path
                                             ~f:(fun idx s ->
                                               let bidir =
                                                 Reactive.Bidirectional.make
                                                   (Reactive.map
                                                      (Reactive.get path)
                                                      ~f:(fun l ->
                                                        Array.get l idx))
                                                   (fun v ->
                                                     Reactive.set path
                                                       (let p =
                                                          Array.copy
                                                            current_path
                                                        in
                                                        Array.set p idx v;
                                                        p))
                                               in
                                               input ~placeholder:(Lwd.pure s)
                                                 bidir)
                                      in
                                      let plus_minus =
                                        [
                                          magic
                                            (Bootstrap.button (t "➕")
                                               ~action:(fun () ->
                                                 Reactive.set path
                                                   (Array.append
                                                      (Reactive.peek path)
                                                      [| "0h" |])));
                                          magic
                                            (Bootstrap.button (t "➖")
                                               ~action:(fun () ->
                                                 let cur = Reactive.peek path in
                                                 Reactive.set path
                                                   (Array.subo
                                                      ~len:(Array.length cur - 1)
                                                      cur)));
                                        ]
                                      in
                                      let name =
                                        [
                                          input
                                            (Reactive.Bidirectional.of_var
                                               account_name)
                                            ~label:(t "Account name:");
                                        ]
                                      in
                                      make
                                        [
                                          row
                                            (List.map
                                               (items @ plus_minus @ name)
                                               ~f:(cell 2));
                                        ]))
                              in
                              div
                                (curve_dropdown % path_form
                                % div
                                    (Fmt.kstr ct "ledger://%s/" animals
                                    % Reactive.bind_var curve ~f:(fun c ->
                                          ct (Curve_derivation.to_string c))
                                    % Reactive.bind_var path ~f:(fun parray ->
                                          Fmt.kstr ct "/%a"
                                            Fmt.(array ~sep:(any "/") string)
                                            parray)
                                    %% Bootstrap.button ~kind:`Primary
                                         (t "Import!") ~action:(fun () ->
                                           Server_communication.import_account
                                             form_state
                                             (Reactive.peek account_name)
                                             (`Ledger_uri
                                               (Fmt.str "ledger://%s/%s/%a"
                                                  animals
                                                  (Reactive.peek curve
                                                 |> Curve_derivation.to_string)
                                                  Fmt.(
                                                    array ~sep:(any "/") string)
                                                  (Reactive.peek path))))))
                            in
                            head %% form)))
              %% try_again ()
          | Wip.WIP ->
              Bootstrap.alert ~kind:`Warning
                (bt "Work-in-progress, you mean to validate a PKH on the ledger"
                %% Bootstrap.spinner (t "Please wait"))
          | Wip.Success () ->
              Reactive.set state Wip.Idle;
              empty ()
          | Wip.Failure msg ->
              Bootstrap.alert ~kind:`Danger (bt msg) %% try_again ())
    | Wip.Failure msg -> Bootstrap.alert ~kind:`Danger (bt msg) %% try_again ())

let import_unencrypted_ui () =
  let open Mono_html in
  let state = Reactive.var `Button in
  let cancel () =
    Bootstrap.button (t "Cancel") ~action:(fun () -> Reactive.set state `Button)
  in
  Reactive.bind_var state ~f:(function
    | `Button ->
        Bootstrap.button (t "Import an unencrypted key-pair") ~action:(fun () ->
            Reactive.set state `Form)
    | `Form ->
        let form_state = Reactive.var Wip.Idle in
        let uri = Reactive.var "" in
        let account_name = Reactive.var "new-account" in
        Reactive.bind_var form_state ~f:(function
          | Wip.WIP ->
              Bootstrap.alert ~kind:`Warning
                (bt "Work-in-progress" %% Bootstrap.spinner (t "Please wait"))
          | Wip.Success () ->
              Reactive.set form_state Wip.Idle;
              empty ()
          | Wip.Failure msg ->
              Bootstrap.alert ~kind:`Danger (bt msg) %% cancel ()
          | Wip.Idle ->
              Bootstrap.Form.(
                make
                  [
                    row
                      [
                        cell 4 (input (Reactive.Bidirectional.of_var uri));
                        cell 2
                          (input (Reactive.Bidirectional.of_var account_name));
                        cell 1
                          (submit_button (t "Go") (fun () ->
                               Server_communication.import_account form_state
                                 (Reactive.peek account_name)
                                 (`Unencrypted_uri (Reactive.peek uri))
                               (* Reactive.set state Wip.WIP *)));
                      ];
                  ])
              %% cancel ()))

let import_address_ui () =
  let open Mono_html in
  let state = Reactive.var `Button in
  let cancel () =
    Bootstrap.button (t "Cancel") ~action:(fun () -> Reactive.set state `Button)
  in
  Reactive.bind_var state ~f:(function
    | `Button ->
        Bootstrap.button (t "Import an Address") ~action:(fun () ->
            Reactive.set state `Form)
    | `Form ->
        let form_state = Reactive.var Wip.Idle in
        let address = Reactive.var "" in
        let account_name = Reactive.var "acccount-42" in
        let is_valid = Reactive.var true in
        Reactive.bind_var form_state ~f:(function
          | Wip.WIP ->
              Bootstrap.alert ~kind:`Warning
                (bt "Work-in-progress" %% Bootstrap.spinner (t "Please wait"))
          | Wip.Success () ->
              Reactive.set form_state Wip.Idle;
              empty ()
          | Wip.Failure msg ->
              Bootstrap.alert ~kind:`Danger (bt msg) %% cancel ()
          | Wip.Idle ->
              Bootstrap.Form.(
                make
                  [
                    row
                      [
                        cell 4
                          (input ~label:(it "Address")
                             (Reactive.Bidirectional.of_var address));
                        cell 2
                          (input
                             ~label:(it "Account (local) name")
                             (Reactive.Bidirectional.of_var account_name));
                        cell 1
                          (submit_button ~active:(Reactive.get is_valid)
                             (t "Go") (fun () ->
                               Server_communication.import_account form_state
                                 (Reactive.peek account_name)
                                 (`Address (Reactive.peek address))
                               (* Reactive.set state Wip.WIP *)));
                      ];
                  ])
              %% cancel ()))

let account_name_choice _ctxt account_name =
  let open Mono_html in
  let choose_account_name =
    it "Account name:"
    %% input_bidirectional (Reactive.Bidirectional.of_var account_name)
  in
  choose_account_name

let better_call_dev_address_link ?shorten _ctxt addr =
  let open Mono_html in
  let text =
    match shorten with None -> addr | Some n -> Stringext.take addr n ^ "…"
  in
  link ~target:(Fmt.str "https://better-call.dev/search?text=%s" addr) (ct text)

let account_short_display ?(add_details = []) ctxt acc =
  let open Account in
  let open Mono_html in
  link ~target:(Fmt.str "#%s" acc.id) (t acc.display_name)
  %
  match add_details with
  | [] -> empty ()
  | one :: more ->
      let show = function
        | `Address_short shorten -> begin
            match get_address acc with
            | None -> empty ()
            | Some addr -> better_call_dev_address_link ctxt addr ~shorten
          end
        | `Balance ->
            Fmt.kstr t "%s ꜩ"
              (Option.value_map ~default:"‽" ~f:Mutez.to_tez_string acc.balance)
      in
      t " "
      % parens
          (List.fold more ~init:(show one) ~f:(fun p m -> p % t "," %% show m))

let short_account_display ctxt =
  let open Mono_html in
  function
  | `Id id ->
      Reactive.bind (State.accounts ctxt) ~f:(fun l ->
          match List.find l ~f:(fun acc -> String.equal acc.id id) with
          | None ->
              Bootstrap.color `Danger (Fmt.kstr bt "account:%s:not-found" id)
          | Some acc -> account_short_display ctxt acc)
  | `Address addr ->
      Reactive.bind (State.accounts ctxt) ~f:(fun l ->
          match
            List.find l ~f:(fun acc ->
                Option.equal String.equal (Account.get_address acc) (Some addr))
          with
          | None -> better_call_dev_address_link ctxt addr
          | Some acc -> account_short_display ctxt acc)

let source_account_choice ctxt originator =
  let open Mono_html in
  Reactive.bind_var originator ~f:(fun choice ->
      Reactive.bind (State.accounts ctxt) ~f:(fun accounts ->
          let open Account in
          List.filter_map accounts ~f:(function
            | { state = Status.Key_pair _; _ } as acc ->
                Some
                  ((match choice with
                   | Some id when String.equal id acc.id -> t "👉"
                   | _ ->
                       Bootstrap.button (t "Choose:") ~action:(fun () ->
                           Reactive.set originator (Some acc.id)))
                  % account_short_display ctxt acc
                      ~add_details:[ `Balance; `Address_short 10 ])
            | _ -> None)
          |> itemize))

let validated_input ?(initial_value = "") _ctxt ~validate ~prompt =
  let open Mono_html in
  let var = Reactive.var initial_value in
  let valid_signal =
    Reactive.map (Reactive.get var) ~f:(fun s ->
        match validate s with
        | n -> Ok n
        | exception Failure s -> Error s
        | exception e -> Error (Fmt.str "%a" Exn.pp e))
  in
  let form =
    span
      (it prompt
      %% input_bidirectional (Reactive.Bidirectional.of_var var)
      %% Reactive.bind valid_signal ~f:(function
           | Ok _ -> t "👍"
           | Error msg -> Fmt.kstr t "👎 (%s)" msg))
  in
  object
    method form = form
    method valid_signal = valid_signal
  end

let rec variables_form ctxt ?(can_be_empty = false) typed_variables =
  let open Mono_html in
  let initialization = Reactive.var [] in
  let open Variable in
  let parse t s =
    if can_be_empty then ()
    else if String.is_empty s then Fmt.failwith "Should not be empty";
    try
      match t with
      | Type.Mutez -> Value.mutez (Z.of_string s)
      | Type.Nat -> Value.nat (Z.of_string s)
      | Type.Time_span -> Value.time_span (Z.of_string s)
      | Type.Address -> Value.address s
      | Type.Pkh_option -> (
          match s with "None" -> Value.none_pkh | s -> Value.some_pkh s)
      | Type.Key_list ->
          Value.key_list (String.split s ~on:',' |> List.map ~f:String.strip)
      (* TODO: try using sexp here for a first attempt *)
      | Type.Weighted_key_list -> Value.weighted_key_list []
      | Type.Signature_option_list ->
          Value.signature_option_list
            (String.split s ~on:','
            |> List.map ~f:(fun s ->
                   match String.strip s with "" -> None | s -> Some s))
      | Type.Bytes ->
          Value.bytes
            (`Hex (String.chop_prefix_if_exists s ~prefix:"0x") |> Hex.to_string)
      | Type.Lambda_unit_to_operation_list ->
          Value.lambda_unit_to_operation_list
            (Tezai_michelson.Untyped.C.concrete s)
      | Type.Timestamp_option -> assert false
      | Type.Unit ->
          assert (String.(s = "Unit"));
          Value.Unit
      | Type.Arbitrary _ -> assert false
    with _ ->
      Fmt.failwith "Failed to parse %S as a value of type %a" s Sexp.pp_hum
        (Type.sexp_of_t t)
  in
  let help_thing typ bidi =
    match typ with
    | Type.Mutez ->
        Reactive.Bidirectional.get bidi
        |> Reactive.bind ~f:(fun s ->
               match parse typ s with
               | Value.Mutez z -> Fmt.kstr ct "= %s ꜩ" (Mutez.to_tez_string z)
               | _ | (exception _) -> empty ())
    | Type.Lambda_unit_to_operation_list ->
        let ui_state = Reactive.var `Button in
        let commented_lambda =
          Reactive.var
            {tz|
DROP; # The Unit
NIL operation; # Start the list of operations
|tz}
        in
        Reactive.bind_var ui_state ~f:(function
          | `Button ->
              Bootstrap.button ~size:`Small ~kind:`Secondary
                (t "Help constructing a lambda") ~action:(fun () ->
                  Reactive.set ui_state `Help_choices)
          | `Help_choices ->
              let commands_state = Reactive.var `Buttons in
              div
                (bt "Constructing a Michelson Lambda:"
                %% Reactive.bind_var commented_lambda ~f:(fun s ->
                       pre (code (t s))))
              %% Reactive.bind_var commands_state ~f:(function
                   | `Buttons ->
                       div
                         (Bootstrap.button ~size:`Small (t "Add transfer")
                            ~action:(fun () ->
                              Reactive.set commands_state `Add_transfer)
                         % Bootstrap.button ~size:`Small (t "Add delegation")
                             ~action:(fun () ->
                               Reactive.set commands_state `Add_delegation))
                   | `Add_transfer ->
                       let choices =
                         variables_form ctxt
                           (Reactive.pure
                              [
                                ("destination", Type.address);
                                ("amount", Type.mutez);
                              ])
                       in
                       choices#form
                       %% Reactive.bind choices#choices_signal ~f:(function
                            | Ok choices ->
                                let vmich k =
                                  try
                                    Value.to_michelson
                                      (List.Assoc.find_exn choices k
                                         ~equal:String.equal)
                                    |> Tezai_michelson.Concrete_syntax.to_string
                                  with _ -> "ERROR:vmich"
                                in
                                Bootstrap.button ~size:`Small (t "Add")
                                  ~action:(fun () ->
                                    Reactive.set commented_lambda
                                      (Reactive.peek commented_lambda
                                      ^ Fmt.str
                                          {tz|{ # Simple transfer
  PUSH address %s;
  CONTRACT unit;
  ASSERT_SOME;
  PUSH mutez %s;
  PUSH unit Unit;
  TRANSFER_TOKENS;
};
CONS; # Adding to the list of operations.
|tz}
                                          (vmich "destination") (vmich "amount")
                                      );
                                    Reactive.set commands_state `Buttons)
                            | Error s -> Fmt.kstr bt "Error: %s" s)
                   | `Add_delegation ->
                       let choices =
                         variables_form ctxt
                           (Reactive.pure [ ("delegate", Type.pkh_option) ])
                       in
                       choices#form
                       %% Reactive.bind choices#choices_signal ~f:(function
                            | Ok choices ->
                                let vmich k =
                                  try
                                    Value.to_michelson
                                      (List.Assoc.find_exn choices k
                                         ~equal:String.equal)
                                    |> Tezai_michelson.Concrete_syntax.to_string
                                  with _ -> "ERROR:vmich"
                                in
                                Bootstrap.button ~size:`Small (t "Add")
                                  ~action:(fun () ->
                                    Reactive.set commented_lambda
                                      (Reactive.peek commented_lambda
                                      ^ Fmt.str
                                          {tz|{ # Delegation
  PUSH (option key_hash) (%s);
  SET_DELEGATE;
};
CONS; # Adding to the list of operations.
|tz}
                                          (vmich "delegate"));
                                    Reactive.set commands_state `Buttons)
                            | Error s -> Fmt.kstr bt "Error: %s" s))
              % br ()
              %% Bootstrap.button (t "Use that") ~action:(fun () ->
                     let code =
                       Reactive.peek commented_lambda
                       |> String.split ~on:'\n'
                       |> List.map ~f:(fun l ->
                              match String.lsplit2 l ~on:'#' with
                              | Some (l, _) -> l
                              | None -> l)
                       |> String.concat ~sep:" "
                     in
                     Reactive.Bidirectional.set bidi (Fmt.str "{ %s }" code);
                     Reactive.set ui_state `Button))
    | _ -> empty ()
  in
  let make_form () =
    Reactive.bind typed_variables ~f:(function
      | [] -> it "There are no variables to fill …"
      | typed_variables ->
          let variable_forms =
            List.map typed_variables ~f:(fun (field, typ) ->
                let bidi =
                  Reactive.Bidirectional.make
                    (Reactive.get initialization
                    |> Reactive.map ~f:(fun l ->
                           match
                             List.Assoc.find l field ~equal:String.equal
                           with
                           | Some s -> s
                           | None -> ""))
                    (fun s ->
                      Reactive.set initialization
                        (List.Assoc.add
                           (Reactive.peek initialization)
                           ~equal:String.equal field s))
                in
                t "Field" %% ct field
                %% Fmt.kstr t " (%a):" Sexp.pp (Variable.Type.sexp_of_t typ)
                %% input_bidirectional bidi
                %% Reactive.bind (Reactive.Bidirectional.get bidi) ~f:(fun s ->
                       match parse typ s with
                       | _ -> t "👌"
                       | exception Failure s -> Fmt.kstr t "❌ (%s)" s)
                %% help_thing typ bidi)
          in
          it "Please fill the following variables:" %% itemize variable_forms)
  in
  let compute_map (typed_variables, variables) =
    List.map typed_variables ~f:(fun (n, t) ->
        match List.Assoc.find variables ~equal:String.equal n with
        | None -> Fmt.failwith "Variable %S is not set" n
        | Some s -> (n, parse t s))
    |> Variable.Map.of_list
  in
  object
    method form = make_form ()

    method choices_signal =
      Reactive.(map (typed_variables ** get initialization)) ~f:(fun l ->
          match compute_map l with
          | s -> Ok s
          | exception Failure s -> Error s
          | exception e -> Error (Fmt.str "%a" Exn.pp e))
  end

let add_account_ui ctxt =
  let open Mono_html in
  let state = Reactive.var `Button in
  let cancel () =
    Bootstrap.button (t "Cancel") ~action:(fun () -> Reactive.set state `Button)
  in
  Reactive.bind_var state ~f:(function
    | `Button ->
        Bootstrap.button (t "New Account") ~action:(fun () ->
            Reactive.set state `Add_account)
    | `Add_account ->
        Bootstrap.button (t "Originate a generic multisig") ~action:(fun () ->
            Reactive.set state `Originate_multisig)
        %% Bootstrap.button (t "Originate a custom contract") ~action:(fun () ->
               Reactive.set state `Originate_contract)
        %% cancel ()
    | `Originate_multisig ->
        let account_name =
          Reactive.var (Fmt.str "new-account-%d" (Random.int 10_000))
        in
        let choose_account_name = account_name_choice ctxt account_name in
        let originator = Reactive.var None in
        let choose_originator =
          it
            "Pick a key-pair to run the origination (a.k.a. “source” or “gas \
             wallet”):"
          %% source_account_choice ctxt originator
        in
        let choose_threshhold =
          validated_input ctxt ~prompt:"Signature threshold:"
            ~validate:(fun s ->
              match Int.of_string s with
              | n when n > 0 -> n
              | _ | (exception _) -> Fmt.failwith "not a positive integer")
        in
        let pubkeys = Reactive.var "" in
        let valid_pubkeys =
          Reactive.map
            Reactive.(choose_threshhold#valid_signal ** get pubkeys)
            ~f:(fun (thr, s) ->
              match
                (thr, String.split s ~on:',' |> List.map ~f:String.strip)
              with
              | _, [ "" ] -> Error "cannot be empty"
              | Ok trh, l when List.length l >= trh -> Ok l
              | Error _, l when List.length l > 0 -> Ok l
              | Ok trh, l when List.length l < trh ->
                  Error "you need more than the threashold, duh"
              | _ | (exception _) -> Error "nope, nope")
        in
        let choose_pubkeys =
          it "Public keys (comma separated):"
          %% input_bidirectional (Reactive.Bidirectional.of_var pubkeys)
          %% Reactive.bind valid_pubkeys ~f:(function
               | Ok _ -> t "👍"
               | Error msg -> Fmt.kstr t "👎 (%s)" msg)
        in
        let call_state = Reactive.var Wip.Idle in
        bt "Originating multisig contract"
        %% div choose_account_name %% div choose_originator
        %% div choose_threshhold#form %% div choose_pubkeys
        % div
            (Reactive.(
               bind
                 (get call_state ** valid_pubkeys
                ** choose_threshhold#valid_signal ** get originator
                ** get account_name)) ~f:(function
              | Wip.WIP, _ -> Bootstrap.spinner (t "WIP")
              | Wip.Success (), _ -> bt "Done: 👍"
              | Wip.Failure s, _ -> Fmt.kstr bt "FAILURE: %s" s
              | ( Wip.Idle,
                  (Ok keys, (Ok threshold, (Some originator, account_name))) )
                ->
                  Bootstrap.button (t "Go!") ~action:(fun () ->
                      let msg =
                        Protocol.Message.Up.create_generic_multisig_account
                          ~name:account_name ~keys ~threshold
                          ~gas_wallet:originator ~comments:None
                      in
                      Server_communication.call_with_var_unit call_state msg;
                      dbgf "TODO ")
              | _ ->
                  Bootstrap.button ~disabled:true (t "No ready to Go!")
                    ~action:(fun () -> dbgf "TODO")))
        % div (cancel ())
    | `Originate_contract ->
        let account_name =
          Reactive.var (Fmt.str "new-account-%d" (Random.int 10_000))
        in
        let choose_account_name = account_name_choice ctxt account_name in
        let originator = Reactive.var None in
        let choose_originator =
          it
            "Pick a key-pair to run the origination (a.k.a. “source” or “gas \
             wallet”):"
          %% source_account_choice ctxt originator
        in
        let entrypoints = Reactive.var [] in
        let choose_entrypoints =
          let add_button ~initial_name content ~control ~ability =
            Bootstrap.button ~size:`Small content ~action:(fun () ->
                let current = Reactive.peek entrypoints in
                let name =
                  let rec find_name n =
                    let nn =
                      if n = 0 then initial_name
                      else Fmt.str "%s_%d" initial_name n
                    in
                    match
                      List.exists current ~f:(function
                        | { Entrypoint.name; _ } when String.equal nn name ->
                            true
                        | _ -> false)
                    with
                    | true -> find_name (n + 1)
                    | false -> nn
                  in
                  find_name 0
                in
                Reactive.set entrypoints
                  (Entrypoint.make name ~control ~ability :: current))
          in
          add_button
            (t "Add default entrypoint")
            ~initial_name:"default" ~control:Control.anyone
            ~ability:Ability.do_nothing
          %% add_button
               (t "Add admin-can-set-delegate entrypoint")
               ~initial_name:"admin_delegates"
               ~control:
                 (Control.sender_is (Constant_or_variable.variable "admin"))
               ~ability:Ability.set_delegate
          %% add_button
               (t "Add admin-can-withdraw-100 entrypoint")
               ~initial_name:"admin_withdraw_allowance"
               ~control:
                 (Control.sender_is (Constant_or_variable.variable "admin"))
               ~ability:
                 (Ability.transfer_mutez
                    ~amount:
                      (`Limit (Constant_or_variable.constant (Z.of_int 100)))
                    ~destination:
                      (`Address (Constant_or_variable.variable "admin")))
          %% add_button
               (t "Add admin-can-transfer entrypoint")
               ~initial_name:"admin_allowance"
               ~control:
                 (Control.sender_is (Constant_or_variable.variable "admin"))
               ~ability:
                 (Ability.transfer_mutez
                    ~amount:(`Limit (Constant_or_variable.variable "allowance"))
                    ~destination:`Anyone)
          %% Reactive.bind_var entrypoints ~f:(fun eps ->
                 let entrypoint_form ith ep =
                   let open Entrypoint in
                   let editable_sexp _ctxt ~initial ~set ~to_sexp ~of_sexp =
                     let state = Reactive.var `Button in
                     Reactive.bind_var state ~f:(function
                       | `Button ->
                           Bootstrap.button ~size:`Small ~kind:`Secondary
                             (ct (to_sexp initial |> Sexp.to_string_hum))
                             ~action:(fun () -> Reactive.set state `Editor)
                       | `Editor ->
                           let this_bidi =
                             Reactive.var (to_sexp initial |> Sexp.to_string_hum)
                           in
                           let bidi = Reactive.Bidirectional.of_var this_bidi in
                           input_bidirectional bidi
                           %% Reactive.bind (Reactive.Bidirectional.get bidi)
                                ~f:(fun s ->
                                  match of_sexp (Sexplib.Sexp.of_string s) with
                                  | v ->
                                      Bootstrap.button ~size:`Small
                                        ~kind:`Success (t "✅")
                                        ~action:(fun () ->
                                          set v;
                                          Reactive.set state `Button)
                                  | exception _ -> t "❌"))
                   in
                   strong (Fmt.kstr ct "%%%s" ep.name)
                   %% it "Access-control:"
                   %% editable_sexp ctxt ~initial:ep.control
                        ~of_sexp:Control.t_of_sexp ~to_sexp:Control.sexp_of_t
                        ~set:(fun c ->
                          Reactive.set entrypoints
                            (List.mapi (Reactive.peek entrypoints)
                               ~f:(fun i v ->
                                 if i = ith then { v with control = c } else v)))
                   %% it "Action:"
                   %% editable_sexp ctxt ~initial:ep.ability
                        ~of_sexp:Ability.t_of_sexp ~to_sexp:Ability.sexp_of_t
                        ~set:(fun c ->
                          Reactive.set entrypoints
                            (List.mapi (Reactive.peek entrypoints)
                               ~f:(fun i v ->
                                 if i = ith then { v with ability = c } else v)))
                   % Bootstrap.button ~size:`Small (t "Remove")
                       ~action:(fun () ->
                         Reactive.set entrypoints
                           (List.filteri (Reactive.peek entrypoints)
                              ~f:(fun i _ -> i <> ith)))
                 in
                 itemize (List.mapi eps ~f:entrypoint_form))
        in
        let typed_variables =
          Reactive.(map (get entrypoints)) ~f:(fun eps ->
              let fake_spec = Smart_contract.make eps in
              Smart_contract.collect_variables fake_spec)
        in
        let variables_choice = variables_form ctxt typed_variables in
        let call_state = Reactive.var Wip.Idle in
        bt "Originating Multi contract"
        %% div choose_account_name %% div choose_originator
        %% div (bt "Entrypoints" %% choose_entrypoints)
        %% variables_choice#form
        % div
            (Reactive.(
               bind
                 (get call_state ** get originator ** get account_name
                ** get entrypoints ** variables_choice#choices_signal))
               ~f:(function
              | Wip.WIP, _ -> Bootstrap.spinner (t "WIP")
              | Wip.Success (), _ -> bt "Done: 👍"
              | Wip.Failure s, _ -> Fmt.kstr bt "FAILURE: %s" s
              | ( Wip.Idle,
                  ( Some originator,
                    (account_name, (entrypoints, Ok initialization)) ) ) ->
                  Bootstrap.button (t "Go!") ~action:(fun () ->
                      let msg =
                        Protocol.Message.Up.create_smart_contract ~entrypoints
                          ~initialization ~name:account_name
                          ~gas_wallet:originator ~comments:None
                      in
                      Server_communication.call_with_var_unit call_state msg;
                      dbgf "TODO ")
              | _ ->
                  Bootstrap.button (t "Go!")
                    ~action:(fun () -> ())
                    ~disabled:true))
        %% cancel ())

let tests ctxt =
  let open Mono_html in
  let action_button msg action =
    let form_state = Reactive.var Wip.Idle in
    Reactive.bind_var form_state ~f:(function
      | Wip.WIP ->
          Bootstrap.alert ~kind:`Warning
            (bt "Work-in-progress" %% Bootstrap.spinner (t "Please wait"))
      | Wip.Success () ->
          Reactive.set form_state Wip.Idle;
          empty ()
      | Wip.Failure msg -> Bootstrap.alert ~kind:`Danger (bt msg)
      | Wip.Idle ->
          Bootstrap.button ~size:`Small msg ~action:(fun () ->
              action form_state))
  in
  let add_friend account_name address =
    action_button
      (t "Import “friend”:" %% ct address %% t "as" %% it account_name)
      (fun form_state ->
        Server_communication.import_account form_state account_name
          (`Address address)
        (* Reactive.set state Wip.WIP *))
  in
  p
    (t "The window is "
    % (Browser_window.width ctxt
      |> Reactive.bind ~f:(function
           | None -> t "Unknown"
           | Some `Thin -> it "Thin"
           | Some `Wide -> bt "Wide")))
  % itemize
      [
        begin
          let account_name = Fmt.str "m-%d" (Random.int 10000) in
          let address = "KT1WSUAUrqBaDou2DPkysu4nNQUoUyHgtRKz" in
          action_button
            (t "Import multisg:" %% ct address %% t "as" %% it account_name)
            (fun form_state ->
              Server_communication.import_account form_state account_name
                (`Address address)
              (* Reactive.set state Wip.WIP *))
        end;
        add_friend "bob" "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6";
        add_friend "broke-friend" "tz1LsdMMzfrXoBGMwWX9oPDoD4kU3Pyn2ei5";
        add_friend
          (Fmt.str "f-%d" (Random.int 10000))
          "tz1PEdtfNy9pVPjXaeH39DupR1UxCe8EgPio";
        add_friend "baker-box0" "tz1YPSCGWXwBdTncK2aCctSZAXWvGsGwVJqU";
        begin
          let account_name = "alice" in
          let uri =
            "unencrypted:edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq"
          in
          action_button
            (t "Import " %% ct uri %% t "as" %% it account_name)
            (fun form_state ->
              Server_communication.import_account form_state account_name
                (`Unencrypted_uri uri))
        end;
        begin
          let account_name = "alice-box0-tz" in
          let alice_pk =
            "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn"
          in
          let bob_pk =
            "edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4"
          in
          let hello_pk =
            "edpkuoK2J2UVbDcSTdJgP85JmDN3gxBCswcgApbtY5d7zHVunwCKNR"
          in
          Reactive.bind (State.accounts ctxt) ~f:(fun accounts ->
              let open Account in
              match
                List.find_map accounts ~f:(function
                  | { id; state = Key_pair Key_pair.{ public_key_hash; _ }; _ }
                    when String.equal public_key_hash
                           "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" ->
                      Some id
                  | _ -> None)
              with
              | None -> bt "Add Alice do add a multisig"
              | Some gas_wallet ->
                  action_button
                    (t
                       "Originate generic-multisig 1/3 with alice, bob, and \
                        somebody else"
                    %% it "as" %% ct "alice")
                    (fun form_state ->
                      let msg =
                        let keys = [ alice_pk; bob_pk; hello_pk ] in
                        let threshold = 1 in
                        Protocol.Message.Up.create_generic_multisig_account
                          ~name:account_name ~keys ~threshold ~gas_wallet
                          ~comments:None
                      in
                      Server_communication.call_with_var_unit form_state msg))
        end;
        begin
          let account_name = "ledger-abandon" in
          let uri = "ledger://barren-gharial-uneven-giraffe/bip25519/0h/0h" in
          action_button
            (t "Import " %% ct uri %% t "as" %% it account_name)
            (fun form_state ->
              Server_communication.import_account form_state account_name
                (`Ledger_uri uri))
        end;
        action_button (t "Add GUI-failure") (fun _ ->
            let date =
              Date_js.of_float
                Float.(
                  let d = Unix.gettimeofday () in
                  round_up (d / 10.) * 10.)
              |> Date_js.to_iso_string
            in
            Fmt.kstr
              (State.Local_failures.add_message ctxt)
              "Failure added from the button, resolution 10s: %s" date);
        action_button (t "Add a fake alert") (fun _ ->
            let open Protocol.Message.Down.Alert in
            let date =
              Date_js.of_float (Unix.gettimeofday ()) |> Date_js.to_iso_string
            in
            let id = Fmt.str "fake-alert-%s" date in
            let content =
              ledger_wants_human_intervention ~ledger_id:"some-ledger-id"
                ~ledger_name:"ledger-name"
                ~reason:"This is fake, there is no reason."
            in
            State.Alerts.add ctxt (make ~id ~content));
        action_button (t "Remove one of the fake-alerts randomly") (fun _ ->
            let peeked = Reactive.peek (State.Alerts.get_var ctxt) in
            let open Protocol.Message.Down.Alert in
            let to_remove =
              List.find_map peeked ~f:(function
                | { id; _ } when String.is_prefix id ~prefix:"fake-alert" ->
                    Some id
                | _ -> None)
            in
            match to_remove with
            | Some id -> State.Alerts.remove ctxt id
            | None ->
                State.Local_failures.add_message ctxt
                  "Cannot find a fake-alert to remove");
      ]

let show_value _ =
  let open Mono_html in
  let open Variable.Value in
  function
  | Nat z -> t (Z.to_string z)
  | other -> Fmt.kstr ct "%a" Sexp.pp_hum (sexp_of_t other)

let call_button ctxt (acc : Account.t) =
  let open Mono_html in
  let state = Reactive.var `Button in
  let cancel () =
    Bootstrap.button (t "Cancel") ~action:(fun () -> Reactive.set state `Button)
  in
  let call_entrypoint_form ~address ~source_account ep =
    let ( amount_must_be_zero,
          ep_name,
          required_parameters,
          title,
          wants_sigs,
          contract_variables ) =
      match ep with
      | `Transfer ->
          ( false,
            "default",
            [],
            bt "Doing a simple transfer",
            false,
            Variable.Record.empty )
      | `Call_generic_multisig_main_update_keys variables ->
          ( true,
            "main/update_keys",
            [
              ("threshold", Variable.Type.nat); ("keys", Variable.Type.key_list);
            ],
            bt "Calling the multisig's main entrypoint to update its parameters",
            true,
            variables )
      | `Call_generic_multisig_main_lambda variables ->
          ( true,
            "main/lambda",
            [ ("lambda", Variable.Type.lambda_unit_to_operation_list) ],
            bt "Calling the multisig's main entrypoint with a generic lambda",
            true,
            variables )
      | `Entrypoint (ep, variables) ->
          ( false,
            Entrypoint.name ep,
            Entrypoint.collect_parameters ep,
            bt "Calling entrypoint" %% ct ep.name,
            false,
            variables )
    in
    let variables_choice =
      variables_form ctxt (Reactive.pure required_parameters)
    in
    let amount =
      match amount_must_be_zero with
      | true ->
          object
            method form =
              div (it "This entrypoint requires the amount to be 0 ꜩ")

            method valid_signal = Reactive.pure (Ok Z.zero)
          end
      | false ->
          validated_input ctxt ~prompt:"Transfer ammount (μꜩ):"
            ~validate:(fun s ->
              match Z.of_string s with
              | _ when String.equal s "" -> Fmt.failwith "empty amount"
              | n when Z.geq n Z.zero -> n
              | _ | (exception _) -> Fmt.failwith "not a positive integer")
    in
    let variable field show =
      match Variable.Record.get contract_variables field with
      | v -> show v
      | exception Failure f -> Bootstrap.color `Danger (bt f)
    in
    let signatures_choice =
      variables_form ctxt ~can_be_empty:(not wants_sigs)
        (Reactive.pure [ ("signatures", Variable.Type.signature_option_list) ])
    in
    let signatures_form () =
      let form_state = Reactive.var `Button in
      let with_keys ~blob = function
        | Variable.Value.Key_list kl ->
            let accounts =
              let all = [] in
              List.map kl ~f:(fun pk ->
                  List.find_map all
                    ~f:
                      Account.(
                        fun acc ->
                          match acc.state with
                          | Status.Key_pair { public_key; _ }
                            when String.equal public_key pk ->
                              Some (`Owned (acc, pk))
                          | Status.Friend { public_key = Some fpk; _ }
                            when String.equal fpk pk ->
                              Some (`Friend (acc, pk))
                          | _ -> None)
                  |> Option.value ~default:(`Unknown pk))
            in
            itemize
              (List.map accounts ~f:(function
                | `Unknown pk ->
                    t "Key" %% ct pk %% t "is totally unknown to us."
                | `Friend (acc, pk) ->
                    t "Key" %% ct pk
                    %% t "is in the friend-account"
                    %% account_short_display ctxt acc
                | `Owned (acc, pk) ->
                    let sign_state = Reactive.var Wip.Idle in
                    t "Key" %% ct pk %% t "is yours:"
                    %% account_short_display ctxt acc
                    %% Reactive.bind_var sign_state ~f:(function
                         | Wip.Idle ->
                             Bootstrap.button ~kind:`Primary ~size:`Small
                               (t "Sign Now!") ~action:(fun () ->
                                 let open Protocol.Message in
                                 Server_communication.call_with_var sign_state
                                   (Up.sign_blob ~account:acc.id ~blob)
                                   (function
                                   | Down.Signature s -> s
                                   | _ -> assert false))
                         | Wip.Success s -> ct s
                         | Wip.Failure s -> Fmt.kstr bt "FAILURE: %s" s
                         | Wip.WIP -> bt "TODO")))
        | _ -> Bootstrap.color `Danger (bt "Wrong value for `keys`")
      in
      let show_blob blob =
        let hex_blob = Bytes_rep.to_zero_x blob in
        let tzcomet =
          (* "https://tzcomet.io/#/editor%253Feditor-input%253D" ^ hex_blob in *)
          "https://tzcomet.io/#/editor"
        in
        t "We need to get at least"
        %% variable "threshold" (show_value ctxt)
        %% t "users to sign:" %% Fmt.kstr ct "%s" hex_blob
        % div
            (t "📎 You can try TZComet's blob parser to check the bytes:"
            %% link ~target:tzcomet (t tzcomet))
        %% div
             (t "You can ask any" %% ct "octez-client"
            %% t "user to sign using:"
             %% pre
                  (code
                     (t "octez-client sign bytes"
                     %% bt "\"$blob\"" %% t "for" %% bt "their-key-pair")))
        %% variable "keys" (with_keys ~blob)
      in
      signatures_choice#form
      %% Reactive.bind_var form_state ~f:(function
           | `Button ->
               Bootstrap.button ~size:`Small ~kind:`Info
                 (t "Help making signatures") ~action:(fun () ->
                   Reactive.set form_state `Form)
           | `Form ->
               Reactive.bind variables_choice#choices_signal ~f:(function
                 | Ok parameters ->
                     let call_state = Reactive.var Wip.Idle in
                     Server_communication.call_with_var call_state
                       (Protocol.Message.Up.make_blob_for_signature
                          ~target_contract_id:acc.id ~entrypoint:ep_name
                          ~parameters) (function
                       | Protocol.Message.Down.Blob blob -> blob
                       | _ -> assert false);
                     Reactive.bind_var call_state ~f:(function
                       | Wip.Idle | Wip.WIP -> Bootstrap.spinner (t "WIP")
                       | Wip.Success blob -> show_blob blob
                       | Wip.Failure s -> Fmt.kstr bt "FAILURE: %s" s)
                 | Error _ ->
                     Bootstrap.color `Danger
                       (t "Parameters must be properly filled 🡅")))
    in
    let call_state = Reactive.var Wip.Idle in
    div title %% div amount#form %% variables_choice#form
    %% (if wants_sigs then div (signatures_form ()) else empty ())
    %% Reactive.(
         bind
           (get call_state ** amount#valid_signal ** get source_account
          ** variables_choice#choices_signal ** signatures_choice#choices_signal
           )
           ~f:(function
             | Wip.WIP, _ -> Bootstrap.spinner (t "WIP")
             | Wip.Success (), _ -> bt "Done: 👍"
             | Wip.Failure s, _ -> Fmt.kstr bt "FAILURE: %s" s
             | Wip.Idle, (Ok amount, (Some source, (Ok parameters, sigs))) ->
                 let signatures =
                   match sigs with
                   | Ok s -> s
                   | Error s ->
                       dbgf "sigs error: %s" s;
                       []
                 in
                 Bootstrap.button
                   (Fmt.kstr t "Go (%s XTZ!)" (Mutez.to_tez_string amount))
                   ~action:(fun () ->
                     let msg =
                       let specification =
                         Operation.Specification.transfer ~destination:address
                           ~entrypoint:ep_name
                           ~parameters:(parameters @ signatures) ~source ~amount
                       in
                       Protocol.Message.Up.order_operation ~specification
                         ~comments:None
                     in
                     Server_communication.call_with_var_unit call_state msg;
                     ())
             | Wip.Idle, _ -> bt "Not ready to go :("))
  in
  let callable ~address eps =
    Reactive.bind_var state ~f:(function
      | `Button ->
          let title =
            String.concat ~sep:"/"
              ((if
                List.exists eps ~f:(function `Transfer -> false | _ -> true)
               then [ "Call 📞" ]
               else [])
              @
              if List.mem eps ~equal:Poly.equal `Transfer then
                [ "Transfer-to 💸" ]
              else [])
          in
          Bootstrap.button (t title) ~action:(fun () ->
              Reactive.set state `Call)
      | `Call ->
          div
            (let source_account = Reactive.var None in
             let source_ui =
               it "Pick a key-pair as “source”:"
               %% source_account_choice ctxt source_account
             in
             let entrypoint =
               Reactive.var (match eps with [ one ] -> Some one | _ -> None)
             in
             let entrypoint_form =
               div
               @@ Reactive.bind_var entrypoint ~f:(function
                    | None ->
                        list
                          (List.map eps ~f:(fun ep ->
                               let name =
                                 match ep with
                                 | `Transfer -> t "Regular ꜩ transfer"
                                 | `Call_generic_multisig_main_update_keys _ ->
                                     t "Call The Multisig's"
                                     %% ct "main/update_keys" %% t "Entrypoint"
                                 | `Call_generic_multisig_main_lambda _ ->
                                     t "Call The Multisig's" %% ct "main/lambda"
                                     %% t "Entrypoint"
                                 | `Entrypoint ({ Entrypoint.name; _ }, _) ->
                                     t "Call Entrypoint" %% ct name
                               in
                               Bootstrap.button name ~action:(fun () ->
                                   Reactive.set entrypoint (Some ep))))
                    | Some ep ->
                        call_entrypoint_form ~address ep ~source_account)
             in
             source_ui %% entrypoint_form %% cancel ()))
  in
  match acc.state with
  | Account.Status.Draft _ | Account.Status.Contract_failed_to_originate _
  | Account.Status.Contract_to_originate _ ->
      empty ()
  | Account.Status.Originated_contract { contract; address } ->
      let eps =
        match contract with
        | Smart_contract.Generic_multisig { variables } ->
            [
              `Transfer;
              `Call_generic_multisig_main_update_keys variables;
              `Call_generic_multisig_main_lambda variables;
            ]
        | Custom { entrypoints; can_receive_funds; variables } ->
            List.map entrypoints ~f:(fun ep ->
                if String.equal ep.name "default" && can_receive_funds then
                  `Transfer
                else `Entrypoint (ep, variables))
        | Foreign _ -> []
      in
      callable ~address eps
  | Account.Status.Key_pair { public_key_hash; _ }
  | Account.Status.Friend { public_key_hash; public_key = _ } ->
      callable ~address:public_key_hash [ `Transfer ]

let display_operations ctxt =
  let open Mono_html in
  let operation op =
    let open Operation in
    let kind =
      match op.status with
      | Status.Paused | Status.Ordered -> `Primary
      | Status.Work_in_progress _ -> `Warning
      | Status.Success _ -> `Success
      | Status.Failed _ -> `Danger
    in
    let spec_summary =
      match op.order with
      | Specification.Draft _ -> bt "Draft"
      | Specification.Origination
          { account; specification; initialization = _; gas_wallet } ->
          Fmt.kstr t "Origination of %s contract"
            (match specification with
            | Custom _ -> "custom"
            | Generic_multisig _ -> "generic-multisig"
            | Foreign _ -> "foreign")
          %% short_account_display ctxt (`Id account)
          %% t "by"
          %% short_account_display ctxt (`Id gas_wallet)
      | Specification.Transfer
          { source; destination; amount; entrypoint; parameters } ->
          (match parameters with
          | [] -> t "Pure transfer of"
          | _ -> t "Call" %% ct entrypoint %% t "with amount")
          %% Fmt.kstr t "%s ꜩ" (Mutez.to_tez_string amount)
          %% short_account_display ctxt (`Id source)
          %% bt "→"
          %% short_account_display ctxt (`Address destination)
    in
    let time_info =
      let open Float in
      Reactive.bind (State.get_low_resolution_date ctxt `Seconds_10)
        ~f:(fun now ->
          match now - op.last_update with
          | s when s < 5. -> it "Seconds ago"
          | s when s < 60. * 55. ->
              let mins = to_int (s / 60. |> round_up) in
              Fmt.kstr it "About %d minute%s ago" mins
                Int.(if mins = 1 then "" else "s")
          | s when s < 60. * 60. *. 10. ->
              let hours = to_int (s / (60. * 60.) |> round_up) in
              Fmt.kstr it "About %d hour%s ago" hours
                Int.(if hours = 1 then "" else "s")
          | _ ->
              let date_string =
                (new%js Js_of_ocaml.Js.date_fromTimeValue
                   (1000. *. op.last_update))##toISOString
                |> Js_of_ocaml__Js.to_string
              in
              Fmt.kstr it "On %s" date_string)
    in
    Bootstrap.alert ~kind
      (* ~a:[H5.a_id (Reactive.pure op.id)] *)
      (ct op.id %% time_info %% spec_summary
      %% details ~summary:(t "Full details")
           (pre
              ~a:[ H5.a_style (Reactive.pure "font-size: 80%") ]
              (Fmt.kstr ct "%a" Sexp.pp_hum (Operation.sexp_of_t op)))
      % div
          (t "Test:"
          % Custom_widget.inline_button (t "open as draft") ~action:(fun () ->
                Web_app_state.Current_page.change_to ctxt
                  (Web_app_state.Page.draft_operation ~id:(Some op.id)))))
  in
  let table_of_operations table =
    Reactive.Table.sort_extract_transform table
      ~compare:Web_app_state.Operation_row.compare
      ~extract:(* TODO: pagination *) Fn.id
      ~transform:(fun orow ->
        Web_app_state.bind_operation ctxt orow.id ~f:operation)
      ~reduce:(fun a b -> a % b)
  in
  h4 (t "Recent") %% table_of_operations (State.operations ctxt)

let render ctxt =
  let open Mono_html in
  Bootstrap.container ~suffix:"-fluid"
    (h2 (t "Babuso: Bank But Self-Operated")
    % div
        (h3 (t "Mainnet Info")
        %% Reactive.bind_var (State.exchange_rates ctxt) ~f:(function
             | None -> bt "Exchange info not available"
             | Some rates ->
                 let open Exchange_rates in
                 let one k f = bt k %% t "→" %% Fmt.kstr ct "%g" f in
                 t "Exchange info:"
                 %% itemize
                      [
                        one "USD" rates.usd;
                        one "EUR" rates.eur;
                        one "BTC" rates.btc;
                        one "ETH" rates.eth;
                      ]))
    % div
        (let configuration = Reactive.var Wip.Idle in
         let button s =
           Bootstrap.button (bt s) ~action:(fun () ->
               Server_communication.get_configuration configuration)
         in
         t "Configuration:"
         %% Reactive.bind_var configuration ~f:(function
              | Wip.Idle -> button "Get"
              | Wip.WIP -> Bootstrap.spinner (t "WIP")
              | Wip.Failure s ->
                  Bootstrap.label `Danger (bt s) %% button "Get again"
              | Wip.Success blob ->
                  Custom_widget.Debug.sexpable Wallet_configuration.sexp_of_t
                    blob
                  %% button "Get again"))
    % div
        (h3 (t "Navigation History")
        % Reactive.bind (Web_app_state.Current_page.navigation_history ctxt)
            ~f:(fun pl ->
              itemize
                (List.map pl ~f:(fun p ->
                     ct (Sexp.to_string_hum (Web_app_state.Page.sexp_of_t p)))))
        )
    % div (h3 (t "Operations") % display_operations ctxt)
    % div
        (h3 (t "Accounts")
        % Reactive.bind (State.accounts ctxt) ~f:(fun l ->
              list
                (List.map l
                   ~f:(fun
                        (Account.
                           {
                             id;
                             display_name;
                             balance;
                             state;
                             comments = _;
                             history = _;
                           } as acc)
                      ->
                     let remove = Reactive.var Wip.Idle in
                     let remove_button id =
                       Reactive.bind_var remove ~f:(function
                         | Wip.Idle ->
                             Bootstrap.button ~size:`Small (t "❌")
                               ~action:(fun () ->
                                 Server_communication.delete_account remove ~id)
                         | Wip.WIP | Wip.Success () ->
                             Bootstrap.spinner (t "WIP")
                         | Wip.Failure s -> Bootstrap.label `Danger (bt s))
                     in
                     let bcd =
                       match Account.get_address acc with
                       | None -> empty ()
                       | Some add -> better_call_dev_address_link ctxt add
                     in
                     div
                       ~a:[ style "padding: 5px; border: 2px solid #999" ]
                       (a ~a:[ H5.a_id (Reactive.pure id) ] (bt display_name)
                       %% bcd
                       %% (match balance with
                          | None -> it "Unknown balance"
                          | Some i ->
                              Fmt.kstr it "Balance: %s ꜩ"
                                (Mutez.to_tez_string i))
                       %% remove_button id %% call_button ctxt acc
                       %% div
                            (pre
                               ~a:
                                 [
                                   H5.a_style
                                     (Reactive.pure
                                        "background-color: #ddd; padding: \
                                         10px; font-family: mono; font-size: \
                                         75%; color: #040; line-height: 1.01");
                                 ]
                               (Fmt.kstr t "%a"
                                  (fun ppf sexp ->
                                    Caml.Format.pp_safe_set_geometry ppf
                                      ~margin:140 ~max_indent:90;
                                    Sexp.pp_hum_indent 2 ppf sexp)
                                  (Account.Status.sexp_of_t state))))))))
    % div
        (h3 (t "Actions")
        % div (add_account_ui ctxt)
        % import_ledger_ui () % import_unencrypted_ui () % import_address_ui ()
        )
    % div (h3 (t "Tests") %% tests ctxt)
    % div
        (h3 (t "Error/Warnings")
        % Reactive.bind_var (State.failures ctxt) ~f:(function
            | [] -> t "None 💪"
            | more ->
                itemize
                  (List.map
                     ~f:(fun lf ->
                       ct
                         (Sexp.to_string_hum (State.Local_failure.sexp_of_t lf)))
                     more))))
