open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

let code_block lines =
  let open Mono_html in
  Bootstrap.alert ~kind:`Dark
    (pre
       ~a:[ style "overflow-x: scroll; overflow-y: scroll" ]
       (code (t (String.concat ~sep:"\n" lines))))

let ui_errorf (_ : _ Context.t) fmt =
  let open Mono_html in
  Fmt.kstr
    (fun s -> Bootstrap.alert ~kind:`Danger (bt "Error" %% pre (ct s)))
    fmt

let wip_sexp f v =
  let open Mono_html in
  span
    ~a:[ style "font-weight: bold; color: #500; font-family: monospace" ]
    (Fmt.kstr t "[TODO: %a]" Sexp.pp_hum (f v))

module Debug = Debug_content
module Balance = Balances_display

module Prosaic_message = struct
  open Babuso_lib.Import.Prosaic_message

  let inline_to_html _ctxt =
    let open Mono_html in
    function Text tx -> t tx

  let to_html ctxt = function Inline inl -> inline_to_html ctxt inl
end

(** A pre-filled call to {!Bootstrap.button} for homogeneity. *)
let inline_button ?a ?disabled content ~action =
  Bootstrap.button content ?a ?disabled ~size:`Small ~outline:true ~kind:`Info
    ~action

let wip_div ?(error = Mono_html.t) ?(with_spinner = fun c -> Mono_html.div c)
    ?(idle = Mono_html.empty) wip ~success =
  let open Mono_html in
  Reactive.bind_var wip ~f:(function
    | Wip.Idle -> idle ()
    | WIP -> with_spinner (Bootstrap.spinner (t "WIP"))
    | Failure s -> Bootstrap.alert ~kind:`Danger (error s)
    | Success a -> success a)

let div_title c =
  let open Mono_html in
  Bootstrap.div_lead
    ~a:
      [ classes [ "text-primary" ]; style "font-weight: bold; font-size: 150%" ]
    c

let identifier_with_links ?shorten ?(links = []) _ctxt addr =
  let open Mono_html in
  let text =
    match shorten with None -> addr | Some n -> Stringext.take addr n ^ "…"
  in
  let make_link = function
    | `BCD ->
        link (t "BCD")
          ~target:(Fmt.str "https://better-call.dev/search?text=%s" addr)
  in
  let extras =
    match links with
    | [] -> empty ()
    | one :: more ->
        t " "
        % parens
            (List.fold more ~init:(make_link one) ~f:(fun prev other ->
                 prev % t "|" % make_link other))
  in
  ct text % extras

let address_with_links = identifier_with_links

let tzkt_hash_link ?(shorten = Some 10) ctxt hash =
  let open Mono_html in
  let as_code =
    match shorten with
    | None -> ct hash
    | Some len -> ct (String.subo hash ~len ^ "…")
  in
  Reactive.bind (Web_app_state.Network_info.public_network ctxt) ~f:(function
    | None -> as_code
    | Some net ->
        let target =
          match net with
          | `Mainnet -> Fmt.str "https://tzkt.io/%s" hash
          | `Kathmandunet -> Fmt.str "https://kathmandunet.tzkt.io/%s" hash
          | `Ghostnet -> Fmt.str "https://ghostnet.tzkt.io/%s" hash
        in
        link as_code ~target)

let block_hash ?shorten ctxt hash = tzkt_hash_link ?shorten ctxt hash
let operation_hash ?shorten ctxt hash = tzkt_hash_link ?shorten ctxt hash

let protocol _ctxt proto =
  let open Mono_html in
  let open Tzrotocol in
  match proto with
  | { kind; hash = Some h } ->
      Fmt.kstr it "%s (%s …)" (Kind.name kind) (String.subo h ~len:13)
  | { kind; hash = None } -> Fmt.kstr it "%s" (Kind.name kind)

let network_link (_ctxt : < .. >) network =
  let open Mono_html in
  let open Network in
  let looks_like_flextesa chain_id =
    (* We look for flextesa's vanity chain-IDs *)
    try
      String.is_suffix chain_id ~suffix:"SBox"
      || String.is_substring_at chain_id ~pos:11 ~substring:"Box"
    with _ -> false
  in
  match network with
  | Mainnet ->
      link
        (Bootstrap.color `Warning (t "Mainnet"))
        ~target:"https://tezos.gitlab.io/#mainnet"
  | Kathmandunet ->
      link (t "Kathmandunet") ~target:"https://teztnets.xyz/kathmandunet-about"
  | Ghostnet ->
      link (t "Ghostnet") ~target:"https://teztnets.xyz/ghostnet-about"
  | Sandbox s when looks_like_flextesa s ->
      link
        (t "Sandbox" %% parens (ct s))
        ~target:"https://gitlab.com/tezos/flextesa"
  | Sandbox s -> t "Sandbox" %% parens (ct s)

let thumbs_up_or_down ~ok ~error =
  let open Mono_html in
  function
  | Ok o -> t "👍" %% Bootstrap.color `Success (ok o)
  | Error e -> t "👎" %% Bootstrap.color `Danger (error e)

let icon_with_hover ?(a = []) icon text =
  let a_ = a in
  let open Mono_html in
  span ~a:(H5.a_title (Reactive.pure text) :: a_) (t icon)

let thumbs_up_or_down_string =
  let open Mono_html in
  thumbs_up_or_down ~error:t

let copy_to_clipboard_button ?(min_width = "10em") _ctxt ~input_id =
  let open Mono_html in
  let copying = Wip.idle_var () in
  let icon =
    Reactive.bind_var copying ~f:(function
      | Wip.Idle -> t "Copy 📋"
      | Wip.WIP -> t "Copying ⌛"
      | Wip.Success () -> t "Copied 👍"
      | Wip.Failure s -> t "ERROR:" %% ct s)
  in
  inline_button
    ~a:[ Fmt.kstr style "min-width: %s" min_width ]
    icon
    ~action:(fun () ->
      Reactive.set copying Wip.WIP;
      Clipboard.input_element_to_clipboard ~id:input_id;
      Reactive.set copying (Wip.success ());
      Lwt.async
        Lwt.(
          fun () ->
            Js_of_ocaml_lwt.Lwt_js.sleep 3. >>= fun () ->
            Reactive.set copying Wip.idle;
            return ());
      ())

let make_table ?header_row ctxt content =
  let small = Web_app_state.Configuration.compact ctxt in
  Bootstrap.Table.simple ~small ?header_row content

let address ?shorten ~links ctxt acc =
  let open Mono_html in
  match Account.get_address acc with
  | None -> t "🤷"
  | Some add -> address_with_links ~links ?shorten ctxt add

let secret_key _ctxt uri =
  let open Mono_html in
  let open Bootstrap.Collapse in
  let col = make () in
  make_div col
    (fun () -> ct uri % t " ")
    ~a:(fun _ -> [ style "display: inline" ])
  % make_button col ~kind:`Info
      (Reactive.bind (collapsed_state col) ~f:(function
        | true -> t "Show Secret Key URI"
        | false -> t "Hide"))

let account_short_display ?(add_details = []) _ctxt acc =
  let open Account in
  let open Mono_html in
  t acc.display_name
  %
  match add_details with
  | [] -> empty ()
  | one :: more ->
      let show = function
        | `Address_short shorten -> (
            (* Cannot use [address ~shorten ~links:[] ctxt acc] because
               we don't want links in links. *)
            match get_address acc with
            | None -> empty ()
            | Some addr -> ct (Stringext.take addr shorten ^ "…"))
        | `Public_key shorten -> (
            match get_public_key acc with
            | None -> empty ()
            | Some pk when String.length pk > shorten ->
                ct (Stringext.take pk shorten ^ "…")
            | Some pk -> ct pk)
        | `Balance ->
            Fmt.kstr t "%s ꜩ"
              (Option.value_map ~default:"‽" ~f:Mutez.to_tez_string acc.balance)
      in
      t " "
      % parens
          (List.fold more ~init:(show one) ~f:(fun p m -> p % t "," %% show m))

let action_link ?a:(a_a = []) _ctxt ~action c =
  let open Mono_html in
  let att =
    H5.a_onclick
      (Reactive.pure
         (Option.some (fun _ ->
              action ();
              true)))
    :: H5.a_href (Reactive.pure "#")
    :: a_a
  in
  a ~a:att c

let account_link ?a ctxt ~id c =
  action_link ?a ctxt c ~action:(fun () ->
      Web_app_state.Current_page.change_to ctxt (Web_app_state.Page.account ~id))

let account_or_address_link ?(shorten_addresses = Some 14)
    ?(shorten_public_keys = Some 18) ?add_details ctxt =
  let open Mono_html in
  let account_short_link ctxt acc =
    account_short_display ?add_details ctxt acc
    |> account_link ctxt ~id:acc.Account.id
  in
  function
  | `Id id ->
      Reactive.bind (Web_app_state.accounts ctxt) ~f:(fun l ->
          match List.find l ~f:(fun acc -> String.equal acc.id id) with
          | None ->
              Bootstrap.color `Danger (Fmt.kstr bt "account:%s:not-found" id)
          | Some acc -> account_short_link ctxt acc)
  | `Address addr ->
      Web_app_state.bind_account_by_address ctxt addr ~f:(function
        | None ->
            address_with_links ?shorten:shorten_addresses ctxt addr
              ~links:[ `BCD ]
        | Some acc -> account_short_link ctxt acc)
  | `Account acc -> account_short_link ctxt acc
  | `Public_key pk ->
      Web_app_state.bind_account_by_public_key ctxt pk ~f:(function
        | Some acc -> account_short_link ctxt acc
        | None ->
            identifier_with_links ?shorten:shorten_public_keys ctxt pk
              ~links:[ `BCD ])

let delete_account_button ?a_button ?(kind = `Info) ctxt acc content =
  let open Account in
  let open Mono_html in
  let delete_wip = Wip.idle_var () in
  let modal_id = Fmt.str "delete-account-%s-modal" acc.id in
  let modal =
    Bootstrap.Modal.make ~modal_id ~modal_title:"Are you 100% sure?"
      ~modal_body:
        (div
           (Fmt.kstr t "Are you sure you want to remove account %s (%s)"
              acc.display_name acc.id))
      ~ok_text:"OK" ~cancel_text:"Cancel"
      ~ok_action:(fun () ->
        dbgf "Deleting %s (%s)" acc.id acc.display_name;
        Server_communication.call_with_var_unit delete_wip
          (Protocol.Message.Up.delete_account acc.id)
          ~catch_failure:(Web_app_state.Local_failures.add_message ctxt);
        Web_app_state.Current_page.change_to ctxt Web_app_state.Page.overview;
        ())
      ()
  in
  Reactive.(bind_var delete_wip) ~f:(function
    | Wip.WIP -> div (Bootstrap.spinner (t "WIP"))
    | _ ->
        Bootstrap.Modal.toggle_button ?a:a_button ~size:`Small ~kind
          ~outline:true content ~modal_id)
  % modal

let are_you_sure_and_submit_banner ?(banner_kind = `Primary)
    (_ctxt : _ Context.t) ~button_label ~action ~question =
  let open Mono_html in
  let button =
    inline_button
      ~a:[ style "font-size: 120%; font-weight: bold" ]
      button_label ~action
  in
  Bootstrap.alert ~kind:banner_kind (Bootstrap.p_lead (question %% button))

module Markdown = struct
  let rec update_headings ~f (doc : Omd.doc) =
    let open Omd in
    List.map doc ~f:(function
      | Heading (att, level, inl) -> Heading (att, f level, inl)
      | Blockquote (attr, blocks) -> Blockquote (attr, update_headings ~f blocks)
      | other -> other)

  let omd_to_html ?(transform = Fn.id) ?with_par blob =
    match
      (with_par, String.find blob ~f:(function '\n' -> true | _ -> false))
    with
    | None, None | Some false, _ -> (
        let md = Omd.of_string blob |> transform |> Omd.to_html in
        try
          assert (String.is_prefix md ~prefix:"<p>");
          assert (String.is_suffix md ~suffix:"</p>\n");
          String.sub md ~pos:3 ~len:(String.length md - 3 - 5)
        with _ -> md)
    | Some true, _ | _, Some _ -> Omd.of_string blob |> transform |> Omd.to_html

  let span ?transform md =
    Mono_html.Unsafe.span_string (omd_to_html ?transform md ~with_par:false)

  let div ?transform md =
    Mono_html.Unsafe.div_string (omd_to_html ?transform md ~with_par:true)
end

module Editor = struct
  let make_var content ~to_string =
    Reactive.var (Ok (to_string content, content))

  let low_level_editor _ctxt ?(disabled = false) ?rows content =
    let open Mono_html in
    let nb_of_rows =
      match rows with
      | Some s -> s
      | None ->
          Reactive.Bidirectional.get content
          |> Reactive.map ~f:(fun s ->
                 String.fold s ~init:4 ~f:(fun s -> function
                   | '\n' -> s + 1 | _ -> s))
    in
    let open H5 in
    let a =
      (if disabled then [ a_disabled () ] else [])
      @ [
          a_style (Lwd.pure "font-family: monospace");
          classes [ "col-12" ];
          a_rows nb_of_rows;
          a_oninput
            (Tyxml_lwd.Lwdom.attr
               Js_of_ocaml.(
                 fun ev ->
                   Js.Opt.iter ev##.target (fun input ->
                       Js.Opt.iter (Dom_html.CoerceTo.textarea input)
                         (fun input ->
                           let v = input##.value |> Js.to_string in
                           dbgf "TA inputs: %d bytes: %S" (String.length v) v;
                           Reactive.Bidirectional.set content v));
                   true));
        ]
    in
    div [ textarea ((* txt *) Reactive.Bidirectional.get content) ~a ]

  let show_result _ctxt edited ~to_html =
    let open Mono_html in
    Reactive.bind_var edited ~f:(function
      | Error (s, e) ->
          pre (code (t s)) % Bootstrap.alert ~kind:`Danger (t (Exn.to_string e))
      | Ok (_, parsed_content) -> to_html parsed_content)

  let collapsable ctxt edited ~show_button_content ~save_button_content
      ~of_string ~unchanged ~save_action =
    let open Mono_html in
    let initial_content = Reactive.peek edited in
    let content =
      Reactive.Bidirectional.make
        (Reactive.get edited
        |> Reactive.map ~f:(function Ok (s, _) -> s | Error (s, _) -> s))
        (fun s ->
          match of_string s with
          | conf -> Reactive.set edited (Ok (s, conf))
          | exception e -> Reactive.set edited (Error (s, e)))
    in
    let save_button =
      let make_button how =
        let disabled, action =
          match how with
          | `Disabled -> (true, Fn.id)
          | `Ready conf -> (false, fun () -> save_action conf)
        in
        inline_button save_button_content ~disabled ~action
      in
      Reactive.bind_var edited ~f:(function
        | Ok (_, conf) when unchanged conf -> make_button `Disabled
        | Ok (_, conf) -> make_button (`Ready conf)
        | Error (_, _) -> make_button `Disabled)
    in
    let collapse = Low_level_collapse.make () in
    Low_level_collapse.button collapse ~content_of_state:(function
      | true ->
          Reactive.set edited initial_content;
          show_button_content
      | false -> t "Cancel")
    % Reactive.bind (Low_level_collapse.collapsed_state collapse) ~f:(function
        | true -> empty ()
        | false -> save_button)
    %% Low_level_collapse.div
         (* ~a:(function false -> [style "display: inline-block"] | _ -> []) *)
         collapse (fun () ->
           Reactive.set edited initial_content;
           low_level_editor ctxt content)

  let side_by_side ?(editor_tab_title = "Editor")
      ?(viewer_tab_title = "Results") ctxt ~editor ~viewer =
    let open Mono_html in
    div ~a:[]
      (Bootstrap.div_row
         (Reactive.bind (Browser_window.width ctxt) ~f:(function
           | Some `Wide ->
               Bootstrap.div_col 6 editor % Bootstrap.div_col 6 viewer
           | Some `Thin | None ->
               let visible = Reactive.var `Editor in
               let tabs =
                 let open Bootstrap.Tab_bar in
                 make
                   [
                     item (t editor_tab_title)
                       ~active:
                         (Reactive.get visible
                         |> Reactive.map ~f:(function
                              | `Editor -> false
                              | `Result -> true))
                       ~action:(fun () -> Reactive.set visible `Editor);
                     item (t viewer_tab_title)
                       ~active:
                         (Reactive.get visible
                         |> Reactive.map ~f:(function
                              | `Editor -> true
                              | `Result -> false))
                       ~action:(fun () -> Reactive.set visible `Result);
                   ]
               in
               Bootstrap.div_col 12
                 (tabs
                 %% Reactive.bind_var visible ~f:(function
                      | `Editor -> editor
                      | `Result -> viewer)))))

  let side_by_side_human_prose_editor ctxt comments =
    let open Mono_html in
    let editor =
      low_level_editor ctxt
        (* ~help:(t "Whichever prose you may desire.")
           ~a:[H5.a_rows (Reactive.pure 20)] *)
        Reactive.Bidirectional.(
          convert (of_var comments)
            (function None -> "" | Some (Human_prose.Raw_markdown s) -> s)
            (function
              | "" -> None | more -> Some (Human_prose.raw_markdown more)))
    in
    let viewer =
      Reactive.bind_var comments ~f:(function
        | None -> empty ()
        | Some (Human_prose.Raw_markdown md) -> Markdown.div md)
    in
    side_by_side ctxt ~editor ~viewer ~viewer_tab_title:"Preview"
end

module Human_prose_widget = struct
  let option_summary ?(wrap = Fn.id) =
    let open Mono_html in
    function
    | None -> empty ()
    | Some (Human_prose.Raw_markdown m) ->
        let the_line =
          String.strip
            ~drop:(function ' ' | '#' -> true | _ -> false)
            (match String.lsplit2 m ~on:'\n' with
            | None -> m
            | Some (first_line, _)
              when String.is_prefix first_line ~prefix:"```" ->
                "……"
            | Some (first_line, _) -> first_line ^ " ……")
        in
        Markdown.span the_line |> wrap

  let option_div ?(before_some = Mono_html.empty) _ctxt =
    let open Mono_html in
    let open Human_prose in
    function
    | None -> empty ()
    | Some (Raw_markdown s) ->
        before_some ()
        % div
            ~a:[ classes [ "user_comment" ] ]
            (Markdown.(div ~transform:(update_headings ~f:(fun lvl -> lvl + 3)))
               s)

  let editable ctxt initial ~id =
    let open Mono_html in
    let open Human_prose in
    let editing =
      Editor.make_var initial ~to_string:(function
        | None -> ""
        | Some (Raw_markdown s) -> s)
    in
    let edit_wip = Wip.idle_var () in
    Editor.show_result ctxt editing ~to_html:(option_div ctxt)
    % Editor.collapsable ctxt editing
        ~show_button_content:
          (Reactive.bind_var editing ~f:(function
            | Ok (_, None) -> t "Add comments"
            | _ -> t "Edit"))
        ~save_button_content:(t "Save Comments")
        ~save_action:(fun comments ->
          Server_communication.call_with_var_unit edit_wip
            (Protocol.Message.Up.update_comments ~id ~comments)
            ~catch_failure:(Web_app_state.Local_failures.add_message ctxt))
        ~of_string:(function
          | "" -> None
          | s ->
              let (_ : Omd.doc) = Omd.of_string s in
              Some (Raw_markdown s))
        ~unchanged:(Option.equal Human_prose.equal initial)
end

let render_validation (ctxt : _ Context.t) validation ~ok =
  let open! Mono_html in
  let open Drafting.Render in
  let show_warnings = function
    | [] -> empty ()
    | more ->
        t " "
        % parens
            (let sep () = t "|" in
             list
             @@ Language.oxfordize_list more
                  ~map:(fun m ->
                    Bootstrap.color `Warning
                      (t "⚠" %% Prosaic_message.to_html ctxt m))
                  ~sep ~last_sep:sep)
  in
  let show_errors err =
    Bootstrap.color `Danger
      (let sep () = t "|" in
       list
         (Language.oxfordize_list err
            ~map:(fun m -> Prosaic_message.to_html ctxt m)
            ~sep ~last_sep:sep))
  in
  Reactive.bind
    (env @@ fun () -> validation)
    ~f:(fun res ->
      thumbs_up_or_down res
        ~ok:(fun { v; warnings } -> ok v % show_warnings warnings)
        ~error:(fun { warnings = _; errors } -> show_errors errors))

module Form = struct
  let choose_an_account ?(filter = fun _ -> true) ctxt string_bidi ~placeholder
      =
    let open! Mono_html in
    Reactive.Bidirectional.bind string_bidi ~f:(fun choice ->
        Reactive.bind (Web_app_state.accounts ctxt) ~f:(fun accounts ->
            let accounts = List.filter accounts ~f:filter in
            let choice_account =
              List.find accounts ~f:(fun a ->
                  String.equal (Account.id a) choice)
            in
            let show_account acc =
              account_short_display ctxt acc
                ~add_details:[ `Balance; `Address_short 10 ]
            in
            let head =
              match choice_account with
              | None -> placeholder
              | Some acc -> show_account acc
            in
            Bootstrap.Dropdown_menu.(
              button head
                ~a_button:[ style "min-width: 27em" ]
                ~kind:`Secondary
                (List.map accounts ~f:(fun acc ->
                     item
                       ~action:(fun () ->
                         Reactive.Bidirectional.set string_bidi acc.id)
                       (show_account acc))))))

  let gas_wallet ctxt gas_wallet ~for_reason =
    let open Mono_html in
    t "Choose a gas-wallet" %% for_reason % t ":"
    %% choose_an_account ctxt gas_wallet ~placeholder:(it "Gas-Wallet Required")
         ~filter:Account.can_be_gas_wallet

  let custom_input_modal ctxt ~id ~modal_title ~custom_choice_label
      ~validate_custom ~modal_prompt ~modal_help ~ok_help ~modal_ok_button
      ~ok_action =
    let open Mono_html in
    let modal_id = id in
    let modal =
      let pk_var = Reactive.var "" in
      let valid_pk =
        Reactive.(bind (get pk_var)) ~f:(fun s -> validate_custom s)
      in
      Bootstrap.Modal.make ~modal_id ~modal_title
        ~modal_body:
          (div
             (Bootstrap.Input.bidirectional
                (Reactive.Bidirectional.of_var pk_var)
                ~label:(modal_prompt ())
                ~help:
                  (modal_help () %% render_validation ctxt ~ok:ok_help valid_pk)
             %%
             let btn active =
               let disabled = not active in
               inline_button ~disabled (t modal_ok_button)
                 ~a:[ Bootstrap.Modal.a_dismiss () ]
                 ~action:(fun () -> ok_action pk_var)
             in
             Reactive.bind (Drafting.Render.is_ok valid_pk) ~f:(fun b -> btn b)
             ))
        ~cancel_text:"Cancel" ()
    in
    let button () =
      Bootstrap.Modal.toggle_button ~size:`Small ~kind:`Dark ~outline:true
        (t custom_choice_label) ~modal_id
    in
    object
      method id = modal_id
      method modal = modal
      method toggle_button = button ()
    end

  let pick_address ctxt ~prompt ~placeholder ~modal_title ~custom_choice_label
      ~validate_custom ~modal_prompt ~modal_help ~ok_help ~modal_ok_button
      bidistring =
    let open Mono_html in
    prompt ()
    % Reactive.Bidirectional.bind bidistring ~f:(fun choice ->
          let custom_choice =
            custom_input_modal ctxt ~id:(Fmt.str "choose-address") ~modal_title
              ~custom_choice_label ~validate_custom ~modal_prompt ~modal_help
              ~ok_help ~modal_ok_button ~ok_action:(fun pk_var ->
                Reactive.Bidirectional.set bidistring (Reactive.peek pk_var);
                ())
          in
          Reactive.bind (Web_app_state.accounts ctxt) ~f:(fun accounts ->
              let accounts =
                List.filter accounts ~f:(fun acc ->
                    Option.is_some (Account.get_address acc))
              in
              let choice_account =
                List.find accounts ~f:(fun a ->
                    Option.equal String.equal (Account.get_address a)
                      (Some choice))
              in
              let show_account acc =
                account_short_display ctxt acc
                  ~add_details:[ `Address_short 12 ]
              in
              let head =
                match (choice_account, choice) with
                | None, "" -> it placeholder
                | None, more ->
                    ct
                      (try String.sub ~pos:0 ~len:16 more ^ "…" with _ -> more)
                | Some acc, _ -> show_account acc
              in
              custom_choice#modal
              % Bootstrap.Dropdown_menu.(
                  button head ~button_classes:[ "col-12"; "text-right" ]
                    ~div_style:"min-width: 25em"
                    (List.map accounts ~f:(fun acc ->
                         item
                           ~action:(fun () ->
                             Reactive.Bidirectional.set bidistring
                               (Account.get_address acc
                               |> Option.value ~default:"BBBUUUGGG"))
                           (span
                              ~a:
                                [
                                  classes [ "text-right" ];
                                  style "width: 100%; display: inline-block";
                                ]
                              (show_account acc)))
                    @ [ header custom_choice#toggle_button ]))))

  let destination_address ?(placeholder = "Desination Address")
      ?(prompt = fun () -> Mono_html.t "Choose a destination address:") ctxt
      bidistring =
    let open Mono_html in
    pick_address ctxt ~placeholder ~prompt
      ~modal_title:"Custom Destination Address"
      ~custom_choice_label:"Pick a custom address."
      ~validate_custom:(fun s -> Validate.address s)
      ~modal_prompt:(fun () -> t "Enter a valid Tezos address:")
      ~modal_help:(fun () ->
        t "Regular Base58 encoding of a"
        %% ct "tz123" %% t "or" %% ct "KT1" %% t "address.")
      ~ok_help:(fun _ -> t "This should work.")
      ~modal_ok_button:"Use This Address" bidistring

  let collection_of_public_keys ?(prompt = fun () -> Mono_html.t "Public Keys:")
      ctxt ~public_keys =
    let open Mono_html in
    prompt ()
    % Reactive.bind (Web_app_state.accounts ctxt) ~f:(fun accounts ->
          let accounts =
            List.filter accounts ~f:(fun acc ->
                Option.is_some (Account.get_public_key acc))
          in
          let set_ith table ith v =
            Reactive.set table
              (List.mapi (Reactive.peek table) ~f:(fun i c ->
                   if i = ith then v else c))
          in
          let remove_ith table ith =
            Reactive.set table
              (List.filteri (Reactive.peek table) ~f:(fun i _ -> i <> ith))
          in
          inline_button (t "➕") ~action:(fun () ->
              Reactive.update public_keys ~f:(fun l -> l @ [ "" ])
              (* Reactive.Table.append public_keys "" *))
          %% Reactive.bind_var
               (* Reactive.Table.list_document *)
               (* concat_map *)
               public_keys
               (* ~map:(fun pk_row pk -> *) ~f:(fun pks ->
                 list
                 @@ List.mapi pks ~f:(fun ith pk ->
                        let choice_account =
                          List.find accounts ~f:(fun a ->
                              Option.equal String.equal
                                (Account.get_public_key a) (Some pk))
                        in
                        let show_account acc =
                          account_short_display ctxt acc
                            ~add_details:[ `Public_key 12 ]
                        in
                        let custom_choice =
                          custom_input_modal ctxt
                            ~id:(Fmt.str "pk-choice-%d" ith)
                            ~modal_title:"Pick A Public Key"
                            ~custom_choice_label:"Custom public key"
                            ~validate_custom:Validate.public_key
                            ~modal_prompt:(fun () -> t "Choose a public key")
                            ~modal_help:(fun () -> t "MODAL-HELP-TODO")
                            ~ok_help:(fun _ -> empty ())
                            ~modal_ok_button:"Use Public Key"
                            ~ok_action:(fun pk_var ->
                              set_ith public_keys ith (Reactive.peek pk_var))
                        in
                        let head =
                          match (choice_account, pk) with
                          | None, "" -> it "Choose a public key"
                          | None, more ->
                              ct
                                (try String.sub ~pos:0 ~len:16 more ^ "…"
                                 with _ -> more)
                          | Some acc, _ -> show_account acc
                        in
                        div
                          (Bootstrap.Dropdown_menu.(
                             button head
                               ~button_classes:[ "col-12"; "text-right" ]
                               ~div_style:"min-width: 25em"
                               (List.map accounts ~f:(fun acc ->
                                    item
                                      ~action:(fun () ->
                                        set_ith public_keys ith
                                          (* Reactive.Table.set pk_row *)
                                          (Account.get_public_key acc
                                          |> Option.value ~default:"BBBUUUGGG"))
                                      (span
                                         ~a:
                                           [
                                             classes [ "text-right" ];
                                             style
                                               "width: 100%; display: \
                                                inline-block";
                                           ]
                                         (show_account acc)))
                               @ [ header custom_choice#toggle_button ]))
                          %% custom_choice#modal
                          %% inline_button (t "❌") ~action:(fun () ->
                                 dbgf "removing====================";
                                 remove_ith public_keys ith
                                 (* Lwd_table.remove pk_row *))))))

  let public_key ctxt pk =
    let open Mono_html in
    Reactive.bind (Web_app_state.accounts ctxt) ~f:(fun accounts ->
        let account =
          List.find accounts ~f:(fun acc ->
              Option.equal String.equal (Account.get_public_key acc) (Some pk))
        in
        match account with
        | None -> ct pk %% parens (t "not from any known account")
        | Some acc ->
            account_short_display ctxt acc ~add_details:[ `Public_key 14 ])

  let make_signing_request ctxt ~blob ~public_key =
    let open Mono_html in
    let from_account_id = Reactive.var "" in
    let memo_markdown = Reactive.var None in
    let request =
      Reactive.(
        Web_app_state.accounts ctxt ** get from_account_id ** get memo_markdown
        |> map) ~f:(fun (accounts, (from_id, memo)) ->
          let metadata : Signing_request.metadata list =
            (if String.is_empty from_id then []
            else
              let addr =
                List.find_map accounts ~f:(function
                  | acc when String.equal (Account.id acc) from_id ->
                      Account.get_address acc
                  | _ -> None)
                |> Option.value ~default:"BUG MISSING ADDRESS"
              in
              [ `From_address addr ])
            @ Option.value_map memo ~default:[] ~f:(fun hp ->
                  [ `Comment ("memo", hp) ])
            @ [
                `Comment
                  ( "wip-from-babuso",
                    Human_prose.raw_markdown "This _is_ **Work-in-Progress**" );
              ]
          in
          Signing_request.make blob ~public_key ~metadata)
    in
    let request_blob_id = "signature-request-for-" ^ public_key in
    let request_blob_input =
      let bidi =
        Reactive.Bidirectional.make
          (Reactive.map request ~f:(fun req ->
               Sexp.to_string_mach (Signing_request.sexp_of_t req)))
          (fun _ -> ())
      in
      Bootstrap.Input.bidirectional ~cols:4 ~inline_block:true
        ~active:(Reactive.pure false) bidi
        ~a:[ H5.a_id (Reactive.pure request_blob_id) ]
    in
    let copy_button = copy_to_clipboard_button ctxt ~input_id:request_blob_id in
    div
      (t "Add a" %% bt "“From”" %% t "address:"
      % choose_an_account ctxt
          (Reactive.Bidirectional.of_var from_account_id)
          ~filter:Account.can_sign ~placeholder:(t "From-address"))
    % div
        (t "Add a memo/message for your co-signer:"
        %% Editor.side_by_side_human_prose_editor ctxt memo_markdown)
    % copy_button % request_blob_input

  let list_of_optional_signatures ctxt ~public_keys ~signatures ~hash ~blob
      ~sign_blob =
    let open Mono_html in
    Reactive.bind_var blob ~f:(function
      | None ->
          it "Building blob to sign …"
          %% Bootstrap.spinner (t "WIP: Getting blob")
      | Some blob ->
          let sign_button account ~pk =
            let sig_wip = Reactive.var Wip.Idle in
            inline_button (t "✍") ~action:(fun () ->
                Wip.lwt_async sig_wip
                  ~f:
                    Lwt.(
                      fun () ->
                        sign_blob account blob >>= fun signature ->
                        Signature.Draft.add_or_replace signatures ~hash
                          signature ~pk;
                        dbgf "Got SIG: %s" signature;
                        return ()))
            % wip_div sig_wip ~success:(fun () -> empty ())
          in
          let sig_input ~pk =
            let sig_var = Reactive.var "" in
            let state = Wip.idle_var () in
            let validation =
              Reactive.(bind (get sig_var)) ~f:(fun s -> Validate.signature s)
            in
            let action () =
              let signature = Reactive.peek sig_var in
              Server_communication.call_with_var_unit state
                (Protocol.Message.Up.check_signature ~blob ~signature
                   ~public_key:pk);
              ()
            in
            let help_state = Reactive.var `None in
            let help () =
              Reactive.bind_var help_state ~f:(fun current ->
                  let button v c =
                    let is_current = Poly.equal current v in
                    inline_button
                      ~a:[ style "min-width: 10em" ]
                      (if is_current then b (c %% t "🠯") else c)
                      ~action:(fun () ->
                        Reactive.set help_state
                          (if is_current then `None else v))
                  in
                  let help_block c = Bootstrap.bordered ~kind:`Secondary c in
                  div
                    ~a:[ style "" ]
                    (bt "Help:"
                    %% button `Octez_client
                         (t "Using" %% Bootstrap.monospace (t "octez-client"))
                    % button `Babuso_signing (t "Using" %% bt "Babuso"))
                  %
                  match current with
                  | `None -> empty ()
                  | `Octez_client ->
                      help_block
                        (t
                           "Please ask the owner of the corresponding \
                            secret-key to run:"
                        % code_block
                            [
                              Fmt.str "octez-client sign bytes %s for <account>"
                                (Bytes_rep.to_zero_x_summary blob);
                            ]
                        % t "where" %% ct "<account>"
                        %% t
                             "is the alias for the secret-key. For instance, \
                              imported using:"
                        % code_block
                            [
                              "octez-client import secret key <account> \
                               ledger://crouching-tiger-hidden-dragon/ed25519/0h/1h";
                            ])
                  | `Babuso_signing ->
                      help_block
                        (p
                           (bt "Option 1:"
                           %% t
                                "Ask the owner of the corresponding secret-key \
                                 to open the"
                           %% bt "Signing"
                           %% t
                                "tab of Babuso, enter the bytes to sign, \
                                 choose the account for this public-key, and \
                                 click “sign.”")
                        % p
                            (bt "Option 2:"
                            %% t
                                 "Make a formal signing-request and ask the \
                                  user to import it:")
                        % make_signing_request ctxt ~blob ~public_key:pk))
            in
            let form () =
              let validate () =
                Reactive.bind validation ~f:(fun v ->
                    let disabled = Result.is_error v in
                    Bootstrap.button
                      ~a:[ classes [ "btn"; "btn-outline-info"; "col-md-1" ] ]
                      (t "Validate") ~action ~disabled)
              in
              Bootstrap.Input.bidirectional
                ~enter_action:action (* ~inline_block:true *)
                ~cols:11
                ~help:
                  (render_validation ctxt validation ~ok:(fun _ ->
                       t "This is a signature."))
                ~placeholder:(Reactive.pure "sigBLaBlaFooBar…")
                (Reactive.Bidirectional.of_var sig_var)
              % Reactive.bind_var state ~f:(function
                  | Wip.Idle -> validate ()
                  | Wip.WIP -> Bootstrap.spinner (t "WIP")
                  | Wip.Success () ->
                      let signature = Reactive.peek sig_var in
                      Signature.Draft.add_or_replace signatures ~hash signature
                        ~pk;
                      t "Should not show this"
                  | Wip.Failure s ->
                      validate ()
                      % Bootstrap.alert ~kind:`Danger
                          (t "This signature is not valid 😭")
                      % Debug.bloc ctxt (Fmt.kstr ct "error: %s" s))
            in
            div ~a:[ classes [ "form-row" ] ] (form ()) % div (help ())
          in
          let of_pk pk =
            let signature_option =
              Reactive.Table.find_map signatures ~f:(fun (req, sign) ->
                  if Signature.Draft.equal_sig_req req (hash, pk) then sign
                  else None)
            in
            let wrap c =
              Reactive.bind signature_option ~f:(fun sg ->
                  let checkbox =
                    match sg with None -> t "☐" | Some _ -> t "☑"
                  in
                  Bootstrap.alert
                    ~kind:(match sg with Some _ -> `Success | None -> `Info)
                    (div
                       ~a:[ style ""; classes [ "row" ] ]
                       (div
                          ~a:[ classes [ "col-1" ]; style "font-size: 200%;" ]
                          checkbox
                       % div
                           ~a:[ classes [ "col-11" ] ]
                           (div (c sg)
                           %
                           match sg with
                           | None -> empty ()
                           | Some sg -> div (t "👍" %% ct sg)))))
            in
            (* let ordering_key one = one ^ pk in *)
            Web_app_state.bind_account_by_public_key ctxt pk ~f:(function
              | Some acc ->
                  wrap (fun sgo ->
                      bt "From"
                      %% account_or_address_link ctxt (`Account acc)
                      %% parens
                           (abbreviation_with_toggle pk
                              ~abbreviated:(ct (Stringext.take pk 12 ^ "…"))
                              ~expanded:(ct pk))
                      %
                      match (sgo, Account.get_secret_key acc) with
                      | Some _, _ -> t ":"
                      | None, Some _ ->
                          t ", you can sign this right now:"
                          %% sign_button acc ~pk
                      | None, None ->
                          t ", you can ask that user to sign the orders"
                          % sig_input ~pk)
              | None ->
                  wrap (fun sgo ->
                      bt "From public key" %% ct pk
                      %%
                      match sgo with
                      | None -> sig_input ~pk
                      | Some _ -> empty ()))
          in
          let pk_list =
            List.map
              (List.sort
                 (* We want the order to be at least stable for now. *)
                 public_keys ~compare:String.compare)
              ~f:of_pk
          in
          div (list pk_list))

  let amount ctxt ?(prompt = fun () -> Mono_html.t "Choose the amount:")
      bidistring =
    let open Mono_html in
    prompt ()
    %% Bootstrap.Input.bidirectional ~cols:1 ~inline_block:true bidistring
    %% Reactive.Bidirectional.bind bidistring ~f:(fun s ->
           Reactive.bind_var (Web_app_state.exchange_rates ctxt)
             ~f:(fun rates ->
               let validation = Validate.parse_amount_to_mutez s rates in
               render_validation ctxt validation ~ok:(fun mutez ->
                   let with_style = Balance.adaptive_inline_style mutez in
                   Balance.multi ctxt mutez ~with_style)))

  let multisig_threshold_input
      ?(prompt =
        fun () -> Mono_html.t "Signatures required (a.k.a. “threshold”):") ctxt
      ~threshold ~public_keys =
    let open Mono_html in
    let valid_threshold =
      Reactive.(map (Bidirectional.get threshold ** public_keys))
        ~f:(fun (th, public_keys) -> Validate.threshold th ~public_keys)
    in
    prompt ()
    %% Bootstrap.Input.bidirectional ~cols:1 ~inline_block:true threshold
    %% t "of"
    %% Reactive.bind public_keys ~f:(fun l -> Fmt.kstr it "%d" (List.length l))
    %% Reactive.bind valid_threshold ~f:(fun v ->
           render_validation ctxt v ~ok:(fun _ -> empty ()))

  let lambda_operation_item_form ctxt op =
    let open Mono_html in
    match op with
    | Lambda_unit_operations.Draft.Transfer { destination; amount = ams } ->
        let bidiamount = Reactive.Bidirectional.of_var ams in
        let bididest = Reactive.Bidirectional.of_var destination in
        Fmt.kstr bt "Transfer ꜩ:"
        % div (destination_address ctxt bididest)
        % div (amount ctxt bidiamount)
    | Delegation { delegate } ->
        let bididelegate = Reactive.Bidirectional.of_var delegate in
        div
          (destination_address ctxt bididelegate ~prompt:(fun () ->
               bt "Set delegate:"))
    | Fa2_transfer
        { token_id_address; token_id_number; amount; source; destination } ->
        bt "Transfer an FA2 Token:"
        % div
            (destination_address ctxt
               (Reactive.Bidirectional.of_var token_id_address)
               ~prompt:(fun () -> t "Address of the token:"))
        % div
            (t "Token ID (within the contract):"
            %% Bootstrap.Input.bidirectional ~cols:1 ~inline_block:true
                 (Reactive.Bidirectional.of_var token_id_number))
        % div
            (t "Amount to transfer:"
            %% Bootstrap.Input.bidirectional ~cols:1 ~inline_block:true
                 (Reactive.Bidirectional.of_var amount))
        % div
            (t "Transfer source:"
            % destination_address ctxt (Reactive.Bidirectional.of_var source)
                ~prompt:(fun () -> t ""))
        % div
            (t "Transfer destination:"
            % destination_address ctxt
                (Reactive.Bidirectional.of_var destination) ~prompt:(fun () ->
                  t ""))

  let lambda_unit_operations ctxt lambda_var =
    let open Mono_html in
    Reactive.bind_var lambda_var ~f:(fun lambda_draft ->
        Debug.(
          bloc ctxt
            (sexpable Lambda_unit_operations.Draft.sexp_of_t lambda_draft
            %% div
                 (let validation =
                    Drafting.Render.env (fun () ->
                        let wargnore k s =
                          let open Drafting.Render.Env in
                          of_var s >>= fun s ->
                          warntext
                            (Fmt.kstr Michelson.C.string "(debug-%s: %s)" k s
                            |> return)
                            "Not validating: %s" s
                        in
                        Validate.Operation.lambda_unit_operations lambda_draft
                          ~amount_mutez:(wargnore "amount"))
                  in
                  Reactive.bind validation ~f:(function
                    | Ok ok ->
                        pre
                          (ok.v |> Lambda_unit_operations.to_michelson
                         |> Michelson.to_concrete |> t)
                        %% t "Warnings:"
                        %% itemize
                             (List.map
                                ~f:(fun v -> Prosaic_message.to_html ctxt v)
                                ok.warnings)
                    | Error _ -> t "ERRTODO"))))
        % (match lambda_draft with
          | Structured ops ->
              Reactive.Table.list_document ops
              |> Reactive.bind ~f:(fun aslist ->
                     let ops_number = List.length aslist in
                     list
                     @@ List.mapi aslist ~f:(fun idx op ->
                            (* Reactive.Table.concat_map ops ~map:(fun row op -> *)
                            let form = lambda_operation_item_form ctxt op in
                            div
                              (let control_button ?(disabled = false) c ~action
                                   =
                                 inline_button ~disabled ~action
                                   (if disabled then
                                    Bootstrap.color `Secondary (b c)
                                   else Bootstrap.color `Danger (b c))
                               in
                               Fmt.kstr bt "Step %d:" (idx + 1)
                               %% control_button (t "❌") ~action:(fun () ->
                                      Reactive.Table.clear ops;
                                      List.iteri aslist ~f:(fun i op ->
                                          if i <> idx then
                                            Reactive.Table.append ops op
                                          else ());
                                      ())
                               %% control_button (t "⇧") ~disabled:(idx = 0)
                                    ~action:(fun () ->
                                      let prev =
                                        List.nth_exn aslist (idx - 1)
                                      in
                                      let newlist =
                                        List.mapi aslist ~f:(fun i o ->
                                            if i = idx - 1 then op
                                            else if i = idx then prev
                                            else o)
                                      in
                                      Reactive.Table.clear ops;
                                      List.iter newlist ~f:(fun op ->
                                          Reactive.Table.append ops op);
                                      ())
                               %% control_button (t "⇩")
                                    ~disabled:(idx = ops_number - 1)
                                    ~action:(fun () ->
                                      let next =
                                        List.nth_exn aslist (idx + 1)
                                      in
                                      let newlist =
                                        List.mapi aslist ~f:(fun i o ->
                                            if i = idx + 1 then op
                                            else if i = idx then next
                                            else o)
                                      in
                                      Reactive.Table.clear ops;
                                      List.iter newlist ~f:(fun op ->
                                          Reactive.Table.append ops op);
                                      ())
                               %% bt "→" %% form))))
        %% div
             (inline_button (t "Add transfer") ~action:(fun () ->
                  Lambda_unit_operations.Draft.add_empty_transfer lambda_draft)
             %% inline_button (t "Add delegation") ~action:(fun () ->
                    Lambda_unit_operations.Draft.add_empty_delegation
                      lambda_draft)
             %% inline_button (t "Add FA2 Transfer (incl. NFTs)")
                  ~action:(fun () ->
                    Lambda_unit_operations.Draft.add_empty_fa2_transfer
                      lambda_draft)))

  let time_span (ctxt : _ Context.t) bidi =
    let open Mono_html in
    let show_time_span z =
      let warn c = Bootstrap.color `Warning (t " ⚠" %% bt c) in
      let info c = t " 💡" %% it c in
      let hour = Z.of_int (60 * 60) in
      let day = Z.(of_int 24 * hour) in
      let month = Z.(of_int 30 * day) in
      let year = Z.(of_int 365 * day) in
      Fmt.kstr t "%a seconds" Z.pp_print z
      % Debug.text ctxt (Fmt.str "h:%a y:%a" Z.pp_print hour Z.pp_print year)
      %
      match z with
      | n when Z.lt n Z.zero -> warn "Negative? Really?"
      | n when Z.lt n Z.(of_int 6 * hour) ->
          warn "Less than a few hours does not make much sense in a blockchain."
      | n when Z.lt n Z.(of_int 72 * hour) ->
          Fmt.kstr info "About %a hours" Z.pp_print (Z.div n hour)
      | n when Z.lt n Z.(of_int 75 * day) ->
          Fmt.kstr info "About %a days" Z.pp_print (Z.div n day)
      | n when Z.lt n Z.(of_int 2 * year) ->
          Fmt.kstr info "About %a months" Z.pp_print (Z.div n month)
      | n when Z.lt n Z.(of_int 100 * year) ->
          Fmt.kstr info "About %a years" Z.pp_print (Z.div n year)
      | n ->
          Fmt.kstr warn "About %a years, which is a lot!" Z.pp_print
            (Z.div n year)
    in
    Bootstrap.Input.bidirectional ~inline_block:true ~cols:3 bidi
      ~placeholder:(Reactive.pure "Time-span in seconds …")
    % small
        (Reactive.Bidirectional.bind bidi ~f:(fun s ->
             render_validation ctxt (Validate.seconds_of_string "" s)
               ~ok:(fun v -> show_time_span v)))
end

let show_validation_errors_and_warnings ctxt client_validation =
  let open Mono_html in
  let warning m = t "🚨" %% Prosaic_message.to_html ctxt m in
  let error m = t "⛔" %% Prosaic_message.to_html ctxt m in
  let messages ttl l kind =
    match l with
    | [] -> empty ()
    | _ ->
        h5 ttl
        %% list
             (List.map l ~f:(fun m ->
                  div ~a:[ style "margin-bottom: 4px" ] (kind m)))
  in
  Reactive.bind client_validation ~f:(function
    | Ok { Drafting.Render.v = _; warnings = [] } -> empty ()
    | Ok { Drafting.Render.v = _; warnings } ->
        Bootstrap.alert ~kind:`Warning
          (messages (t "Warnings") warnings warning)
    | Error { Drafting.Render.errors; warnings } ->
        Bootstrap.alert ~kind:`Danger
          (messages (t "Errors") errors error
          % messages (t "Warnings") warnings warning))

module List_of_actions = struct
  open Mono_html

  type 'a item = Item of 'a

  let button_style = "min-width: 50em; text-align: left"
  let add_content c = t "🔲 " % c

  let item content ~action =
    Item
      (Bootstrap.button (add_content content)
         ~a:[ style button_style ]
         ~size:`Small ~outline:true ~kind:`Info ~action)

  let delete_account_item ctxt acc content =
    Item
      (delete_account_button
         ~a_button:[ style button_style ]
         ctxt acc (add_content content))

  let render l = list (List.map ~f:(function Item i -> div i) l)
end

module Toolkit_automaton = struct
  open Mono_html

  type logs = Html_types.li_content_fun H5.elt list

  type ('a, 'b) item =
    | Item of { button_name : 'a t; form : idle:(logs -> unit) -> 'b t }
  [@@deriving variants]

  let render items =
    let form_state = Reactive.var (`Idle []) in
    Reactive.bind_var form_state ~f:(function
      | `Idle (logs : logs) ->
          let logs_banner =
            match logs with
            | [] -> empty ()
            | more ->
                Bootstrap.alert ~kind:`Secondary
                  (div (it "This just happened ✍:" %% itemize more))
          in
          let buttons =
            List.map
              ~f:(fun (Item item) ->
                List_of_actions.item item.button_name ~action:(fun () ->
                    Reactive.set form_state (`Item (Item item))))
              items
            |> List_of_actions.render
          in
          logs_banner % div buttons
      | `Item (Item item) ->
          item.form ~idle:(fun logs -> Reactive.set form_state (`Idle logs)))

  let box_with_title items =
    Bootstrap.bordered ~kind:`Info ~rounded:`Default
      (h4 (t "Tools 🛠") %% render items)
end

module Global_modal = struct
  type t = { id : string; content : Html_types.div Mono_html.t }
  [@@deriving make]
  (** {b Hack Alert:} Bootstrap modals (the JS) don't play well with redrawing
      by [Tyxcml_lwd] so we store them in a global hash-table and reuse them
      when already created. *)

  let _all_modals : (string, t) Hashtbl.t = Hashtbl.create (module String)

  let make_or_get ~title ~close_text id ~content =
    match Hashtbl.find _all_modals id with
    | Some m -> m
    | None ->
        let modal =
          Bootstrap.Modal.mk_modal ~modal_id:id ~modal_title:title
            ~width:`Extra_large ~modal_body:content ~cancel_text:close_text ()
        in
        let data = make ~id ~content:modal in
        Hashtbl.add_exn _all_modals ~key:id ~data;
        data

  let as_button ?a:abutton ?(size = `Small) ?(kind = `Info) ?(outline = true)
      self ~label =
    let open Mono_html in
    div ~a:[ style "display:inline-block" ] self.content
    % Bootstrap.Modal.toggle_button ?a:abutton ~size ~kind ~outline label
        ~modal_id:self.id

  let raw_json ?(title = "Raw Json") ?(close_text = "Close") json =
    let id =
      Fmt.str "global-json-%s-modal" Caml.Digest.(string json |> to_hex)
    in
    make_or_get id ~title ~close_text
      ~content:
        Mono_html.(
          div
            (code_block
               (try Json.[ of_string json |> to_string ] with _ -> [ json ])))

  let michelson ?(title = "Raw Michelson") ?(close_text = "Close") m =
    let concrete = Michelson.to_concrete m in
    let id =
      Fmt.str "global-json-%s-modal" Caml.Digest.(string concrete |> to_hex)
    in
    make_or_get id ~title ~close_text
      ~content:Mono_html.(div (code_block [ concrete ]))
end

module Staged_form = struct
  open Mono_html

  type item = {
    id : string;
    button_title : Html_types.button_content_fun H5.elt;
    button_active : bool Reactive.t;
    load_action : unit -> unit;
    content : Html_types.div_content_fun H5.elt; [@main]
  }
  [@@deriving make, fields]

  let item ~button_title ?(button_active = Reactive.pure true)
      ?(load_action = Fn.id) content =
    let id = Fresh_id.make () in
    make_item ~id ~button_title ~button_active ~load_action content

  let make_button ~current_stage ~stage_var sta =
    let is_current = String.equal sta.id current_stage.id in
    Reactive.bind (button_active sta) ~f:(fun v ->
        let ttle = button_title sta in
        inline_button ~disabled:(is_current || not v)
          ~action:(fun () ->
            Reactive.set stage_var sta;
            load_action sta ())
          (if is_current then b ttle else ttle))

  let render ?(before_buttons = empty ()) ctxt = function
    | [] -> Debug.(bloc ctxt (t "Empty staged-form????"))
    | first :: more ->
        let stage_var = Reactive.var first in
        Reactive.bind_var stage_var ~f:(fun current_stage ->
            let stages_buttons =
              before_buttons
              %% List.fold ~init:(make_button ~current_stage ~stage_var first)
                   more ~f:(fun prev sta ->
                     prev %% t "❱" %% make_button ~current_stage ~stage_var sta)
            in
            div stages_buttons % div (content current_stage))
end

let michelson _ctxt m = Mono_html.ct (Michelson.to_concrete m)

let big_michelson ?label ?(before_button = Mono_html.empty) ctxt m =
  let open Mono_html in
  let c = Michelson.to_concrete m in
  let lines = String.count c ~f:(function '\n' -> true | _ -> false) + 1 in
  if lines > 2 then
    let label =
      match label with
      | Some l -> l
      | None -> Fmt.kstr it "Show %d lines of Michelson" lines
    in
    before_button ()
    % (Global_modal.michelson m |> Global_modal.as_button ~label)
  else michelson ctxt m

let variable_value ctxt =
  let open Mono_html in
  let open Variable.Value in
  function
  | Address addr ->
      account_or_address_link ?shorten_addresses:None ctxt (`Address addr)
        ~add_details:[ `Address_short 12 ]
  | Mutez z -> Balance.inline_tez z
  | Nat z -> Fmt.kstr ct "%a" Z.pp_print z
  | Key_list pks ->
      itemize
        (List.map pks ~f:(fun pk ->
             account_or_address_link ?shorten_public_keys:None ctxt
               ~add_details:[ `Public_key 100 ]
               (`Public_key pk)))
  | Unit -> t "🗋"
  | Arbitrary_typed { t = ty; v } ->
      div
        (t "Type:"
        %% ct (Michelson_expression.to_michelson ty |> Michelson.to_concrete))
      % div
          (t "Value:"
          %% ct (Michelson_expression.to_michelson v |> Michelson.to_concrete))
  | Time_span zseconds -> Fmt.kstr t "%a seconds" Z.pp_print zseconds
  | Timestamp_option None -> it "Not set"
  | Timestamp_option (Some (Seconds ts)) ->
      Fmt.kstr t "%a seconds" Z.pp_print ts
  | Weighted_key_list kl ->
      itemize
        (List.map kl ~f:(fun (wgt, pk) ->
             Fmt.kstr ct "%a" Z.pp_print wgt
             %% t "→"
             %% account_or_address_link ?shorten_public_keys:None ctxt
                  ~add_details:[ `Public_key 100 ]
                  (`Public_key pk)))
  | Lambda_unit_to_operation_list mich -> big_michelson ctxt mich
  | Signature_option_list sol ->
      itemize (List.map sol ~f:(function None -> t "∅" | Some s -> ct s))
  | Some_pkh addr ->
      account_or_address_link ?shorten_addresses:None ctxt (`Address addr)
        ~add_details:[ `Address_short 12 ]
  | None_pkh -> t "None"
  | (Bytes _ | String _) as unhandled ->
      ct (Sexp.to_string_hum (sexp_of_t unhandled))

let rec tezos_operation_error ctxt err =
  let open Mono_html in
  let open Tezos_operation_error.Kind in
  match Tezos_operation_error.kind err with
  | Empty_transaction -> t "The transaction is empty, refused by the protocol."
  | Non_existing_contract s -> t "The address" %% ct s %% t "does not exist"
  | Balance_too_low { contract : string; balance : Z.t; amount : Z.t } ->
      t "The account"
      %% account_or_address_link ctxt (`Address contract)
      %% t "does not have enough funds"
      %% parens (Balance.inline_tez balance)
      %% t "to afford the" %% Balance.inline_tez amount %% t "required."
  | Ill_typed_michelson { expected_type; expression } ->
      t "The type of the Michelson expression"
      %% michelson ctxt expression
      %% t "does not have the right type:"
      %% michelson ctxt expected_type
      % t "."
  | Storage_exhausted -> t "The operation has hit its storage limit."
  | Syntax_error_in_michelson { expected_from_protocol; expression } ->
      t "This Michelson expression caused a syntax-error:"
      %% michelson ctxt expression
      % Fmt.kstr t ", the protocol says it wants “%s” …" expected_from_protocol
  | Contract_runtime_error { address } ->
      t "The contract"
      %% account_or_address_link ctxt (`Address address)
      %% t "has hit a runtime error."
  | Contract_hit_failwith { location = _; message } ->
      t "The contract exited with the"
      %% ct "FAILWITH"
      %% t "Michelson operation and"
      %% michelson ctxt message %% t "as argument."
  | Unknown json ->
      t "Error non-understood:"
      %% code_block [ Json.to_string json ]
      %% Debug.(
           bloc ctxt
             (t "Hacking the re-parsing:"
             %% div
                  (match Tezos_operation_error.Kind.of_error_json json with
                  | Unknown _ -> t "still unknown"
                  | other ->
                      tezos_operation_error ctxt { kind = other; raw = `Null })
             ))
