open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

let account_actions ctxt acc =
  let open Mono_html in
  let action title action =
    Custom_widget.List_of_actions.(item title ~action)
  in
  let some_action title actionf = Option.some (action title actionf) in
  let transfer_to =
    match Account.get_address acc with
    | Some addr ->
        some_action (t "Transfer funds to this account") (fun () ->
            Web_app_state.new_operation_draft ctxt
              (Operation.Draft.create_simple_transfer ~destination:addr ()))
    | None -> None
  in
  let transfer_from =
    match Account.can_be_gas_wallet acc with
    | true ->
        some_action (t "Transfer funds from this account") (fun () ->
            Web_app_state.new_operation_draft ctxt
              (Operation.Draft.create_simple_transfer ~source:acc.id ()))
    | false -> None
  in
  let calls =
    match Account.state acc with
    | Originated_contract { contract = Generic_multisig { variables }; address }
      ->
        [
          action
            (t "Reconfigure Multi-sig parameters (public-keys and threshold)")
            (fun () ->
              let threshold =
                Variable.Record.get variables "threshold"
                |> Variable.Value.Q.nat
              in
              let public_keys =
                Variable.Record.get variables "keys"
                |> Variable.Value.Q.key_list
              in
              Web_app_state.new_operation_draft ctxt
                (Operation.Draft.create_call_generic_multisig_update_keys
                   ~threshold ~public_keys ~address ()));
          action
            (t
               "Order transactions through the Multi-sig (main entrypoint, \
                Michelson lambda)") (fun () ->
              Web_app_state.new_operation_draft ctxt
                (Operation.Draft.create_call_generic_multisig_main ~address ()));
        ]
    | Originated_contract { contract = Custom { entrypoints; _ }; address; _ }
      ->
        List.map entrypoints ~f:(fun ({ name; _ } as ep) ->
            let parameters = Entrypoint.collect_parameters ep in
            action
              (t "Call entrypoint" %% ct name)
              (fun () ->
                try
                  Web_app_state.new_operation_draft ctxt
                    (Operation.Draft.create_call_custom_contract_entrypoint
                       ~parameters ~address ~entrypoint:name ())
                with _ ->
                  Web_app_state.Local_failures.add_message ctxt
                    (Fmt.str
                       "create_call_custom_contract_entrypoint failed for: %a"
                       Sexp.pp_hum (Entrypoint.sexp_of_t ep))))
    | Originated_contract { contract = Foreign { entrypoints; _ }; address; _ }
      ->
        List.map entrypoints ~f:(fun (name, et) ->
            (* let parameters = Entrypoint.collect_parameters ep in *)
            action
              (t "Call entrypoint" %% ct name)
              (fun () ->
                try
                  let parameters =
                    Variable.Parameter_type.(
                      comb_of_normalized_michelson et |> get_comb)
                  in
                  Web_app_state.new_operation_draft ctxt
                    (Operation.Draft.create_call_foreign_contract_entrypoint
                       ~address ~entrypoint:name ~parameters ())
                with _ ->
                  Web_app_state.Local_failures.add_message ctxt
                    (Fmt.str
                       "create_call_custom_contract_entrypoint failed for: \
                        %s.%S"
                       address name)))
    | _ -> []
  in
  Custom_widget.List_of_actions.(
    let delete =
      delete_account_item ctxt acc
        (Bootstrap.color `Danger (t "Delete this account"))
    in
    render (List.filter_opt [ transfer_to; transfer_from ] @ calls @ [ delete ]))

let display_account ctxt acc =
  let open Mono_html in
  let open Account in
  let make_row name v = (bt name, v) in
  let comments =
    [
      make_row "User-comments"
        (Custom_widget.Human_prose_widget.editable ctxt acc.comments ~id:acc.id);
    ]
  in
  let type_name, more_rows =
    let open Status in
    match acc.state with
    | Key_pair { public_key_hash = _; public_key; backend } ->
        ( "Key Pair",
          [
            make_row "Public-Key" (ct public_key);
            make_row "Backend"
              (match backend with
              | Key_pair.Ledger { uri } -> ct uri
              | Key_pair.Unencrypted { private_uri } ->
                  Custom_widget.secret_key ctxt private_uri);
          ] )
    | Friend { public_key_hash = _; public_key } ->
        ( "Friend",
          [
            make_row "Public-Key"
              (match public_key with None -> it "Unknown" | Some s -> ct s);
          ] )
    | Originated_contract { address = _; contract } ->
        let summary =
          let open Smart_contract in
          match contract with
          | Generic_multisig _ ->
              it "A “Generic-multisig,” the formally verified one."
          | Custom _ -> it "A custom contract."
          | Foreign _ -> it "A foreign contract."
        in
        let show_variables =
          let open Variable in
          function
          | Record.Comb vars ->
              list
                (List.map vars ~f:(fun (k, v) ->
                     let name_entry =
                       match k with "" -> it "NO-NAME" | n -> b (ct n)
                     in
                     div
                       (name_entry % t " → "
                       % Custom_widget.variable_value ctxt v)))
        in
        let state =
          let open Smart_contract in
          match contract with
          | Generic_multisig { variables } -> show_variables variables
          | Custom { variables; _ } -> show_variables variables
          | Foreign { storage_content; storage_type; _ } ->
              let v =
                Variable.Record.of_normalized_michelson_type_and_expression
                  ~t:storage_type ~v:storage_content
              in
              show_variables v
              % Custom_widget.Global_modal.(
                  michelson storage_content
                  |> as_button ~label:(t "Show as Michelson"))
        in
        ( "Smart-Contract",
          [ make_row "Contract Type" summary; make_row "State" state ] )
    | Draft _ ->
        ("Draft account (Not Ready)", [ make_row "TODO-Edit" (ct "EDIT") ])
    | Contract_failed_to_originate { operation; _ } ->
        ( "Smart-Contract (Failed to originate)",
          [
            make_row "Failed Origination"
              (Web_app_state.bind_operation ctxt operation ~f:(fun op ->
                   Operation_display.short ctxt op));
          ] )
    | Contract_to_originate { operation; _ } ->
        ( "Smart-Contract (Not Ready)",
          [
            make_row "Origination in progress"
              (Web_app_state.bind_operation ctxt operation ~f:(fun op ->
                   Operation_display.short ctxt op));
          ] )
  in
  let basic_account_data =
    let rows =
      [
        make_row "Address" (Custom_widget.address ctxt acc ~links:[ `BCD ]);
        make_row "Balance"
          (match Account.balance acc with
          | None -> t "🤷 Not Available."
          | Some mutez ->
              let with_style =
                Custom_widget.Balance.adaptive_inline_style mutez
              in
              Custom_widget.Balance.multi ctxt ~with_style mutez);
      ]
      @ more_rows @ comments
      @ [ make_row "Actions" (account_actions ctxt acc) ]
    in
    Custom_widget.make_table ctxt
      (list (List.map rows ~f:(fun (k, v) -> Bootstrap.Table.row [ k; v ])))
  in
  let name_edition_statue = Reactive.var (Wip.Success acc.display_name) in
  Custom_widget.div_title
    (Reactive.bind_var name_edition_statue ~f:(function
      | Wip.Success name ->
          Fmt.kstr t "%s “" type_name
          % t name % t "”"
          % Custom_widget.inline_button (t "🖉") ~action:(fun () ->
                Reactive.set name_edition_statue Wip.Idle)
      | Wip.Failure s -> Bootstrap.label `Danger (t s)
      | Wip.WIP -> div (Bootstrap.spinner (t "WIP"))
      | Idle ->
          let v = Reactive.var acc.display_name in
          t "Set new display name:"
          % Bootstrap.Input.bidirectional ~cols:4 ~inline_block:true
              (Reactive.Bidirectional.of_var v)
          % Custom_widget.inline_button (t "Save") ~action:(fun () ->
                (* Reactive.set name_edition_statue Wip.WIP ; *)
                Server_communication.call_with_var name_edition_statue
                  ~catch_failure:(Web_app_state.Local_failures.add_message ctxt)
                  (Protocol.Message.Up.update_display_name ~id:acc.id
                     ~name:(Reactive.peek v))
                  (fun _ -> Reactive.peek v);
                ())
          %% Custom_widget.inline_button (t "Cancel") ~action:(fun () ->
                 Reactive.set name_edition_statue (Wip.success acc.display_name))))
  % basic_account_data
  % Operations_table.table_for_account ctxt acc
  % Custom_widget.Debug.(bloc ctxt (sexpable Account.sexp_of_t acc))

let render ?show_account ctxt ~account_id =
  let open Mono_html in
  Reactive.bind (Web_app_state.accounts ctxt) ~f:(fun accounts ->
      match
        List.find accounts ~f:String.(fun a -> Account.id a = account_id)
      with
      | None ->
          Bootstrap.alert ~kind:`Danger
            (Fmt.kstr bt "Account not found %s ⇒ BUG Found" account_id)
      | Some acc ->
          let to_html =
            match show_account with None -> display_account ctxt | Some f -> f
          in
          to_html acc)
